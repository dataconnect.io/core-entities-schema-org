
package io.dataconnect.model;

import java.net.URI;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * ServiceChannel
 * <p>
 * A means for accessing a service, e.g. a government office location, web site, or phone number.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "potentialAction",
    "name",
    "servicePhone",
    "url",
    "description",
    "providesService",
    "processingTime",
    "sameAs",
    "serviceLocation",
    "servicePostalAddress",
    "serviceUrl",
    "mainEntityOfPage",
    "additionalType",
    "alternateName",
    "image",
    "availableLanguage",
    "serviceSmsNumber"
})
public class AvailableChannel {

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;
    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     */
    @JsonProperty("servicePhone")
    @JsonPropertyDescription("")
    private ContactPoint servicePhone;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;
    /**
     * Service
     * <p>
     * A service provided by an organization, e.g. delivery service, print services, etc.
     * 
     */
    @JsonProperty("providesService")
    @JsonPropertyDescription("")
    private ProvidesService providesService;
    /**
     * Duration
     * <p>
     * Quantity: Duration (use  <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 duration format</a>).
     * 
     */
    @JsonProperty("processingTime")
    @JsonPropertyDescription("")
    private Duration processingTime;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     */
    @JsonProperty("serviceLocation")
    @JsonPropertyDescription("")
    private ContainsPlace serviceLocation;
    /**
     * PostalAddress
     * <p>
     * The mailing address.
     * 
     */
    @JsonProperty("servicePostalAddress")
    @JsonPropertyDescription("")
    private ServicePostalAddress servicePostalAddress;
    /**
     * The website to access the service.
     * 
     */
    @JsonProperty("serviceUrl")
    @JsonPropertyDescription("")
    private URI serviceUrl;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * Language
     * <p>
     * Natural languages such as Spanish, Tamil, Hindi, English, etc. and programming languages such as Scheme and Lisp.
     * 
     */
    @JsonProperty("availableLanguage")
    @JsonPropertyDescription("")
    private AvailableLanguage availableLanguage;
    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     */
    @JsonProperty("serviceSmsNumber")
    @JsonPropertyDescription("")
    private ContactPoint serviceSmsNumber;

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public AvailableChannel withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public AvailableChannel withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     * @return
     *     The servicePhone
     */
    @JsonProperty("servicePhone")
    public ContactPoint getServicePhone() {
        return servicePhone;
    }

    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     * @param servicePhone
     *     The servicePhone
     */
    @JsonProperty("servicePhone")
    public void setServicePhone(ContactPoint servicePhone) {
        this.servicePhone = servicePhone;
    }

    public AvailableChannel withServicePhone(ContactPoint servicePhone) {
        this.servicePhone = servicePhone;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public AvailableChannel withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public AvailableChannel withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * Service
     * <p>
     * A service provided by an organization, e.g. delivery service, print services, etc.
     * 
     * @return
     *     The providesService
     */
    @JsonProperty("providesService")
    public ProvidesService getProvidesService() {
        return providesService;
    }

    /**
     * Service
     * <p>
     * A service provided by an organization, e.g. delivery service, print services, etc.
     * 
     * @param providesService
     *     The providesService
     */
    @JsonProperty("providesService")
    public void setProvidesService(ProvidesService providesService) {
        this.providesService = providesService;
    }

    public AvailableChannel withProvidesService(ProvidesService providesService) {
        this.providesService = providesService;
        return this;
    }

    /**
     * Duration
     * <p>
     * Quantity: Duration (use  <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 duration format</a>).
     * 
     * @return
     *     The processingTime
     */
    @JsonProperty("processingTime")
    public Duration getProcessingTime() {
        return processingTime;
    }

    /**
     * Duration
     * <p>
     * Quantity: Duration (use  <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 duration format</a>).
     * 
     * @param processingTime
     *     The processingTime
     */
    @JsonProperty("processingTime")
    public void setProcessingTime(Duration processingTime) {
        this.processingTime = processingTime;
    }

    public AvailableChannel withProcessingTime(Duration processingTime) {
        this.processingTime = processingTime;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public AvailableChannel withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @return
     *     The serviceLocation
     */
    @JsonProperty("serviceLocation")
    public ContainsPlace getServiceLocation() {
        return serviceLocation;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @param serviceLocation
     *     The serviceLocation
     */
    @JsonProperty("serviceLocation")
    public void setServiceLocation(ContainsPlace serviceLocation) {
        this.serviceLocation = serviceLocation;
    }

    public AvailableChannel withServiceLocation(ContainsPlace serviceLocation) {
        this.serviceLocation = serviceLocation;
        return this;
    }

    /**
     * PostalAddress
     * <p>
     * The mailing address.
     * 
     * @return
     *     The servicePostalAddress
     */
    @JsonProperty("servicePostalAddress")
    public ServicePostalAddress getServicePostalAddress() {
        return servicePostalAddress;
    }

    /**
     * PostalAddress
     * <p>
     * The mailing address.
     * 
     * @param servicePostalAddress
     *     The servicePostalAddress
     */
    @JsonProperty("servicePostalAddress")
    public void setServicePostalAddress(ServicePostalAddress servicePostalAddress) {
        this.servicePostalAddress = servicePostalAddress;
    }

    public AvailableChannel withServicePostalAddress(ServicePostalAddress servicePostalAddress) {
        this.servicePostalAddress = servicePostalAddress;
        return this;
    }

    /**
     * The website to access the service.
     * 
     * @return
     *     The serviceUrl
     */
    @JsonProperty("serviceUrl")
    public URI getServiceUrl() {
        return serviceUrl;
    }

    /**
     * The website to access the service.
     * 
     * @param serviceUrl
     *     The serviceUrl
     */
    @JsonProperty("serviceUrl")
    public void setServiceUrl(URI serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public AvailableChannel withServiceUrl(URI serviceUrl) {
        this.serviceUrl = serviceUrl;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public AvailableChannel withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public AvailableChannel withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public AvailableChannel withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public AvailableChannel withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * Language
     * <p>
     * Natural languages such as Spanish, Tamil, Hindi, English, etc. and programming languages such as Scheme and Lisp.
     * 
     * @return
     *     The availableLanguage
     */
    @JsonProperty("availableLanguage")
    public AvailableLanguage getAvailableLanguage() {
        return availableLanguage;
    }

    /**
     * Language
     * <p>
     * Natural languages such as Spanish, Tamil, Hindi, English, etc. and programming languages such as Scheme and Lisp.
     * 
     * @param availableLanguage
     *     The availableLanguage
     */
    @JsonProperty("availableLanguage")
    public void setAvailableLanguage(AvailableLanguage availableLanguage) {
        this.availableLanguage = availableLanguage;
    }

    public AvailableChannel withAvailableLanguage(AvailableLanguage availableLanguage) {
        this.availableLanguage = availableLanguage;
        return this;
    }

    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     * @return
     *     The serviceSmsNumber
     */
    @JsonProperty("serviceSmsNumber")
    public ContactPoint getServiceSmsNumber() {
        return serviceSmsNumber;
    }

    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     * @param serviceSmsNumber
     *     The serviceSmsNumber
     */
    @JsonProperty("serviceSmsNumber")
    public void setServiceSmsNumber(ContactPoint serviceSmsNumber) {
        this.serviceSmsNumber = serviceSmsNumber;
    }

    public AvailableChannel withServiceSmsNumber(ContactPoint serviceSmsNumber) {
        this.serviceSmsNumber = serviceSmsNumber;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(potentialAction).append(name).append(servicePhone).append(url).append(description).append(providesService).append(processingTime).append(sameAs).append(serviceLocation).append(servicePostalAddress).append(serviceUrl).append(mainEntityOfPage).append(additionalType).append(alternateName).append(image).append(availableLanguage).append(serviceSmsNumber).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AvailableChannel) == false) {
            return false;
        }
        AvailableChannel rhs = ((AvailableChannel) other);
        return new EqualsBuilder().append(potentialAction, rhs.potentialAction).append(name, rhs.name).append(servicePhone, rhs.servicePhone).append(url, rhs.url).append(description, rhs.description).append(providesService, rhs.providesService).append(processingTime, rhs.processingTime).append(sameAs, rhs.sameAs).append(serviceLocation, rhs.serviceLocation).append(servicePostalAddress, rhs.servicePostalAddress).append(serviceUrl, rhs.serviceUrl).append(mainEntityOfPage, rhs.mainEntityOfPage).append(additionalType, rhs.additionalType).append(alternateName, rhs.alternateName).append(image, rhs.image).append(availableLanguage, rhs.availableLanguage).append(serviceSmsNumber, rhs.serviceSmsNumber).isEquals();
    }

}
