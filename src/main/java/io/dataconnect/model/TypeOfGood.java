
package io.dataconnect.model;

import java.net.URI;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Product
 * <p>
 * Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "isConsumableFor",
    "weight",
    "isAccessoryOrSparePartFor",
    "sameAs",
    "purchaseDate",
    "image",
    "gtin8",
    "height",
    "releaseDate",
    "isRelatedTo",
    "additionalType",
    "logo",
    "productID",
    "category",
    "isSimilarTo",
    "width",
    "review",
    "audience",
    "color",
    "additionalProperty",
    "offers",
    "mainEntityOfPage",
    "productionDate",
    "sku",
    "description",
    "mpn",
    "brand",
    "award",
    "awards",
    "itemCondition",
    "alternateName",
    "manufacturer",
    "potentialAction",
    "name",
    "aggregateRating",
    "url",
    "reviews",
    "gtin14",
    "depth",
    "gtin13",
    "gtin12",
    "model"
})
public class TypeOfGood {

    /**
     * Product
     * <p>
     * Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online.
     * 
     */
    @JsonProperty("isConsumableFor")
    @JsonPropertyDescription("")
    private TypeOfGood isConsumableFor;
    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     */
    @JsonProperty("weight")
    @JsonPropertyDescription("")
    private Weight weight;
    /**
     * Product
     * <p>
     * Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online.
     * 
     */
    @JsonProperty("isAccessoryOrSparePartFor")
    @JsonPropertyDescription("")
    private TypeOfGood isAccessoryOrSparePartFor;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * The date the item e.g. vehicle was purchased by the current owner.
     * 
     */
    @JsonProperty("purchaseDate")
    @JsonPropertyDescription("")
    private Date purchaseDate;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-8.aspx">GTIN-8</a> code of the product, or the product to which the offer refers. This code is also known as EAN/UCC-8 or 8-digit EAN. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     */
    @JsonProperty("gtin8")
    @JsonPropertyDescription("")
    private String gtin8;
    /**
     * The height of the item.
     * 
     */
    @JsonProperty("height")
    @JsonPropertyDescription("")
    private java.lang.Object height;
    /**
     * The release date of a product or product model. This can be used to distinguish the exact variant of a product.
     * 
     */
    @JsonProperty("releaseDate")
    @JsonPropertyDescription("")
    private Date releaseDate;
    /**
     * Product
     * <p>
     * Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online.
     * 
     */
    @JsonProperty("isRelatedTo")
    @JsonPropertyDescription("")
    private TypeOfGood isRelatedTo;
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * An associated logo.
     * 
     */
    @JsonProperty("logo")
    @JsonPropertyDescription("")
    private java.lang.Object logo;
    /**
     * The product identifier, such as ISBN. For example: <code>&lt;meta itemprop='productID' content='isbn:123-456-789'/&gt;</code>.
     * 
     */
    @JsonProperty("productID")
    @JsonPropertyDescription("")
    private String productID;
    /**
     * A category for the item. Greater signs or slashes can be used to informally indicate a category hierarchy.
     * 
     */
    @JsonProperty("category")
    @JsonPropertyDescription("")
    private java.lang.Object category;
    /**
     * Product
     * <p>
     * Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online.
     * 
     */
    @JsonProperty("isSimilarTo")
    @JsonPropertyDescription("")
    private TypeOfGood isSimilarTo;
    /**
     * The width of the item.
     * 
     */
    @JsonProperty("width")
    @JsonPropertyDescription("")
    private java.lang.Object width;
    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     */
    @JsonProperty("review")
    @JsonPropertyDescription("")
    private Review review;
    /**
     * Audience
     * <p>
     * Intended audience for an item, i.e. the group for whom the item was created.
     * 
     */
    @JsonProperty("audience")
    @JsonPropertyDescription("")
    private Audience audience;
    /**
     * The color of the product.
     * 
     */
    @JsonProperty("color")
    @JsonPropertyDescription("")
    private String color;
    /**
     * PropertyValue
     * <p>
     * A property-value pair, e.g. representing a feature of a product or place. Use the 'name' property for the name of the property. If there is an additional human-readable version of the value, put that into the 'description' property.
     *         <br/><br/>
     *         Always use specific schema.org properties when a) they exist and b) you can populate them. Using PropertyValue as a substitute will typically not trigger the same effect as using the original, specific property.
     *     
     * 
     */
    @JsonProperty("additionalProperty")
    @JsonPropertyDescription("")
    private AdditionalProperty additionalProperty;
    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     */
    @JsonProperty("offers")
    @JsonPropertyDescription("")
    private Offer offers;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * The date of production of the item, e.g. vehicle.
     * 
     */
    @JsonProperty("productionDate")
    @JsonPropertyDescription("")
    private Date productionDate;
    /**
     * The Stock Keeping Unit (SKU), i.e. a merchant-specific identifier for a product or service, or the product to which the offer refers.
     * 
     */
    @JsonProperty("sku")
    @JsonPropertyDescription("")
    private String sku;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;
    /**
     * The Manufacturer Part Number (MPN) of the product, or the product to which the offer refers.
     * 
     */
    @JsonProperty("mpn")
    @JsonPropertyDescription("")
    private String mpn;
    /**
     * The brand(s) associated with a product or service, or the brand(s) maintained by an organization or business person.
     * 
     */
    @JsonProperty("brand")
    @JsonPropertyDescription("")
    private java.lang.Object brand;
    /**
     * An award won by or for this item.
     * 
     */
    @JsonProperty("award")
    @JsonPropertyDescription("")
    private String award;
    /**
     * Awards won by or for this item.
     * 
     */
    @JsonProperty("awards")
    @JsonPropertyDescription("")
    private String awards;
    /**
     * OfferItemCondition
     * <p>
     * A list of possible conditions for the item.
     * 
     */
    @JsonProperty("itemCondition")
    @JsonPropertyDescription("")
    private ItemCondition itemCondition;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     */
    @JsonProperty("manufacturer")
    @JsonPropertyDescription("")
    private Affiliation manufacturer;
    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;
    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     */
    @JsonProperty("aggregateRating")
    @JsonPropertyDescription("")
    private AggregateRating aggregateRating;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     */
    @JsonProperty("reviews")
    @JsonPropertyDescription("")
    private Review reviews;
    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-14.aspx">GTIN-14</a> code of the product, or the product to which the offer refers. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     */
    @JsonProperty("gtin14")
    @JsonPropertyDescription("")
    private String gtin14;
    /**
     * The depth of the item.
     * 
     */
    @JsonProperty("depth")
    @JsonPropertyDescription("")
    private java.lang.Object depth;
    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-13.aspx">GTIN-13</a> code of the product, or the product to which the offer refers. This is equivalent to 13-digit ISBN codes and EAN UCC-13. Former 12-digit UPC codes can be converted into a GTIN-13 code by simply adding a preceeding zero. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     */
    @JsonProperty("gtin13")
    @JsonPropertyDescription("")
    private String gtin13;
    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-12.aspx">GTIN-12</a> code of the product, or the product to which the offer refers. The GTIN-12 is the 12-digit GS1 Identification Key composed of a U.P.C. Company Prefix, Item Reference, and Check Digit used to identify trade items. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     */
    @JsonProperty("gtin12")
    @JsonPropertyDescription("")
    private String gtin12;
    /**
     * The model of the product. Use with the URL of a ProductModel or a textual representation of the model identifier. The URL of the ProductModel can be from an external source. It is recommended to additionally provide strong product identifiers via the gtin8/gtin13/gtin14 and mpn properties.
     * 
     */
    @JsonProperty("model")
    @JsonPropertyDescription("")
    private java.lang.Object model;

    /**
     * Product
     * <p>
     * Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online.
     * 
     * @return
     *     The isConsumableFor
     */
    @JsonProperty("isConsumableFor")
    public TypeOfGood getIsConsumableFor() {
        return isConsumableFor;
    }

    /**
     * Product
     * <p>
     * Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online.
     * 
     * @param isConsumableFor
     *     The isConsumableFor
     */
    @JsonProperty("isConsumableFor")
    public void setIsConsumableFor(TypeOfGood isConsumableFor) {
        this.isConsumableFor = isConsumableFor;
    }

    public TypeOfGood withIsConsumableFor(TypeOfGood isConsumableFor) {
        this.isConsumableFor = isConsumableFor;
        return this;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @return
     *     The weight
     */
    @JsonProperty("weight")
    public Weight getWeight() {
        return weight;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @param weight
     *     The weight
     */
    @JsonProperty("weight")
    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    public TypeOfGood withWeight(Weight weight) {
        this.weight = weight;
        return this;
    }

    /**
     * Product
     * <p>
     * Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online.
     * 
     * @return
     *     The isAccessoryOrSparePartFor
     */
    @JsonProperty("isAccessoryOrSparePartFor")
    public TypeOfGood getIsAccessoryOrSparePartFor() {
        return isAccessoryOrSparePartFor;
    }

    /**
     * Product
     * <p>
     * Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online.
     * 
     * @param isAccessoryOrSparePartFor
     *     The isAccessoryOrSparePartFor
     */
    @JsonProperty("isAccessoryOrSparePartFor")
    public void setIsAccessoryOrSparePartFor(TypeOfGood isAccessoryOrSparePartFor) {
        this.isAccessoryOrSparePartFor = isAccessoryOrSparePartFor;
    }

    public TypeOfGood withIsAccessoryOrSparePartFor(TypeOfGood isAccessoryOrSparePartFor) {
        this.isAccessoryOrSparePartFor = isAccessoryOrSparePartFor;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public TypeOfGood withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * The date the item e.g. vehicle was purchased by the current owner.
     * 
     * @return
     *     The purchaseDate
     */
    @JsonProperty("purchaseDate")
    public Date getPurchaseDate() {
        return purchaseDate;
    }

    /**
     * The date the item e.g. vehicle was purchased by the current owner.
     * 
     * @param purchaseDate
     *     The purchaseDate
     */
    @JsonProperty("purchaseDate")
    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public TypeOfGood withPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public TypeOfGood withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-8.aspx">GTIN-8</a> code of the product, or the product to which the offer refers. This code is also known as EAN/UCC-8 or 8-digit EAN. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     * @return
     *     The gtin8
     */
    @JsonProperty("gtin8")
    public String getGtin8() {
        return gtin8;
    }

    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-8.aspx">GTIN-8</a> code of the product, or the product to which the offer refers. This code is also known as EAN/UCC-8 or 8-digit EAN. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     * @param gtin8
     *     The gtin8
     */
    @JsonProperty("gtin8")
    public void setGtin8(String gtin8) {
        this.gtin8 = gtin8;
    }

    public TypeOfGood withGtin8(String gtin8) {
        this.gtin8 = gtin8;
        return this;
    }

    /**
     * The height of the item.
     * 
     * @return
     *     The height
     */
    @JsonProperty("height")
    public java.lang.Object getHeight() {
        return height;
    }

    /**
     * The height of the item.
     * 
     * @param height
     *     The height
     */
    @JsonProperty("height")
    public void setHeight(java.lang.Object height) {
        this.height = height;
    }

    public TypeOfGood withHeight(java.lang.Object height) {
        this.height = height;
        return this;
    }

    /**
     * The release date of a product or product model. This can be used to distinguish the exact variant of a product.
     * 
     * @return
     *     The releaseDate
     */
    @JsonProperty("releaseDate")
    public Date getReleaseDate() {
        return releaseDate;
    }

    /**
     * The release date of a product or product model. This can be used to distinguish the exact variant of a product.
     * 
     * @param releaseDate
     *     The releaseDate
     */
    @JsonProperty("releaseDate")
    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public TypeOfGood withReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
        return this;
    }

    /**
     * Product
     * <p>
     * Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online.
     * 
     * @return
     *     The isRelatedTo
     */
    @JsonProperty("isRelatedTo")
    public TypeOfGood getIsRelatedTo() {
        return isRelatedTo;
    }

    /**
     * Product
     * <p>
     * Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online.
     * 
     * @param isRelatedTo
     *     The isRelatedTo
     */
    @JsonProperty("isRelatedTo")
    public void setIsRelatedTo(TypeOfGood isRelatedTo) {
        this.isRelatedTo = isRelatedTo;
    }

    public TypeOfGood withIsRelatedTo(TypeOfGood isRelatedTo) {
        this.isRelatedTo = isRelatedTo;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public TypeOfGood withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * An associated logo.
     * 
     * @return
     *     The logo
     */
    @JsonProperty("logo")
    public java.lang.Object getLogo() {
        return logo;
    }

    /**
     * An associated logo.
     * 
     * @param logo
     *     The logo
     */
    @JsonProperty("logo")
    public void setLogo(java.lang.Object logo) {
        this.logo = logo;
    }

    public TypeOfGood withLogo(java.lang.Object logo) {
        this.logo = logo;
        return this;
    }

    /**
     * The product identifier, such as ISBN. For example: <code>&lt;meta itemprop='productID' content='isbn:123-456-789'/&gt;</code>.
     * 
     * @return
     *     The productID
     */
    @JsonProperty("productID")
    public String getProductID() {
        return productID;
    }

    /**
     * The product identifier, such as ISBN. For example: <code>&lt;meta itemprop='productID' content='isbn:123-456-789'/&gt;</code>.
     * 
     * @param productID
     *     The productID
     */
    @JsonProperty("productID")
    public void setProductID(String productID) {
        this.productID = productID;
    }

    public TypeOfGood withProductID(String productID) {
        this.productID = productID;
        return this;
    }

    /**
     * A category for the item. Greater signs or slashes can be used to informally indicate a category hierarchy.
     * 
     * @return
     *     The category
     */
    @JsonProperty("category")
    public java.lang.Object getCategory() {
        return category;
    }

    /**
     * A category for the item. Greater signs or slashes can be used to informally indicate a category hierarchy.
     * 
     * @param category
     *     The category
     */
    @JsonProperty("category")
    public void setCategory(java.lang.Object category) {
        this.category = category;
    }

    public TypeOfGood withCategory(java.lang.Object category) {
        this.category = category;
        return this;
    }

    /**
     * Product
     * <p>
     * Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online.
     * 
     * @return
     *     The isSimilarTo
     */
    @JsonProperty("isSimilarTo")
    public TypeOfGood getIsSimilarTo() {
        return isSimilarTo;
    }

    /**
     * Product
     * <p>
     * Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online.
     * 
     * @param isSimilarTo
     *     The isSimilarTo
     */
    @JsonProperty("isSimilarTo")
    public void setIsSimilarTo(TypeOfGood isSimilarTo) {
        this.isSimilarTo = isSimilarTo;
    }

    public TypeOfGood withIsSimilarTo(TypeOfGood isSimilarTo) {
        this.isSimilarTo = isSimilarTo;
        return this;
    }

    /**
     * The width of the item.
     * 
     * @return
     *     The width
     */
    @JsonProperty("width")
    public java.lang.Object getWidth() {
        return width;
    }

    /**
     * The width of the item.
     * 
     * @param width
     *     The width
     */
    @JsonProperty("width")
    public void setWidth(java.lang.Object width) {
        this.width = width;
    }

    public TypeOfGood withWidth(java.lang.Object width) {
        this.width = width;
        return this;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @return
     *     The review
     */
    @JsonProperty("review")
    public Review getReview() {
        return review;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @param review
     *     The review
     */
    @JsonProperty("review")
    public void setReview(Review review) {
        this.review = review;
    }

    public TypeOfGood withReview(Review review) {
        this.review = review;
        return this;
    }

    /**
     * Audience
     * <p>
     * Intended audience for an item, i.e. the group for whom the item was created.
     * 
     * @return
     *     The audience
     */
    @JsonProperty("audience")
    public Audience getAudience() {
        return audience;
    }

    /**
     * Audience
     * <p>
     * Intended audience for an item, i.e. the group for whom the item was created.
     * 
     * @param audience
     *     The audience
     */
    @JsonProperty("audience")
    public void setAudience(Audience audience) {
        this.audience = audience;
    }

    public TypeOfGood withAudience(Audience audience) {
        this.audience = audience;
        return this;
    }

    /**
     * The color of the product.
     * 
     * @return
     *     The color
     */
    @JsonProperty("color")
    public String getColor() {
        return color;
    }

    /**
     * The color of the product.
     * 
     * @param color
     *     The color
     */
    @JsonProperty("color")
    public void setColor(String color) {
        this.color = color;
    }

    public TypeOfGood withColor(String color) {
        this.color = color;
        return this;
    }

    /**
     * PropertyValue
     * <p>
     * A property-value pair, e.g. representing a feature of a product or place. Use the 'name' property for the name of the property. If there is an additional human-readable version of the value, put that into the 'description' property.
     *         <br/><br/>
     *         Always use specific schema.org properties when a) they exist and b) you can populate them. Using PropertyValue as a substitute will typically not trigger the same effect as using the original, specific property.
     *     
     * 
     * @return
     *     The additionalProperty
     */
    @JsonProperty("additionalProperty")
    public AdditionalProperty getAdditionalProperty() {
        return additionalProperty;
    }

    /**
     * PropertyValue
     * <p>
     * A property-value pair, e.g. representing a feature of a product or place. Use the 'name' property for the name of the property. If there is an additional human-readable version of the value, put that into the 'description' property.
     *         <br/><br/>
     *         Always use specific schema.org properties when a) they exist and b) you can populate them. Using PropertyValue as a substitute will typically not trigger the same effect as using the original, specific property.
     *     
     * 
     * @param additionalProperty
     *     The additionalProperty
     */
    @JsonProperty("additionalProperty")
    public void setAdditionalProperty(AdditionalProperty additionalProperty) {
        this.additionalProperty = additionalProperty;
    }

    public TypeOfGood withAdditionalProperty(AdditionalProperty additionalProperty) {
        this.additionalProperty = additionalProperty;
        return this;
    }

    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     * @return
     *     The offers
     */
    @JsonProperty("offers")
    public Offer getOffers() {
        return offers;
    }

    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     * @param offers
     *     The offers
     */
    @JsonProperty("offers")
    public void setOffers(Offer offers) {
        this.offers = offers;
    }

    public TypeOfGood withOffers(Offer offers) {
        this.offers = offers;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public TypeOfGood withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * The date of production of the item, e.g. vehicle.
     * 
     * @return
     *     The productionDate
     */
    @JsonProperty("productionDate")
    public Date getProductionDate() {
        return productionDate;
    }

    /**
     * The date of production of the item, e.g. vehicle.
     * 
     * @param productionDate
     *     The productionDate
     */
    @JsonProperty("productionDate")
    public void setProductionDate(Date productionDate) {
        this.productionDate = productionDate;
    }

    public TypeOfGood withProductionDate(Date productionDate) {
        this.productionDate = productionDate;
        return this;
    }

    /**
     * The Stock Keeping Unit (SKU), i.e. a merchant-specific identifier for a product or service, or the product to which the offer refers.
     * 
     * @return
     *     The sku
     */
    @JsonProperty("sku")
    public String getSku() {
        return sku;
    }

    /**
     * The Stock Keeping Unit (SKU), i.e. a merchant-specific identifier for a product or service, or the product to which the offer refers.
     * 
     * @param sku
     *     The sku
     */
    @JsonProperty("sku")
    public void setSku(String sku) {
        this.sku = sku;
    }

    public TypeOfGood withSku(String sku) {
        this.sku = sku;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public TypeOfGood withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * The Manufacturer Part Number (MPN) of the product, or the product to which the offer refers.
     * 
     * @return
     *     The mpn
     */
    @JsonProperty("mpn")
    public String getMpn() {
        return mpn;
    }

    /**
     * The Manufacturer Part Number (MPN) of the product, or the product to which the offer refers.
     * 
     * @param mpn
     *     The mpn
     */
    @JsonProperty("mpn")
    public void setMpn(String mpn) {
        this.mpn = mpn;
    }

    public TypeOfGood withMpn(String mpn) {
        this.mpn = mpn;
        return this;
    }

    /**
     * The brand(s) associated with a product or service, or the brand(s) maintained by an organization or business person.
     * 
     * @return
     *     The brand
     */
    @JsonProperty("brand")
    public java.lang.Object getBrand() {
        return brand;
    }

    /**
     * The brand(s) associated with a product or service, or the brand(s) maintained by an organization or business person.
     * 
     * @param brand
     *     The brand
     */
    @JsonProperty("brand")
    public void setBrand(java.lang.Object brand) {
        this.brand = brand;
    }

    public TypeOfGood withBrand(java.lang.Object brand) {
        this.brand = brand;
        return this;
    }

    /**
     * An award won by or for this item.
     * 
     * @return
     *     The award
     */
    @JsonProperty("award")
    public String getAward() {
        return award;
    }

    /**
     * An award won by or for this item.
     * 
     * @param award
     *     The award
     */
    @JsonProperty("award")
    public void setAward(String award) {
        this.award = award;
    }

    public TypeOfGood withAward(String award) {
        this.award = award;
        return this;
    }

    /**
     * Awards won by or for this item.
     * 
     * @return
     *     The awards
     */
    @JsonProperty("awards")
    public String getAwards() {
        return awards;
    }

    /**
     * Awards won by or for this item.
     * 
     * @param awards
     *     The awards
     */
    @JsonProperty("awards")
    public void setAwards(String awards) {
        this.awards = awards;
    }

    public TypeOfGood withAwards(String awards) {
        this.awards = awards;
        return this;
    }

    /**
     * OfferItemCondition
     * <p>
     * A list of possible conditions for the item.
     * 
     * @return
     *     The itemCondition
     */
    @JsonProperty("itemCondition")
    public ItemCondition getItemCondition() {
        return itemCondition;
    }

    /**
     * OfferItemCondition
     * <p>
     * A list of possible conditions for the item.
     * 
     * @param itemCondition
     *     The itemCondition
     */
    @JsonProperty("itemCondition")
    public void setItemCondition(ItemCondition itemCondition) {
        this.itemCondition = itemCondition;
    }

    public TypeOfGood withItemCondition(ItemCondition itemCondition) {
        this.itemCondition = itemCondition;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public TypeOfGood withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @return
     *     The manufacturer
     */
    @JsonProperty("manufacturer")
    public Affiliation getManufacturer() {
        return manufacturer;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @param manufacturer
     *     The manufacturer
     */
    @JsonProperty("manufacturer")
    public void setManufacturer(Affiliation manufacturer) {
        this.manufacturer = manufacturer;
    }

    public TypeOfGood withManufacturer(Affiliation manufacturer) {
        this.manufacturer = manufacturer;
        return this;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public TypeOfGood withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public TypeOfGood withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     * @return
     *     The aggregateRating
     */
    @JsonProperty("aggregateRating")
    public AggregateRating getAggregateRating() {
        return aggregateRating;
    }

    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     * @param aggregateRating
     *     The aggregateRating
     */
    @JsonProperty("aggregateRating")
    public void setAggregateRating(AggregateRating aggregateRating) {
        this.aggregateRating = aggregateRating;
    }

    public TypeOfGood withAggregateRating(AggregateRating aggregateRating) {
        this.aggregateRating = aggregateRating;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public TypeOfGood withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @return
     *     The reviews
     */
    @JsonProperty("reviews")
    public Review getReviews() {
        return reviews;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @param reviews
     *     The reviews
     */
    @JsonProperty("reviews")
    public void setReviews(Review reviews) {
        this.reviews = reviews;
    }

    public TypeOfGood withReviews(Review reviews) {
        this.reviews = reviews;
        return this;
    }

    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-14.aspx">GTIN-14</a> code of the product, or the product to which the offer refers. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     * @return
     *     The gtin14
     */
    @JsonProperty("gtin14")
    public String getGtin14() {
        return gtin14;
    }

    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-14.aspx">GTIN-14</a> code of the product, or the product to which the offer refers. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     * @param gtin14
     *     The gtin14
     */
    @JsonProperty("gtin14")
    public void setGtin14(String gtin14) {
        this.gtin14 = gtin14;
    }

    public TypeOfGood withGtin14(String gtin14) {
        this.gtin14 = gtin14;
        return this;
    }

    /**
     * The depth of the item.
     * 
     * @return
     *     The depth
     */
    @JsonProperty("depth")
    public java.lang.Object getDepth() {
        return depth;
    }

    /**
     * The depth of the item.
     * 
     * @param depth
     *     The depth
     */
    @JsonProperty("depth")
    public void setDepth(java.lang.Object depth) {
        this.depth = depth;
    }

    public TypeOfGood withDepth(java.lang.Object depth) {
        this.depth = depth;
        return this;
    }

    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-13.aspx">GTIN-13</a> code of the product, or the product to which the offer refers. This is equivalent to 13-digit ISBN codes and EAN UCC-13. Former 12-digit UPC codes can be converted into a GTIN-13 code by simply adding a preceeding zero. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     * @return
     *     The gtin13
     */
    @JsonProperty("gtin13")
    public String getGtin13() {
        return gtin13;
    }

    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-13.aspx">GTIN-13</a> code of the product, or the product to which the offer refers. This is equivalent to 13-digit ISBN codes and EAN UCC-13. Former 12-digit UPC codes can be converted into a GTIN-13 code by simply adding a preceeding zero. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     * @param gtin13
     *     The gtin13
     */
    @JsonProperty("gtin13")
    public void setGtin13(String gtin13) {
        this.gtin13 = gtin13;
    }

    public TypeOfGood withGtin13(String gtin13) {
        this.gtin13 = gtin13;
        return this;
    }

    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-12.aspx">GTIN-12</a> code of the product, or the product to which the offer refers. The GTIN-12 is the 12-digit GS1 Identification Key composed of a U.P.C. Company Prefix, Item Reference, and Check Digit used to identify trade items. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     * @return
     *     The gtin12
     */
    @JsonProperty("gtin12")
    public String getGtin12() {
        return gtin12;
    }

    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-12.aspx">GTIN-12</a> code of the product, or the product to which the offer refers. The GTIN-12 is the 12-digit GS1 Identification Key composed of a U.P.C. Company Prefix, Item Reference, and Check Digit used to identify trade items. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     * @param gtin12
     *     The gtin12
     */
    @JsonProperty("gtin12")
    public void setGtin12(String gtin12) {
        this.gtin12 = gtin12;
    }

    public TypeOfGood withGtin12(String gtin12) {
        this.gtin12 = gtin12;
        return this;
    }

    /**
     * The model of the product. Use with the URL of a ProductModel or a textual representation of the model identifier. The URL of the ProductModel can be from an external source. It is recommended to additionally provide strong product identifiers via the gtin8/gtin13/gtin14 and mpn properties.
     * 
     * @return
     *     The model
     */
    @JsonProperty("model")
    public java.lang.Object getModel() {
        return model;
    }

    /**
     * The model of the product. Use with the URL of a ProductModel or a textual representation of the model identifier. The URL of the ProductModel can be from an external source. It is recommended to additionally provide strong product identifiers via the gtin8/gtin13/gtin14 and mpn properties.
     * 
     * @param model
     *     The model
     */
    @JsonProperty("model")
    public void setModel(java.lang.Object model) {
        this.model = model;
    }

    public TypeOfGood withModel(java.lang.Object model) {
        this.model = model;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(isConsumableFor).append(weight).append(isAccessoryOrSparePartFor).append(sameAs).append(purchaseDate).append(image).append(gtin8).append(height).append(releaseDate).append(isRelatedTo).append(additionalType).append(logo).append(productID).append(category).append(isSimilarTo).append(width).append(review).append(audience).append(color).append(additionalProperty).append(offers).append(mainEntityOfPage).append(productionDate).append(sku).append(description).append(mpn).append(brand).append(award).append(awards).append(itemCondition).append(alternateName).append(manufacturer).append(potentialAction).append(name).append(aggregateRating).append(url).append(reviews).append(gtin14).append(depth).append(gtin13).append(gtin12).append(model).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof TypeOfGood) == false) {
            return false;
        }
        TypeOfGood rhs = ((TypeOfGood) other);
        return new EqualsBuilder().append(isConsumableFor, rhs.isConsumableFor).append(weight, rhs.weight).append(isAccessoryOrSparePartFor, rhs.isAccessoryOrSparePartFor).append(sameAs, rhs.sameAs).append(purchaseDate, rhs.purchaseDate).append(image, rhs.image).append(gtin8, rhs.gtin8).append(height, rhs.height).append(releaseDate, rhs.releaseDate).append(isRelatedTo, rhs.isRelatedTo).append(additionalType, rhs.additionalType).append(logo, rhs.logo).append(productID, rhs.productID).append(category, rhs.category).append(isSimilarTo, rhs.isSimilarTo).append(width, rhs.width).append(review, rhs.review).append(audience, rhs.audience).append(color, rhs.color).append(additionalProperty, rhs.additionalProperty).append(offers, rhs.offers).append(mainEntityOfPage, rhs.mainEntityOfPage).append(productionDate, rhs.productionDate).append(sku, rhs.sku).append(description, rhs.description).append(mpn, rhs.mpn).append(brand, rhs.brand).append(award, rhs.award).append(awards, rhs.awards).append(itemCondition, rhs.itemCondition).append(alternateName, rhs.alternateName).append(manufacturer, rhs.manufacturer).append(potentialAction, rhs.potentialAction).append(name, rhs.name).append(aggregateRating, rhs.aggregateRating).append(url, rhs.url).append(reviews, rhs.reviews).append(gtin14, rhs.gtin14).append(depth, rhs.depth).append(gtin13, rhs.gtin13).append(gtin12, rhs.gtin12).append(model, rhs.model).isEquals();
    }

}
