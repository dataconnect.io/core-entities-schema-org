
package io.dataconnect.model;

import java.net.URI;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * AggregateRating
 * <p>
 * The average rating based on multiple ratings or reviews.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "reviewCount",
    "potentialAction",
    "name",
    "sameAs",
    "description",
    "bestRating",
    "url",
    "worstRating",
    "ratingCount",
    "mainEntityOfPage",
    "additionalType",
    "alternateName",
    "image",
    "itemReviewed",
    "ratingValue"
})
public class AggregateRating {

    /**
     * The count of total number of reviews.
     * 
     */
    @JsonProperty("reviewCount")
    @JsonPropertyDescription("")
    private Double reviewCount;
    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;
    /**
     * The highest value allowed in this rating system. If bestRating is omitted, 5 is assumed.
     * 
     */
    @JsonProperty("bestRating")
    @JsonPropertyDescription("")
    private java.lang.Object bestRating;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * The lowest value allowed in this rating system. If worstRating is omitted, 1 is assumed.
     * 
     */
    @JsonProperty("worstRating")
    @JsonPropertyDescription("")
    private java.lang.Object worstRating;
    /**
     * The count of total number of ratings.
     * 
     */
    @JsonProperty("ratingCount")
    @JsonPropertyDescription("")
    private Double ratingCount;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     */
    @JsonProperty("itemReviewed")
    @JsonPropertyDescription("")
    private io.dataconnect.model.Object itemReviewed;
    /**
     * The rating for the content.
     * 
     */
    @JsonProperty("ratingValue")
    @JsonPropertyDescription("")
    private String ratingValue;

    /**
     * The count of total number of reviews.
     * 
     * @return
     *     The reviewCount
     */
    @JsonProperty("reviewCount")
    public Double getReviewCount() {
        return reviewCount;
    }

    /**
     * The count of total number of reviews.
     * 
     * @param reviewCount
     *     The reviewCount
     */
    @JsonProperty("reviewCount")
    public void setReviewCount(Double reviewCount) {
        this.reviewCount = reviewCount;
    }

    public AggregateRating withReviewCount(Double reviewCount) {
        this.reviewCount = reviewCount;
        return this;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public AggregateRating withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public AggregateRating withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public AggregateRating withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public AggregateRating withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * The highest value allowed in this rating system. If bestRating is omitted, 5 is assumed.
     * 
     * @return
     *     The bestRating
     */
    @JsonProperty("bestRating")
    public java.lang.Object getBestRating() {
        return bestRating;
    }

    /**
     * The highest value allowed in this rating system. If bestRating is omitted, 5 is assumed.
     * 
     * @param bestRating
     *     The bestRating
     */
    @JsonProperty("bestRating")
    public void setBestRating(java.lang.Object bestRating) {
        this.bestRating = bestRating;
    }

    public AggregateRating withBestRating(java.lang.Object bestRating) {
        this.bestRating = bestRating;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public AggregateRating withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * The lowest value allowed in this rating system. If worstRating is omitted, 1 is assumed.
     * 
     * @return
     *     The worstRating
     */
    @JsonProperty("worstRating")
    public java.lang.Object getWorstRating() {
        return worstRating;
    }

    /**
     * The lowest value allowed in this rating system. If worstRating is omitted, 1 is assumed.
     * 
     * @param worstRating
     *     The worstRating
     */
    @JsonProperty("worstRating")
    public void setWorstRating(java.lang.Object worstRating) {
        this.worstRating = worstRating;
    }

    public AggregateRating withWorstRating(java.lang.Object worstRating) {
        this.worstRating = worstRating;
        return this;
    }

    /**
     * The count of total number of ratings.
     * 
     * @return
     *     The ratingCount
     */
    @JsonProperty("ratingCount")
    public Double getRatingCount() {
        return ratingCount;
    }

    /**
     * The count of total number of ratings.
     * 
     * @param ratingCount
     *     The ratingCount
     */
    @JsonProperty("ratingCount")
    public void setRatingCount(Double ratingCount) {
        this.ratingCount = ratingCount;
    }

    public AggregateRating withRatingCount(Double ratingCount) {
        this.ratingCount = ratingCount;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public AggregateRating withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public AggregateRating withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public AggregateRating withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public AggregateRating withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @return
     *     The itemReviewed
     */
    @JsonProperty("itemReviewed")
    public io.dataconnect.model.Object getItemReviewed() {
        return itemReviewed;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @param itemReviewed
     *     The itemReviewed
     */
    @JsonProperty("itemReviewed")
    public void setItemReviewed(io.dataconnect.model.Object itemReviewed) {
        this.itemReviewed = itemReviewed;
    }

    public AggregateRating withItemReviewed(io.dataconnect.model.Object itemReviewed) {
        this.itemReviewed = itemReviewed;
        return this;
    }

    /**
     * The rating for the content.
     * 
     * @return
     *     The ratingValue
     */
    @JsonProperty("ratingValue")
    public String getRatingValue() {
        return ratingValue;
    }

    /**
     * The rating for the content.
     * 
     * @param ratingValue
     *     The ratingValue
     */
    @JsonProperty("ratingValue")
    public void setRatingValue(String ratingValue) {
        this.ratingValue = ratingValue;
    }

    public AggregateRating withRatingValue(String ratingValue) {
        this.ratingValue = ratingValue;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(reviewCount).append(potentialAction).append(name).append(sameAs).append(description).append(bestRating).append(url).append(worstRating).append(ratingCount).append(mainEntityOfPage).append(additionalType).append(alternateName).append(image).append(itemReviewed).append(ratingValue).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AggregateRating) == false) {
            return false;
        }
        AggregateRating rhs = ((AggregateRating) other);
        return new EqualsBuilder().append(reviewCount, rhs.reviewCount).append(potentialAction, rhs.potentialAction).append(name, rhs.name).append(sameAs, rhs.sameAs).append(description, rhs.description).append(bestRating, rhs.bestRating).append(url, rhs.url).append(worstRating, rhs.worstRating).append(ratingCount, rhs.ratingCount).append(mainEntityOfPage, rhs.mainEntityOfPage).append(additionalType, rhs.additionalType).append(alternateName, rhs.alternateName).append(image, rhs.image).append(itemReviewed, rhs.itemReviewed).append(ratingValue, rhs.ratingValue).isEquals();
    }

}
