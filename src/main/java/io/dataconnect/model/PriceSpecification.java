
package io.dataconnect.model;

import java.net.URI;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * PriceSpecification
 * <p>
 * A structured value representing a monetary amount. Typically, only the subclasses of this type are used for markup.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "potentialAction",
    "name",
    "priceCurrency",
    "sameAs",
    "image",
    "validFrom",
    "maxPrice",
    "eligibleTransactionVolume",
    "valueAddedTaxIncluded",
    "eligibleQuantity",
    "url",
    "validThrough",
    "mainEntityOfPage",
    "additionalType",
    "alternateName",
    "minPrice",
    "price",
    "description"
})
public class PriceSpecification {

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;
    /**
     * The currency (in 3-letter ISO 4217 format) of the price or a price component, when attached to PriceSpecification and its subtypes.
     * 
     */
    @JsonProperty("priceCurrency")
    @JsonPropertyDescription("")
    private String priceCurrency;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * The date when the item becomes valid.
     * 
     */
    @JsonProperty("validFrom")
    @JsonPropertyDescription("")
    private Date validFrom;
    /**
     * The highest price if the price is a range.
     * 
     */
    @JsonProperty("maxPrice")
    @JsonPropertyDescription("")
    private Double maxPrice;
    /**
     * PriceSpecification
     * <p>
     * A structured value representing a monetary amount. Typically, only the subclasses of this type are used for markup.
     * 
     */
    @JsonProperty("eligibleTransactionVolume")
    @JsonPropertyDescription("")
    private PriceSpecification eligibleTransactionVolume;
    /**
     * Specifies whether the applicable value-added tax (VAT) is included in the price specification or not.
     * 
     */
    @JsonProperty("valueAddedTaxIncluded")
    @JsonPropertyDescription("")
    private Boolean valueAddedTaxIncluded;
    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     */
    @JsonProperty("eligibleQuantity")
    @JsonPropertyDescription("")
    private Weight eligibleQuantity;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * The end of the validity of offer, price specification, or opening hours data.
     * 
     */
    @JsonProperty("validThrough")
    @JsonPropertyDescription("")
    private Date validThrough;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * The lowest price if the price is a range.
     * 
     */
    @JsonProperty("minPrice")
    @JsonPropertyDescription("")
    private Double minPrice;
    /**
     * The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.
     * <br />
     * <br />
     *       Usage guidelines:
     * <br />
     * <ul>
     * <li>Use the <a href="/priceCurrency">priceCurrency</a> property (with <a href="http://en.wikipedia.org/wiki/ISO_4217#Active_codes">ISO 4217 codes</a> e.g. "USD") instead of
     *       including <a href="http://en.wikipedia.org/wiki/Dollar_sign#Currencies_that_use_the_dollar_or_peso_sign">ambiguous symbols</a> such as '$' in the value.
     * </li>
     * <li>
     *       Use '.' (Unicode 'FULL STOP' (U+002E)) rather than ',' to indicate a decimal point. Avoid using these symbols as a readability separator.
     * </li>
     * <li>
     *       Note that both <a href="http://www.w3.org/TR/xhtml-rdfa-primer/#using-the-content-attribute">RDFa</a> and Microdata syntax allow the use of a "content=" attribute for publishing simple machine-readable values
     *       alongside more human-friendly formatting.
     * </li>
     * <li>
     *       Use values from 0123456789 (Unicode 'DIGIT ZERO' (U+0030) to 'DIGIT NINE' (U+0039)) rather than superficially similiar Unicode symbols.
     * </li>
     * </ul>
     *       
     * 
     */
    @JsonProperty("price")
    @JsonPropertyDescription("")
    private java.lang.Object price;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public PriceSpecification withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public PriceSpecification withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * The currency (in 3-letter ISO 4217 format) of the price or a price component, when attached to PriceSpecification and its subtypes.
     * 
     * @return
     *     The priceCurrency
     */
    @JsonProperty("priceCurrency")
    public String getPriceCurrency() {
        return priceCurrency;
    }

    /**
     * The currency (in 3-letter ISO 4217 format) of the price or a price component, when attached to PriceSpecification and its subtypes.
     * 
     * @param priceCurrency
     *     The priceCurrency
     */
    @JsonProperty("priceCurrency")
    public void setPriceCurrency(String priceCurrency) {
        this.priceCurrency = priceCurrency;
    }

    public PriceSpecification withPriceCurrency(String priceCurrency) {
        this.priceCurrency = priceCurrency;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public PriceSpecification withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public PriceSpecification withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * The date when the item becomes valid.
     * 
     * @return
     *     The validFrom
     */
    @JsonProperty("validFrom")
    public Date getValidFrom() {
        return validFrom;
    }

    /**
     * The date when the item becomes valid.
     * 
     * @param validFrom
     *     The validFrom
     */
    @JsonProperty("validFrom")
    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public PriceSpecification withValidFrom(Date validFrom) {
        this.validFrom = validFrom;
        return this;
    }

    /**
     * The highest price if the price is a range.
     * 
     * @return
     *     The maxPrice
     */
    @JsonProperty("maxPrice")
    public Double getMaxPrice() {
        return maxPrice;
    }

    /**
     * The highest price if the price is a range.
     * 
     * @param maxPrice
     *     The maxPrice
     */
    @JsonProperty("maxPrice")
    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public PriceSpecification withMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
        return this;
    }

    /**
     * PriceSpecification
     * <p>
     * A structured value representing a monetary amount. Typically, only the subclasses of this type are used for markup.
     * 
     * @return
     *     The eligibleTransactionVolume
     */
    @JsonProperty("eligibleTransactionVolume")
    public PriceSpecification getEligibleTransactionVolume() {
        return eligibleTransactionVolume;
    }

    /**
     * PriceSpecification
     * <p>
     * A structured value representing a monetary amount. Typically, only the subclasses of this type are used for markup.
     * 
     * @param eligibleTransactionVolume
     *     The eligibleTransactionVolume
     */
    @JsonProperty("eligibleTransactionVolume")
    public void setEligibleTransactionVolume(PriceSpecification eligibleTransactionVolume) {
        this.eligibleTransactionVolume = eligibleTransactionVolume;
    }

    public PriceSpecification withEligibleTransactionVolume(PriceSpecification eligibleTransactionVolume) {
        this.eligibleTransactionVolume = eligibleTransactionVolume;
        return this;
    }

    /**
     * Specifies whether the applicable value-added tax (VAT) is included in the price specification or not.
     * 
     * @return
     *     The valueAddedTaxIncluded
     */
    @JsonProperty("valueAddedTaxIncluded")
    public Boolean getValueAddedTaxIncluded() {
        return valueAddedTaxIncluded;
    }

    /**
     * Specifies whether the applicable value-added tax (VAT) is included in the price specification or not.
     * 
     * @param valueAddedTaxIncluded
     *     The valueAddedTaxIncluded
     */
    @JsonProperty("valueAddedTaxIncluded")
    public void setValueAddedTaxIncluded(Boolean valueAddedTaxIncluded) {
        this.valueAddedTaxIncluded = valueAddedTaxIncluded;
    }

    public PriceSpecification withValueAddedTaxIncluded(Boolean valueAddedTaxIncluded) {
        this.valueAddedTaxIncluded = valueAddedTaxIncluded;
        return this;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @return
     *     The eligibleQuantity
     */
    @JsonProperty("eligibleQuantity")
    public Weight getEligibleQuantity() {
        return eligibleQuantity;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @param eligibleQuantity
     *     The eligibleQuantity
     */
    @JsonProperty("eligibleQuantity")
    public void setEligibleQuantity(Weight eligibleQuantity) {
        this.eligibleQuantity = eligibleQuantity;
    }

    public PriceSpecification withEligibleQuantity(Weight eligibleQuantity) {
        this.eligibleQuantity = eligibleQuantity;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public PriceSpecification withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * The end of the validity of offer, price specification, or opening hours data.
     * 
     * @return
     *     The validThrough
     */
    @JsonProperty("validThrough")
    public Date getValidThrough() {
        return validThrough;
    }

    /**
     * The end of the validity of offer, price specification, or opening hours data.
     * 
     * @param validThrough
     *     The validThrough
     */
    @JsonProperty("validThrough")
    public void setValidThrough(Date validThrough) {
        this.validThrough = validThrough;
    }

    public PriceSpecification withValidThrough(Date validThrough) {
        this.validThrough = validThrough;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public PriceSpecification withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public PriceSpecification withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public PriceSpecification withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * The lowest price if the price is a range.
     * 
     * @return
     *     The minPrice
     */
    @JsonProperty("minPrice")
    public Double getMinPrice() {
        return minPrice;
    }

    /**
     * The lowest price if the price is a range.
     * 
     * @param minPrice
     *     The minPrice
     */
    @JsonProperty("minPrice")
    public void setMinPrice(Double minPrice) {
        this.minPrice = minPrice;
    }

    public PriceSpecification withMinPrice(Double minPrice) {
        this.minPrice = minPrice;
        return this;
    }

    /**
     * The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.
     * <br />
     * <br />
     *       Usage guidelines:
     * <br />
     * <ul>
     * <li>Use the <a href="/priceCurrency">priceCurrency</a> property (with <a href="http://en.wikipedia.org/wiki/ISO_4217#Active_codes">ISO 4217 codes</a> e.g. "USD") instead of
     *       including <a href="http://en.wikipedia.org/wiki/Dollar_sign#Currencies_that_use_the_dollar_or_peso_sign">ambiguous symbols</a> such as '$' in the value.
     * </li>
     * <li>
     *       Use '.' (Unicode 'FULL STOP' (U+002E)) rather than ',' to indicate a decimal point. Avoid using these symbols as a readability separator.
     * </li>
     * <li>
     *       Note that both <a href="http://www.w3.org/TR/xhtml-rdfa-primer/#using-the-content-attribute">RDFa</a> and Microdata syntax allow the use of a "content=" attribute for publishing simple machine-readable values
     *       alongside more human-friendly formatting.
     * </li>
     * <li>
     *       Use values from 0123456789 (Unicode 'DIGIT ZERO' (U+0030) to 'DIGIT NINE' (U+0039)) rather than superficially similiar Unicode symbols.
     * </li>
     * </ul>
     *       
     * 
     * @return
     *     The price
     */
    @JsonProperty("price")
    public java.lang.Object getPrice() {
        return price;
    }

    /**
     * The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.
     * <br />
     * <br />
     *       Usage guidelines:
     * <br />
     * <ul>
     * <li>Use the <a href="/priceCurrency">priceCurrency</a> property (with <a href="http://en.wikipedia.org/wiki/ISO_4217#Active_codes">ISO 4217 codes</a> e.g. "USD") instead of
     *       including <a href="http://en.wikipedia.org/wiki/Dollar_sign#Currencies_that_use_the_dollar_or_peso_sign">ambiguous symbols</a> such as '$' in the value.
     * </li>
     * <li>
     *       Use '.' (Unicode 'FULL STOP' (U+002E)) rather than ',' to indicate a decimal point. Avoid using these symbols as a readability separator.
     * </li>
     * <li>
     *       Note that both <a href="http://www.w3.org/TR/xhtml-rdfa-primer/#using-the-content-attribute">RDFa</a> and Microdata syntax allow the use of a "content=" attribute for publishing simple machine-readable values
     *       alongside more human-friendly formatting.
     * </li>
     * <li>
     *       Use values from 0123456789 (Unicode 'DIGIT ZERO' (U+0030) to 'DIGIT NINE' (U+0039)) rather than superficially similiar Unicode symbols.
     * </li>
     * </ul>
     *       
     * 
     * @param price
     *     The price
     */
    @JsonProperty("price")
    public void setPrice(java.lang.Object price) {
        this.price = price;
    }

    public PriceSpecification withPrice(java.lang.Object price) {
        this.price = price;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public PriceSpecification withDescription(String description) {
        this.description = description;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(potentialAction).append(name).append(priceCurrency).append(sameAs).append(image).append(validFrom).append(maxPrice).append(eligibleTransactionVolume).append(valueAddedTaxIncluded).append(eligibleQuantity).append(url).append(validThrough).append(mainEntityOfPage).append(additionalType).append(alternateName).append(minPrice).append(price).append(description).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PriceSpecification) == false) {
            return false;
        }
        PriceSpecification rhs = ((PriceSpecification) other);
        return new EqualsBuilder().append(potentialAction, rhs.potentialAction).append(name, rhs.name).append(priceCurrency, rhs.priceCurrency).append(sameAs, rhs.sameAs).append(image, rhs.image).append(validFrom, rhs.validFrom).append(maxPrice, rhs.maxPrice).append(eligibleTransactionVolume, rhs.eligibleTransactionVolume).append(valueAddedTaxIncluded, rhs.valueAddedTaxIncluded).append(eligibleQuantity, rhs.eligibleQuantity).append(url, rhs.url).append(validThrough, rhs.validThrough).append(mainEntityOfPage, rhs.mainEntityOfPage).append(additionalType, rhs.additionalType).append(alternateName, rhs.alternateName).append(minPrice, rhs.minPrice).append(price, rhs.price).append(description, rhs.description).isEquals();
    }

}
