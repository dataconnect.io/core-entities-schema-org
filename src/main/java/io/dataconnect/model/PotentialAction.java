
package io.dataconnect.model;

import java.net.URI;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Action
 * <p>
 * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
 *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
 *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "potentialAction",
    "participant",
    "name",
    "sameAs",
    "description",
    "object",
    "agent",
    "actionStatus",
    "instrument",
    "url",
    "location",
    "startTime",
    "target",
    "mainEntityOfPage",
    "additionalType",
    "alternateName",
    "endTime",
    "error",
    "image",
    "result"
})
public class PotentialAction {

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * Other co-agents that participated in the action indirectly. e.g. John wrote a book with *Steve*.
     * 
     */
    @JsonProperty("participant")
    @JsonPropertyDescription("")
    private java.lang.Object participant;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;
    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     */
    @JsonProperty("object")
    @JsonPropertyDescription("")
    private io.dataconnect.model.Object object;
    /**
     * The direct performer or driver of the action (animate or inanimate). e.g. *John* wrote a book.
     * 
     */
    @JsonProperty("agent")
    @JsonPropertyDescription("")
    private java.lang.Object agent;
    /**
     * ActionStatusType
     * <p>
     * The status of an Action.
     * 
     */
    @JsonProperty("actionStatus")
    @JsonPropertyDescription("")
    private ActionStatus actionStatus;
    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     */
    @JsonProperty("instrument")
    @JsonPropertyDescription("")
    private io.dataconnect.model.Object instrument;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * The location of for example where the event is happening, an organization is located, or where an action takes place.
     * 
     */
    @JsonProperty("location")
    @JsonPropertyDescription("")
    private java.lang.Object location;
    /**
     * The startTime of something. For a reserved event or service (e.g. FoodEstablishmentReservation), the time that it is expected to start. For actions that span a period of time, when the action was performed. e.g. John wrote a book from *January* to December.
     * 
     * Note that Event uses startDate/endDate instead of startTime/endTime, even when describing dates with times. This situation may be clarified in future revisions.
     * 
     */
    @JsonProperty("startTime")
    @JsonPropertyDescription("")
    private Date startTime;
    /**
     * EntryPoint
     * <p>
     * An entry point, within some Web-based protocol.
     * 
     */
    @JsonProperty("target")
    @JsonPropertyDescription("")
    private Target target;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * The endTime of something. For a reserved event or service (e.g. FoodEstablishmentReservation), the time that it is expected to end. For actions that span a period of time, when the action was performed. e.g. John wrote a book from January to *December*.
     * 
     * Note that Event uses startDate/endDate instead of startTime/endTime, even when describing dates with times. This situation may be clarified in future revisions.
     * 
     */
    @JsonProperty("endTime")
    @JsonPropertyDescription("")
    private Date endTime;
    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     */
    @JsonProperty("error")
    @JsonPropertyDescription("")
    private io.dataconnect.model.Object error;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     */
    @JsonProperty("result")
    @JsonPropertyDescription("")
    private io.dataconnect.model.Object result;

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public PotentialAction withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * Other co-agents that participated in the action indirectly. e.g. John wrote a book with *Steve*.
     * 
     * @return
     *     The participant
     */
    @JsonProperty("participant")
    public java.lang.Object getParticipant() {
        return participant;
    }

    /**
     * Other co-agents that participated in the action indirectly. e.g. John wrote a book with *Steve*.
     * 
     * @param participant
     *     The participant
     */
    @JsonProperty("participant")
    public void setParticipant(java.lang.Object participant) {
        this.participant = participant;
    }

    public PotentialAction withParticipant(java.lang.Object participant) {
        this.participant = participant;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public PotentialAction withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public PotentialAction withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public PotentialAction withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @return
     *     The object
     */
    @JsonProperty("object")
    public io.dataconnect.model.Object getObject() {
        return object;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @param object
     *     The object
     */
    @JsonProperty("object")
    public void setObject(io.dataconnect.model.Object object) {
        this.object = object;
    }

    public PotentialAction withObject(io.dataconnect.model.Object object) {
        this.object = object;
        return this;
    }

    /**
     * The direct performer or driver of the action (animate or inanimate). e.g. *John* wrote a book.
     * 
     * @return
     *     The agent
     */
    @JsonProperty("agent")
    public java.lang.Object getAgent() {
        return agent;
    }

    /**
     * The direct performer or driver of the action (animate or inanimate). e.g. *John* wrote a book.
     * 
     * @param agent
     *     The agent
     */
    @JsonProperty("agent")
    public void setAgent(java.lang.Object agent) {
        this.agent = agent;
    }

    public PotentialAction withAgent(java.lang.Object agent) {
        this.agent = agent;
        return this;
    }

    /**
     * ActionStatusType
     * <p>
     * The status of an Action.
     * 
     * @return
     *     The actionStatus
     */
    @JsonProperty("actionStatus")
    public ActionStatus getActionStatus() {
        return actionStatus;
    }

    /**
     * ActionStatusType
     * <p>
     * The status of an Action.
     * 
     * @param actionStatus
     *     The actionStatus
     */
    @JsonProperty("actionStatus")
    public void setActionStatus(ActionStatus actionStatus) {
        this.actionStatus = actionStatus;
    }

    public PotentialAction withActionStatus(ActionStatus actionStatus) {
        this.actionStatus = actionStatus;
        return this;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @return
     *     The instrument
     */
    @JsonProperty("instrument")
    public io.dataconnect.model.Object getInstrument() {
        return instrument;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @param instrument
     *     The instrument
     */
    @JsonProperty("instrument")
    public void setInstrument(io.dataconnect.model.Object instrument) {
        this.instrument = instrument;
    }

    public PotentialAction withInstrument(io.dataconnect.model.Object instrument) {
        this.instrument = instrument;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public PotentialAction withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * The location of for example where the event is happening, an organization is located, or where an action takes place.
     * 
     * @return
     *     The location
     */
    @JsonProperty("location")
    public java.lang.Object getLocation() {
        return location;
    }

    /**
     * The location of for example where the event is happening, an organization is located, or where an action takes place.
     * 
     * @param location
     *     The location
     */
    @JsonProperty("location")
    public void setLocation(java.lang.Object location) {
        this.location = location;
    }

    public PotentialAction withLocation(java.lang.Object location) {
        this.location = location;
        return this;
    }

    /**
     * The startTime of something. For a reserved event or service (e.g. FoodEstablishmentReservation), the time that it is expected to start. For actions that span a period of time, when the action was performed. e.g. John wrote a book from *January* to December.
     * 
     * Note that Event uses startDate/endDate instead of startTime/endTime, even when describing dates with times. This situation may be clarified in future revisions.
     * 
     * @return
     *     The startTime
     */
    @JsonProperty("startTime")
    public Date getStartTime() {
        return startTime;
    }

    /**
     * The startTime of something. For a reserved event or service (e.g. FoodEstablishmentReservation), the time that it is expected to start. For actions that span a period of time, when the action was performed. e.g. John wrote a book from *January* to December.
     * 
     * Note that Event uses startDate/endDate instead of startTime/endTime, even when describing dates with times. This situation may be clarified in future revisions.
     * 
     * @param startTime
     *     The startTime
     */
    @JsonProperty("startTime")
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public PotentialAction withStartTime(Date startTime) {
        this.startTime = startTime;
        return this;
    }

    /**
     * EntryPoint
     * <p>
     * An entry point, within some Web-based protocol.
     * 
     * @return
     *     The target
     */
    @JsonProperty("target")
    public Target getTarget() {
        return target;
    }

    /**
     * EntryPoint
     * <p>
     * An entry point, within some Web-based protocol.
     * 
     * @param target
     *     The target
     */
    @JsonProperty("target")
    public void setTarget(Target target) {
        this.target = target;
    }

    public PotentialAction withTarget(Target target) {
        this.target = target;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public PotentialAction withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public PotentialAction withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public PotentialAction withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * The endTime of something. For a reserved event or service (e.g. FoodEstablishmentReservation), the time that it is expected to end. For actions that span a period of time, when the action was performed. e.g. John wrote a book from January to *December*.
     * 
     * Note that Event uses startDate/endDate instead of startTime/endTime, even when describing dates with times. This situation may be clarified in future revisions.
     * 
     * @return
     *     The endTime
     */
    @JsonProperty("endTime")
    public Date getEndTime() {
        return endTime;
    }

    /**
     * The endTime of something. For a reserved event or service (e.g. FoodEstablishmentReservation), the time that it is expected to end. For actions that span a period of time, when the action was performed. e.g. John wrote a book from January to *December*.
     * 
     * Note that Event uses startDate/endDate instead of startTime/endTime, even when describing dates with times. This situation may be clarified in future revisions.
     * 
     * @param endTime
     *     The endTime
     */
    @JsonProperty("endTime")
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public PotentialAction withEndTime(Date endTime) {
        this.endTime = endTime;
        return this;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @return
     *     The error
     */
    @JsonProperty("error")
    public io.dataconnect.model.Object getError() {
        return error;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @param error
     *     The error
     */
    @JsonProperty("error")
    public void setError(io.dataconnect.model.Object error) {
        this.error = error;
    }

    public PotentialAction withError(io.dataconnect.model.Object error) {
        this.error = error;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public PotentialAction withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @return
     *     The result
     */
    @JsonProperty("result")
    public io.dataconnect.model.Object getResult() {
        return result;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @param result
     *     The result
     */
    @JsonProperty("result")
    public void setResult(io.dataconnect.model.Object result) {
        this.result = result;
    }

    public PotentialAction withResult(io.dataconnect.model.Object result) {
        this.result = result;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(potentialAction).append(participant).append(name).append(sameAs).append(description).append(object).append(agent).append(actionStatus).append(instrument).append(url).append(location).append(startTime).append(target).append(mainEntityOfPage).append(additionalType).append(alternateName).append(endTime).append(error).append(image).append(result).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PotentialAction) == false) {
            return false;
        }
        PotentialAction rhs = ((PotentialAction) other);
        return new EqualsBuilder().append(potentialAction, rhs.potentialAction).append(participant, rhs.participant).append(name, rhs.name).append(sameAs, rhs.sameAs).append(description, rhs.description).append(object, rhs.object).append(agent, rhs.agent).append(actionStatus, rhs.actionStatus).append(instrument, rhs.instrument).append(url, rhs.url).append(location, rhs.location).append(startTime, rhs.startTime).append(target, rhs.target).append(mainEntityOfPage, rhs.mainEntityOfPage).append(additionalType, rhs.additionalType).append(alternateName, rhs.alternateName).append(endTime, rhs.endTime).append(error, rhs.error).append(image, rhs.image).append(result, rhs.result).isEquals();
    }

}
