
package io.dataconnect.model;

import java.net.URI;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * EntryPoint
 * <p>
 * An entry point, within some Web-based protocol.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "potentialAction",
    "contentType",
    "actionPlatform",
    "httpMethod",
    "sameAs",
    "actionApplication",
    "description",
    "application",
    "url",
    "urlTemplate",
    "mainEntityOfPage",
    "additionalType",
    "alternateName",
    "encodingType",
    "image",
    "name"
})
public class Target {

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * The supported content type(s) for an EntryPoint response.
     * 
     */
    @JsonProperty("contentType")
    @JsonPropertyDescription("")
    private String contentType;
    /**
     * The high level platform(s) where the Action can be performed for the given URL. To specify a specific application or operating system instance, use actionApplication.
     * 
     */
    @JsonProperty("actionPlatform")
    @JsonPropertyDescription("")
    private java.lang.Object actionPlatform;
    /**
     * An HTTP method that specifies the appropriate HTTP method for a request to an HTTP EntryPoint. Values are capitalized strings as used in HTTP.
     * 
     */
    @JsonProperty("httpMethod")
    @JsonPropertyDescription("")
    private String httpMethod;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * SoftwareApplication
     * <p>
     * A software application.
     * 
     */
    @JsonProperty("actionApplication")
    @JsonPropertyDescription("")
    private ActionApplication actionApplication;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;
    /**
     * SoftwareApplication
     * <p>
     * A software application.
     * 
     */
    @JsonProperty("application")
    @JsonPropertyDescription("")
    private ActionApplication application;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * A url template (RFC6570) that will be used to construct the target of the execution of the action.
     * 
     */
    @JsonProperty("urlTemplate")
    @JsonPropertyDescription("")
    private String urlTemplate;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * The supported encoding type(s) for an EntryPoint request.
     * 
     */
    @JsonProperty("encodingType")
    @JsonPropertyDescription("")
    private String encodingType;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public Target withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * The supported content type(s) for an EntryPoint response.
     * 
     * @return
     *     The contentType
     */
    @JsonProperty("contentType")
    public String getContentType() {
        return contentType;
    }

    /**
     * The supported content type(s) for an EntryPoint response.
     * 
     * @param contentType
     *     The contentType
     */
    @JsonProperty("contentType")
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Target withContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    /**
     * The high level platform(s) where the Action can be performed for the given URL. To specify a specific application or operating system instance, use actionApplication.
     * 
     * @return
     *     The actionPlatform
     */
    @JsonProperty("actionPlatform")
    public java.lang.Object getActionPlatform() {
        return actionPlatform;
    }

    /**
     * The high level platform(s) where the Action can be performed for the given URL. To specify a specific application or operating system instance, use actionApplication.
     * 
     * @param actionPlatform
     *     The actionPlatform
     */
    @JsonProperty("actionPlatform")
    public void setActionPlatform(java.lang.Object actionPlatform) {
        this.actionPlatform = actionPlatform;
    }

    public Target withActionPlatform(java.lang.Object actionPlatform) {
        this.actionPlatform = actionPlatform;
        return this;
    }

    /**
     * An HTTP method that specifies the appropriate HTTP method for a request to an HTTP EntryPoint. Values are capitalized strings as used in HTTP.
     * 
     * @return
     *     The httpMethod
     */
    @JsonProperty("httpMethod")
    public String getHttpMethod() {
        return httpMethod;
    }

    /**
     * An HTTP method that specifies the appropriate HTTP method for a request to an HTTP EntryPoint. Values are capitalized strings as used in HTTP.
     * 
     * @param httpMethod
     *     The httpMethod
     */
    @JsonProperty("httpMethod")
    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public Target withHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public Target withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * SoftwareApplication
     * <p>
     * A software application.
     * 
     * @return
     *     The actionApplication
     */
    @JsonProperty("actionApplication")
    public ActionApplication getActionApplication() {
        return actionApplication;
    }

    /**
     * SoftwareApplication
     * <p>
     * A software application.
     * 
     * @param actionApplication
     *     The actionApplication
     */
    @JsonProperty("actionApplication")
    public void setActionApplication(ActionApplication actionApplication) {
        this.actionApplication = actionApplication;
    }

    public Target withActionApplication(ActionApplication actionApplication) {
        this.actionApplication = actionApplication;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public Target withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * SoftwareApplication
     * <p>
     * A software application.
     * 
     * @return
     *     The application
     */
    @JsonProperty("application")
    public ActionApplication getApplication() {
        return application;
    }

    /**
     * SoftwareApplication
     * <p>
     * A software application.
     * 
     * @param application
     *     The application
     */
    @JsonProperty("application")
    public void setApplication(ActionApplication application) {
        this.application = application;
    }

    public Target withApplication(ActionApplication application) {
        this.application = application;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public Target withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * A url template (RFC6570) that will be used to construct the target of the execution of the action.
     * 
     * @return
     *     The urlTemplate
     */
    @JsonProperty("urlTemplate")
    public String getUrlTemplate() {
        return urlTemplate;
    }

    /**
     * A url template (RFC6570) that will be used to construct the target of the execution of the action.
     * 
     * @param urlTemplate
     *     The urlTemplate
     */
    @JsonProperty("urlTemplate")
    public void setUrlTemplate(String urlTemplate) {
        this.urlTemplate = urlTemplate;
    }

    public Target withUrlTemplate(String urlTemplate) {
        this.urlTemplate = urlTemplate;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public Target withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public Target withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public Target withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * The supported encoding type(s) for an EntryPoint request.
     * 
     * @return
     *     The encodingType
     */
    @JsonProperty("encodingType")
    public String getEncodingType() {
        return encodingType;
    }

    /**
     * The supported encoding type(s) for an EntryPoint request.
     * 
     * @param encodingType
     *     The encodingType
     */
    @JsonProperty("encodingType")
    public void setEncodingType(String encodingType) {
        this.encodingType = encodingType;
    }

    public Target withEncodingType(String encodingType) {
        this.encodingType = encodingType;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public Target withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Target withName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(potentialAction).append(contentType).append(actionPlatform).append(httpMethod).append(sameAs).append(actionApplication).append(description).append(application).append(url).append(urlTemplate).append(mainEntityOfPage).append(additionalType).append(alternateName).append(encodingType).append(image).append(name).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Target) == false) {
            return false;
        }
        Target rhs = ((Target) other);
        return new EqualsBuilder().append(potentialAction, rhs.potentialAction).append(contentType, rhs.contentType).append(actionPlatform, rhs.actionPlatform).append(httpMethod, rhs.httpMethod).append(sameAs, rhs.sameAs).append(actionApplication, rhs.actionApplication).append(description, rhs.description).append(application, rhs.application).append(url, rhs.url).append(urlTemplate, rhs.urlTemplate).append(mainEntityOfPage, rhs.mainEntityOfPage).append(additionalType, rhs.additionalType).append(alternateName, rhs.alternateName).append(encodingType, rhs.encodingType).append(image, rhs.image).append(name, rhs.name).isEquals();
    }

}
