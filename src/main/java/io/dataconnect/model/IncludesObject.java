
package io.dataconnect.model;

import java.net.URI;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * TypeAndQuantityNode
 * <p>
 * A structured value indicating the quantity, unit of measurement, and business function of goods included in a bundle offer.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "potentialAction",
    "description",
    "sameAs",
    "image",
    "additionalType",
    "url",
    "amountOfThisGood",
    "unitText",
    "mainEntityOfPage",
    "unitCode",
    "alternateName",
    "businessFunction",
    "typeOfGood",
    "name"
})
public class IncludesObject {

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * The quantity of the goods included in the offer.
     * 
     */
    @JsonProperty("amountOfThisGood")
    @JsonPropertyDescription("")
    private Double amountOfThisGood;
    /**
     * A string or text indicating the unit of measurement. Useful if you cannot provide a standard unit code for
     * <a href='unitCode'>unitCode</a>.
     * 
     */
    @JsonProperty("unitText")
    @JsonPropertyDescription("")
    private String unitText;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * The unit of measurement given using the UN/CEFACT Common Code (3 characters) or a URL. Other codes than the UN/CEFACT Common Code may be used with a prefix followed by a colon.
     * 
     */
    @JsonProperty("unitCode")
    @JsonPropertyDescription("")
    private java.lang.Object unitCode;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * BusinessFunction
     * <p>
     * The business function specifies the type of activity or access (i.e., the bundle of rights) offered by the organization or business person through the offer. Typical are sell, rental or lease, maintenance or repair, manufacture / produce, recycle / dispose, engineering / construction, or installation. Proprietary specifications of access rights are also instances of this class.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#ConstructionInstallation <br />
     *     http://purl.org/goodrelations/v1#Dispose <br />
     *     http://purl.org/goodrelations/v1#LeaseOut <br />
     *     http://purl.org/goodrelations/v1#Maintain <br />
     *     http://purl.org/goodrelations/v1#ProvideService <br />
     *     http://purl.org/goodrelations/v1#Repair <br />
     *     http://purl.org/goodrelations/v1#Sell <br />
     *     http://purl.org/goodrelations/v1#Buy <br />
     *         
     * 
     */
    @JsonProperty("businessFunction")
    @JsonPropertyDescription("")
    private BusinessFunction businessFunction;
    /**
     * Product
     * <p>
     * Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online.
     * 
     */
    @JsonProperty("typeOfGood")
    @JsonPropertyDescription("")
    private TypeOfGood typeOfGood;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public IncludesObject withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public IncludesObject withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public IncludesObject withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public IncludesObject withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public IncludesObject withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public IncludesObject withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * The quantity of the goods included in the offer.
     * 
     * @return
     *     The amountOfThisGood
     */
    @JsonProperty("amountOfThisGood")
    public Double getAmountOfThisGood() {
        return amountOfThisGood;
    }

    /**
     * The quantity of the goods included in the offer.
     * 
     * @param amountOfThisGood
     *     The amountOfThisGood
     */
    @JsonProperty("amountOfThisGood")
    public void setAmountOfThisGood(Double amountOfThisGood) {
        this.amountOfThisGood = amountOfThisGood;
    }

    public IncludesObject withAmountOfThisGood(Double amountOfThisGood) {
        this.amountOfThisGood = amountOfThisGood;
        return this;
    }

    /**
     * A string or text indicating the unit of measurement. Useful if you cannot provide a standard unit code for
     * <a href='unitCode'>unitCode</a>.
     * 
     * @return
     *     The unitText
     */
    @JsonProperty("unitText")
    public String getUnitText() {
        return unitText;
    }

    /**
     * A string or text indicating the unit of measurement. Useful if you cannot provide a standard unit code for
     * <a href='unitCode'>unitCode</a>.
     * 
     * @param unitText
     *     The unitText
     */
    @JsonProperty("unitText")
    public void setUnitText(String unitText) {
        this.unitText = unitText;
    }

    public IncludesObject withUnitText(String unitText) {
        this.unitText = unitText;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public IncludesObject withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * The unit of measurement given using the UN/CEFACT Common Code (3 characters) or a URL. Other codes than the UN/CEFACT Common Code may be used with a prefix followed by a colon.
     * 
     * @return
     *     The unitCode
     */
    @JsonProperty("unitCode")
    public java.lang.Object getUnitCode() {
        return unitCode;
    }

    /**
     * The unit of measurement given using the UN/CEFACT Common Code (3 characters) or a URL. Other codes than the UN/CEFACT Common Code may be used with a prefix followed by a colon.
     * 
     * @param unitCode
     *     The unitCode
     */
    @JsonProperty("unitCode")
    public void setUnitCode(java.lang.Object unitCode) {
        this.unitCode = unitCode;
    }

    public IncludesObject withUnitCode(java.lang.Object unitCode) {
        this.unitCode = unitCode;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public IncludesObject withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * BusinessFunction
     * <p>
     * The business function specifies the type of activity or access (i.e., the bundle of rights) offered by the organization or business person through the offer. Typical are sell, rental or lease, maintenance or repair, manufacture / produce, recycle / dispose, engineering / construction, or installation. Proprietary specifications of access rights are also instances of this class.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#ConstructionInstallation <br />
     *     http://purl.org/goodrelations/v1#Dispose <br />
     *     http://purl.org/goodrelations/v1#LeaseOut <br />
     *     http://purl.org/goodrelations/v1#Maintain <br />
     *     http://purl.org/goodrelations/v1#ProvideService <br />
     *     http://purl.org/goodrelations/v1#Repair <br />
     *     http://purl.org/goodrelations/v1#Sell <br />
     *     http://purl.org/goodrelations/v1#Buy <br />
     *         
     * 
     * @return
     *     The businessFunction
     */
    @JsonProperty("businessFunction")
    public BusinessFunction getBusinessFunction() {
        return businessFunction;
    }

    /**
     * BusinessFunction
     * <p>
     * The business function specifies the type of activity or access (i.e., the bundle of rights) offered by the organization or business person through the offer. Typical are sell, rental or lease, maintenance or repair, manufacture / produce, recycle / dispose, engineering / construction, or installation. Proprietary specifications of access rights are also instances of this class.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#ConstructionInstallation <br />
     *     http://purl.org/goodrelations/v1#Dispose <br />
     *     http://purl.org/goodrelations/v1#LeaseOut <br />
     *     http://purl.org/goodrelations/v1#Maintain <br />
     *     http://purl.org/goodrelations/v1#ProvideService <br />
     *     http://purl.org/goodrelations/v1#Repair <br />
     *     http://purl.org/goodrelations/v1#Sell <br />
     *     http://purl.org/goodrelations/v1#Buy <br />
     *         
     * 
     * @param businessFunction
     *     The businessFunction
     */
    @JsonProperty("businessFunction")
    public void setBusinessFunction(BusinessFunction businessFunction) {
        this.businessFunction = businessFunction;
    }

    public IncludesObject withBusinessFunction(BusinessFunction businessFunction) {
        this.businessFunction = businessFunction;
        return this;
    }

    /**
     * Product
     * <p>
     * Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online.
     * 
     * @return
     *     The typeOfGood
     */
    @JsonProperty("typeOfGood")
    public TypeOfGood getTypeOfGood() {
        return typeOfGood;
    }

    /**
     * Product
     * <p>
     * Any offered product or service. For example: a pair of shoes; a concert ticket; the rental of a car; a haircut; or an episode of a TV show streamed online.
     * 
     * @param typeOfGood
     *     The typeOfGood
     */
    @JsonProperty("typeOfGood")
    public void setTypeOfGood(TypeOfGood typeOfGood) {
        this.typeOfGood = typeOfGood;
    }

    public IncludesObject withTypeOfGood(TypeOfGood typeOfGood) {
        this.typeOfGood = typeOfGood;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public IncludesObject withName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(potentialAction).append(description).append(sameAs).append(image).append(additionalType).append(url).append(amountOfThisGood).append(unitText).append(mainEntityOfPage).append(unitCode).append(alternateName).append(businessFunction).append(typeOfGood).append(name).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof IncludesObject) == false) {
            return false;
        }
        IncludesObject rhs = ((IncludesObject) other);
        return new EqualsBuilder().append(potentialAction, rhs.potentialAction).append(description, rhs.description).append(sameAs, rhs.sameAs).append(image, rhs.image).append(additionalType, rhs.additionalType).append(url, rhs.url).append(amountOfThisGood, rhs.amountOfThisGood).append(unitText, rhs.unitText).append(mainEntityOfPage, rhs.mainEntityOfPage).append(unitCode, rhs.unitCode).append(alternateName, rhs.alternateName).append(businessFunction, rhs.businessFunction).append(typeOfGood, rhs.typeOfGood).append(name, rhs.name).isEquals();
    }

}
