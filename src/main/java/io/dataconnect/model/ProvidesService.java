
package io.dataconnect.model;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Service
 * <p>
 * A service provided by an organization, e.g. delivery service, print services, etc.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "produces",
    "sameAs",
    "image",
    "aggregateRating",
    "serviceOutput",
    "hasOfferCatalog",
    "additionalType",
    "category",
    "availableChannel",
    "review",
    "offers",
    "provider",
    "mainEntityOfPage",
    "serviceType",
    "serviceAudience",
    "description",
    "award",
    "hoursAvailable",
    "alternateName",
    "serviceArea",
    "potentialAction",
    "name",
    "url",
    "providerMobility",
    "areaServed"
})
public class ProvidesService {

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     */
    @JsonProperty("produces")
    @JsonPropertyDescription("")
    private io.dataconnect.model.Object produces;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     */
    @JsonProperty("aggregateRating")
    @JsonPropertyDescription("")
    private AggregateRating aggregateRating;
    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     */
    @JsonProperty("serviceOutput")
    @JsonPropertyDescription("")
    private io.dataconnect.model.Object serviceOutput;
    /**
     * This is a generated, and simplified, variant of https://schema.org/OfferCatalog. I has been interpreted as a plain array, this behaviour is hard-coded to the itemList types and should be improved.
     * 
     */
    @JsonProperty("hasOfferCatalog")
    @JsonPropertyDescription("")
    private List<java.lang.Object> hasOfferCatalog = new ArrayList<java.lang.Object>();
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * A category for the item. Greater signs or slashes can be used to informally indicate a category hierarchy.
     * 
     */
    @JsonProperty("category")
    @JsonPropertyDescription("")
    private java.lang.Object category;
    /**
     * ServiceChannel
     * <p>
     * A means for accessing a service, e.g. a government office location, web site, or phone number.
     * 
     */
    @JsonProperty("availableChannel")
    @JsonPropertyDescription("")
    private AvailableChannel availableChannel;
    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     */
    @JsonProperty("review")
    @JsonPropertyDescription("")
    private Review review;
    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     */
    @JsonProperty("offers")
    @JsonPropertyDescription("")
    private Offer offers;
    /**
     * The service provider, service operator, or service performer; the goods producer. Another party (a seller) may offer those services or goods on behalf of the provider. A provider may also serve as the seller.
     * 
     */
    @JsonProperty("provider")
    @JsonPropertyDescription("")
    private java.lang.Object provider;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * The type of service being offered, e.g. veterans' benefits, emergency relief, etc.
     * 
     */
    @JsonProperty("serviceType")
    @JsonPropertyDescription("")
    private String serviceType;
    /**
     * Audience
     * <p>
     * Intended audience for an item, i.e. the group for whom the item was created.
     * 
     */
    @JsonProperty("serviceAudience")
    @JsonPropertyDescription("")
    private Audience serviceAudience;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;
    /**
     * An award won by or for this item.
     * 
     */
    @JsonProperty("award")
    @JsonPropertyDescription("")
    private String award;
    /**
     * OpeningHoursSpecification
     * <p>
     * A structured value providing information about the opening hours of a place or a certain service inside a place.
     * 
     */
    @JsonProperty("hoursAvailable")
    @JsonPropertyDescription("")
    private HoursAvailable hoursAvailable;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * The geographic area where the service is provided.
     * 
     */
    @JsonProperty("serviceArea")
    @JsonPropertyDescription("")
    private java.lang.Object serviceArea;
    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * Indicates the mobility of a provided service (e.g. 'static', 'dynamic').
     * 
     */
    @JsonProperty("providerMobility")
    @JsonPropertyDescription("")
    private String providerMobility;
    /**
     * The geographic area where a service or offered item is provided.
     * 
     */
    @JsonProperty("areaServed")
    @JsonPropertyDescription("")
    private java.lang.Object areaServed;

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @return
     *     The produces
     */
    @JsonProperty("produces")
    public io.dataconnect.model.Object getProduces() {
        return produces;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @param produces
     *     The produces
     */
    @JsonProperty("produces")
    public void setProduces(io.dataconnect.model.Object produces) {
        this.produces = produces;
    }

    public ProvidesService withProduces(io.dataconnect.model.Object produces) {
        this.produces = produces;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public ProvidesService withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public ProvidesService withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     * @return
     *     The aggregateRating
     */
    @JsonProperty("aggregateRating")
    public AggregateRating getAggregateRating() {
        return aggregateRating;
    }

    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     * @param aggregateRating
     *     The aggregateRating
     */
    @JsonProperty("aggregateRating")
    public void setAggregateRating(AggregateRating aggregateRating) {
        this.aggregateRating = aggregateRating;
    }

    public ProvidesService withAggregateRating(AggregateRating aggregateRating) {
        this.aggregateRating = aggregateRating;
        return this;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @return
     *     The serviceOutput
     */
    @JsonProperty("serviceOutput")
    public io.dataconnect.model.Object getServiceOutput() {
        return serviceOutput;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @param serviceOutput
     *     The serviceOutput
     */
    @JsonProperty("serviceOutput")
    public void setServiceOutput(io.dataconnect.model.Object serviceOutput) {
        this.serviceOutput = serviceOutput;
    }

    public ProvidesService withServiceOutput(io.dataconnect.model.Object serviceOutput) {
        this.serviceOutput = serviceOutput;
        return this;
    }

    /**
     * This is a generated, and simplified, variant of https://schema.org/OfferCatalog. I has been interpreted as a plain array, this behaviour is hard-coded to the itemList types and should be improved.
     * 
     * @return
     *     The hasOfferCatalog
     */
    @JsonProperty("hasOfferCatalog")
    public List<java.lang.Object> getHasOfferCatalog() {
        return hasOfferCatalog;
    }

    /**
     * This is a generated, and simplified, variant of https://schema.org/OfferCatalog. I has been interpreted as a plain array, this behaviour is hard-coded to the itemList types and should be improved.
     * 
     * @param hasOfferCatalog
     *     The hasOfferCatalog
     */
    @JsonProperty("hasOfferCatalog")
    public void setHasOfferCatalog(List<java.lang.Object> hasOfferCatalog) {
        this.hasOfferCatalog = hasOfferCatalog;
    }

    public ProvidesService withHasOfferCatalog(List<java.lang.Object> hasOfferCatalog) {
        this.hasOfferCatalog = hasOfferCatalog;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public ProvidesService withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * A category for the item. Greater signs or slashes can be used to informally indicate a category hierarchy.
     * 
     * @return
     *     The category
     */
    @JsonProperty("category")
    public java.lang.Object getCategory() {
        return category;
    }

    /**
     * A category for the item. Greater signs or slashes can be used to informally indicate a category hierarchy.
     * 
     * @param category
     *     The category
     */
    @JsonProperty("category")
    public void setCategory(java.lang.Object category) {
        this.category = category;
    }

    public ProvidesService withCategory(java.lang.Object category) {
        this.category = category;
        return this;
    }

    /**
     * ServiceChannel
     * <p>
     * A means for accessing a service, e.g. a government office location, web site, or phone number.
     * 
     * @return
     *     The availableChannel
     */
    @JsonProperty("availableChannel")
    public AvailableChannel getAvailableChannel() {
        return availableChannel;
    }

    /**
     * ServiceChannel
     * <p>
     * A means for accessing a service, e.g. a government office location, web site, or phone number.
     * 
     * @param availableChannel
     *     The availableChannel
     */
    @JsonProperty("availableChannel")
    public void setAvailableChannel(AvailableChannel availableChannel) {
        this.availableChannel = availableChannel;
    }

    public ProvidesService withAvailableChannel(AvailableChannel availableChannel) {
        this.availableChannel = availableChannel;
        return this;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @return
     *     The review
     */
    @JsonProperty("review")
    public Review getReview() {
        return review;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @param review
     *     The review
     */
    @JsonProperty("review")
    public void setReview(Review review) {
        this.review = review;
    }

    public ProvidesService withReview(Review review) {
        this.review = review;
        return this;
    }

    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     * @return
     *     The offers
     */
    @JsonProperty("offers")
    public Offer getOffers() {
        return offers;
    }

    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     * @param offers
     *     The offers
     */
    @JsonProperty("offers")
    public void setOffers(Offer offers) {
        this.offers = offers;
    }

    public ProvidesService withOffers(Offer offers) {
        this.offers = offers;
        return this;
    }

    /**
     * The service provider, service operator, or service performer; the goods producer. Another party (a seller) may offer those services or goods on behalf of the provider. A provider may also serve as the seller.
     * 
     * @return
     *     The provider
     */
    @JsonProperty("provider")
    public java.lang.Object getProvider() {
        return provider;
    }

    /**
     * The service provider, service operator, or service performer; the goods producer. Another party (a seller) may offer those services or goods on behalf of the provider. A provider may also serve as the seller.
     * 
     * @param provider
     *     The provider
     */
    @JsonProperty("provider")
    public void setProvider(java.lang.Object provider) {
        this.provider = provider;
    }

    public ProvidesService withProvider(java.lang.Object provider) {
        this.provider = provider;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public ProvidesService withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * The type of service being offered, e.g. veterans' benefits, emergency relief, etc.
     * 
     * @return
     *     The serviceType
     */
    @JsonProperty("serviceType")
    public String getServiceType() {
        return serviceType;
    }

    /**
     * The type of service being offered, e.g. veterans' benefits, emergency relief, etc.
     * 
     * @param serviceType
     *     The serviceType
     */
    @JsonProperty("serviceType")
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public ProvidesService withServiceType(String serviceType) {
        this.serviceType = serviceType;
        return this;
    }

    /**
     * Audience
     * <p>
     * Intended audience for an item, i.e. the group for whom the item was created.
     * 
     * @return
     *     The serviceAudience
     */
    @JsonProperty("serviceAudience")
    public Audience getServiceAudience() {
        return serviceAudience;
    }

    /**
     * Audience
     * <p>
     * Intended audience for an item, i.e. the group for whom the item was created.
     * 
     * @param serviceAudience
     *     The serviceAudience
     */
    @JsonProperty("serviceAudience")
    public void setServiceAudience(Audience serviceAudience) {
        this.serviceAudience = serviceAudience;
    }

    public ProvidesService withServiceAudience(Audience serviceAudience) {
        this.serviceAudience = serviceAudience;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public ProvidesService withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * An award won by or for this item.
     * 
     * @return
     *     The award
     */
    @JsonProperty("award")
    public String getAward() {
        return award;
    }

    /**
     * An award won by or for this item.
     * 
     * @param award
     *     The award
     */
    @JsonProperty("award")
    public void setAward(String award) {
        this.award = award;
    }

    public ProvidesService withAward(String award) {
        this.award = award;
        return this;
    }

    /**
     * OpeningHoursSpecification
     * <p>
     * A structured value providing information about the opening hours of a place or a certain service inside a place.
     * 
     * @return
     *     The hoursAvailable
     */
    @JsonProperty("hoursAvailable")
    public HoursAvailable getHoursAvailable() {
        return hoursAvailable;
    }

    /**
     * OpeningHoursSpecification
     * <p>
     * A structured value providing information about the opening hours of a place or a certain service inside a place.
     * 
     * @param hoursAvailable
     *     The hoursAvailable
     */
    @JsonProperty("hoursAvailable")
    public void setHoursAvailable(HoursAvailable hoursAvailable) {
        this.hoursAvailable = hoursAvailable;
    }

    public ProvidesService withHoursAvailable(HoursAvailable hoursAvailable) {
        this.hoursAvailable = hoursAvailable;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public ProvidesService withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * The geographic area where the service is provided.
     * 
     * @return
     *     The serviceArea
     */
    @JsonProperty("serviceArea")
    public java.lang.Object getServiceArea() {
        return serviceArea;
    }

    /**
     * The geographic area where the service is provided.
     * 
     * @param serviceArea
     *     The serviceArea
     */
    @JsonProperty("serviceArea")
    public void setServiceArea(java.lang.Object serviceArea) {
        this.serviceArea = serviceArea;
    }

    public ProvidesService withServiceArea(java.lang.Object serviceArea) {
        this.serviceArea = serviceArea;
        return this;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public ProvidesService withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public ProvidesService withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public ProvidesService withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * Indicates the mobility of a provided service (e.g. 'static', 'dynamic').
     * 
     * @return
     *     The providerMobility
     */
    @JsonProperty("providerMobility")
    public String getProviderMobility() {
        return providerMobility;
    }

    /**
     * Indicates the mobility of a provided service (e.g. 'static', 'dynamic').
     * 
     * @param providerMobility
     *     The providerMobility
     */
    @JsonProperty("providerMobility")
    public void setProviderMobility(String providerMobility) {
        this.providerMobility = providerMobility;
    }

    public ProvidesService withProviderMobility(String providerMobility) {
        this.providerMobility = providerMobility;
        return this;
    }

    /**
     * The geographic area where a service or offered item is provided.
     * 
     * @return
     *     The areaServed
     */
    @JsonProperty("areaServed")
    public java.lang.Object getAreaServed() {
        return areaServed;
    }

    /**
     * The geographic area where a service or offered item is provided.
     * 
     * @param areaServed
     *     The areaServed
     */
    @JsonProperty("areaServed")
    public void setAreaServed(java.lang.Object areaServed) {
        this.areaServed = areaServed;
    }

    public ProvidesService withAreaServed(java.lang.Object areaServed) {
        this.areaServed = areaServed;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(produces).append(sameAs).append(image).append(aggregateRating).append(serviceOutput).append(hasOfferCatalog).append(additionalType).append(category).append(availableChannel).append(review).append(offers).append(provider).append(mainEntityOfPage).append(serviceType).append(serviceAudience).append(description).append(award).append(hoursAvailable).append(alternateName).append(serviceArea).append(potentialAction).append(name).append(url).append(providerMobility).append(areaServed).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ProvidesService) == false) {
            return false;
        }
        ProvidesService rhs = ((ProvidesService) other);
        return new EqualsBuilder().append(produces, rhs.produces).append(sameAs, rhs.sameAs).append(image, rhs.image).append(aggregateRating, rhs.aggregateRating).append(serviceOutput, rhs.serviceOutput).append(hasOfferCatalog, rhs.hasOfferCatalog).append(additionalType, rhs.additionalType).append(category, rhs.category).append(availableChannel, rhs.availableChannel).append(review, rhs.review).append(offers, rhs.offers).append(provider, rhs.provider).append(mainEntityOfPage, rhs.mainEntityOfPage).append(serviceType, rhs.serviceType).append(serviceAudience, rhs.serviceAudience).append(description, rhs.description).append(award, rhs.award).append(hoursAvailable, rhs.hoursAvailable).append(alternateName, rhs.alternateName).append(serviceArea, rhs.serviceArea).append(potentialAction, rhs.potentialAction).append(name, rhs.name).append(url, rhs.url).append(providerMobility, rhs.providerMobility).append(areaServed, rhs.areaServed).isEquals();
    }

}
