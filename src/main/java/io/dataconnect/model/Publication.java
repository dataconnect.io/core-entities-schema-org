
package io.dataconnect.model;

import java.net.URI;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * PublicationEvent
 * <p>
 * A PublicationEvent corresponds indifferently to the event of publication for a CreativeWork of any type e.g. a broadcast event, an on-demand event, a book/journal publication via a variety of delivery media.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "startDate",
    "performer",
    "workPerformed",
    "inLanguage",
    "sameAs",
    "image",
    "isAccessibleForFree",
    "aggregateRating",
    "typicalAgeRange",
    "duration",
    "additionalType",
    "organizer",
    "doorTime",
    "alternateName",
    "attendees",
    "previousStartDate",
    "review",
    "offers",
    "location",
    "mainEntityOfPage",
    "publishedOn",
    "workFeatured",
    "attendee",
    "description",
    "superEvent",
    "recordedIn",
    "free",
    "subEvent",
    "publishedBy",
    "potentialAction",
    "name",
    "performers",
    "url",
    "endDate",
    "subEvents",
    "eventStatus"
})
public class Publication {

    /**
     * The start date and time of the item (in <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 date format</a>).
     * 
     */
    @JsonProperty("startDate")
    @JsonPropertyDescription("")
    private Date startDate;
    /**
     * A performer at the event&#x2014;for example, a presenter, musician, musical group or actor.
     * 
     */
    @JsonProperty("performer")
    @JsonPropertyDescription("")
    private java.lang.Object performer;
    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     */
    @JsonProperty("workPerformed")
    @JsonPropertyDescription("")
    private ExampleOfWork workPerformed;
    /**
     * The language of the content or performance or used in an action. Please use one of the language codes from the <a href='http://tools.ietf.org/html/bcp47'>IETF BCP 47 standard</a>.
     * 
     */
    @JsonProperty("inLanguage")
    @JsonPropertyDescription("")
    private java.lang.Object inLanguage;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * A flag to signal that the publication is accessible for free.
     * 
     */
    @JsonProperty("isAccessibleForFree")
    @JsonPropertyDescription("")
    private Boolean isAccessibleForFree;
    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     */
    @JsonProperty("aggregateRating")
    @JsonPropertyDescription("")
    private AggregateRating aggregateRating;
    /**
     * The typical expected age range, e.g. '7-9', '11-'.
     * 
     */
    @JsonProperty("typicalAgeRange")
    @JsonPropertyDescription("")
    private String typicalAgeRange;
    /**
     * Duration
     * <p>
     * Quantity: Duration (use  <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 duration format</a>).
     * 
     */
    @JsonProperty("duration")
    @JsonPropertyDescription("")
    private Duration duration;
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * An organizer of an Event.
     * 
     */
    @JsonProperty("organizer")
    @JsonPropertyDescription("")
    private java.lang.Object organizer;
    /**
     * The time admission will commence.
     * 
     */
    @JsonProperty("doorTime")
    @JsonPropertyDescription("")
    private Date doorTime;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * A person attending the event.
     * 
     */
    @JsonProperty("attendees")
    @JsonPropertyDescription("")
    private java.lang.Object attendees;
    /**
     * Used in conjunction with eventStatus for rescheduled or cancelled events. This property contains the previously scheduled start date. For rescheduled events, the startDate property should be used for the newly scheduled start date. In the (rare) case of an event that has been postponed and rescheduled multiple times, this field may be repeated.
     * 
     */
    @JsonProperty("previousStartDate")
    @JsonPropertyDescription("")
    private Date previousStartDate;
    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     */
    @JsonProperty("review")
    @JsonPropertyDescription("")
    private Review review;
    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     */
    @JsonProperty("offers")
    @JsonPropertyDescription("")
    private Offer offers;
    /**
     * The location of for example where the event is happening, an organization is located, or where an action takes place.
     * 
     */
    @JsonProperty("location")
    @JsonPropertyDescription("")
    private java.lang.Object location;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * BroadcastService
     * <p>
     * A delivery service through which content is provided via broadcast over the air or online.
     * 
     */
    @JsonProperty("publishedOn")
    @JsonPropertyDescription("")
    private PublishedOn publishedOn;
    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     */
    @JsonProperty("workFeatured")
    @JsonPropertyDescription("")
    private ExampleOfWork workFeatured;
    /**
     * A person or organization attending the event.
     * 
     */
    @JsonProperty("attendee")
    @JsonPropertyDescription("")
    private java.lang.Object attendee;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;
    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     */
    @JsonProperty("superEvent")
    @JsonPropertyDescription("")
    private Event superEvent;
    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     */
    @JsonProperty("recordedIn")
    @JsonPropertyDescription("")
    private ExampleOfWork recordedIn;
    /**
     * A flag to signal that the publication is accessible for free.
     * 
     */
    @JsonProperty("free")
    @JsonPropertyDescription("")
    private Boolean free;
    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     */
    @JsonProperty("subEvent")
    @JsonPropertyDescription("")
    private Event subEvent;
    /**
     * An agent associated with the publication event.
     * 
     */
    @JsonProperty("publishedBy")
    @JsonPropertyDescription("")
    private java.lang.Object publishedBy;
    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;
    /**
     * The main performer or performers of the event&#x2014;for example, a presenter, musician, or actor.
     * 
     */
    @JsonProperty("performers")
    @JsonPropertyDescription("")
    private java.lang.Object performers;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * The end date and time of the item (in <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 date format</a>).
     * 
     */
    @JsonProperty("endDate")
    @JsonPropertyDescription("")
    private Date endDate;
    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     */
    @JsonProperty("subEvents")
    @JsonPropertyDescription("")
    private Event subEvents;
    /**
     * EventStatusType
     * <p>
     * EventStatusType is an enumeration type whose instances represent several states that an Event may be in.
     * 
     */
    @JsonProperty("eventStatus")
    @JsonPropertyDescription("")
    private EventStatus eventStatus;

    /**
     * The start date and time of the item (in <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 date format</a>).
     * 
     * @return
     *     The startDate
     */
    @JsonProperty("startDate")
    public Date getStartDate() {
        return startDate;
    }

    /**
     * The start date and time of the item (in <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 date format</a>).
     * 
     * @param startDate
     *     The startDate
     */
    @JsonProperty("startDate")
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Publication withStartDate(Date startDate) {
        this.startDate = startDate;
        return this;
    }

    /**
     * A performer at the event&#x2014;for example, a presenter, musician, musical group or actor.
     * 
     * @return
     *     The performer
     */
    @JsonProperty("performer")
    public java.lang.Object getPerformer() {
        return performer;
    }

    /**
     * A performer at the event&#x2014;for example, a presenter, musician, musical group or actor.
     * 
     * @param performer
     *     The performer
     */
    @JsonProperty("performer")
    public void setPerformer(java.lang.Object performer) {
        this.performer = performer;
    }

    public Publication withPerformer(java.lang.Object performer) {
        this.performer = performer;
        return this;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @return
     *     The workPerformed
     */
    @JsonProperty("workPerformed")
    public ExampleOfWork getWorkPerformed() {
        return workPerformed;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @param workPerformed
     *     The workPerformed
     */
    @JsonProperty("workPerformed")
    public void setWorkPerformed(ExampleOfWork workPerformed) {
        this.workPerformed = workPerformed;
    }

    public Publication withWorkPerformed(ExampleOfWork workPerformed) {
        this.workPerformed = workPerformed;
        return this;
    }

    /**
     * The language of the content or performance or used in an action. Please use one of the language codes from the <a href='http://tools.ietf.org/html/bcp47'>IETF BCP 47 standard</a>.
     * 
     * @return
     *     The inLanguage
     */
    @JsonProperty("inLanguage")
    public java.lang.Object getInLanguage() {
        return inLanguage;
    }

    /**
     * The language of the content or performance or used in an action. Please use one of the language codes from the <a href='http://tools.ietf.org/html/bcp47'>IETF BCP 47 standard</a>.
     * 
     * @param inLanguage
     *     The inLanguage
     */
    @JsonProperty("inLanguage")
    public void setInLanguage(java.lang.Object inLanguage) {
        this.inLanguage = inLanguage;
    }

    public Publication withInLanguage(java.lang.Object inLanguage) {
        this.inLanguage = inLanguage;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public Publication withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public Publication withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * A flag to signal that the publication is accessible for free.
     * 
     * @return
     *     The isAccessibleForFree
     */
    @JsonProperty("isAccessibleForFree")
    public Boolean getIsAccessibleForFree() {
        return isAccessibleForFree;
    }

    /**
     * A flag to signal that the publication is accessible for free.
     * 
     * @param isAccessibleForFree
     *     The isAccessibleForFree
     */
    @JsonProperty("isAccessibleForFree")
    public void setIsAccessibleForFree(Boolean isAccessibleForFree) {
        this.isAccessibleForFree = isAccessibleForFree;
    }

    public Publication withIsAccessibleForFree(Boolean isAccessibleForFree) {
        this.isAccessibleForFree = isAccessibleForFree;
        return this;
    }

    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     * @return
     *     The aggregateRating
     */
    @JsonProperty("aggregateRating")
    public AggregateRating getAggregateRating() {
        return aggregateRating;
    }

    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     * @param aggregateRating
     *     The aggregateRating
     */
    @JsonProperty("aggregateRating")
    public void setAggregateRating(AggregateRating aggregateRating) {
        this.aggregateRating = aggregateRating;
    }

    public Publication withAggregateRating(AggregateRating aggregateRating) {
        this.aggregateRating = aggregateRating;
        return this;
    }

    /**
     * The typical expected age range, e.g. '7-9', '11-'.
     * 
     * @return
     *     The typicalAgeRange
     */
    @JsonProperty("typicalAgeRange")
    public String getTypicalAgeRange() {
        return typicalAgeRange;
    }

    /**
     * The typical expected age range, e.g. '7-9', '11-'.
     * 
     * @param typicalAgeRange
     *     The typicalAgeRange
     */
    @JsonProperty("typicalAgeRange")
    public void setTypicalAgeRange(String typicalAgeRange) {
        this.typicalAgeRange = typicalAgeRange;
    }

    public Publication withTypicalAgeRange(String typicalAgeRange) {
        this.typicalAgeRange = typicalAgeRange;
        return this;
    }

    /**
     * Duration
     * <p>
     * Quantity: Duration (use  <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 duration format</a>).
     * 
     * @return
     *     The duration
     */
    @JsonProperty("duration")
    public Duration getDuration() {
        return duration;
    }

    /**
     * Duration
     * <p>
     * Quantity: Duration (use  <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 duration format</a>).
     * 
     * @param duration
     *     The duration
     */
    @JsonProperty("duration")
    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Publication withDuration(Duration duration) {
        this.duration = duration;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public Publication withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * An organizer of an Event.
     * 
     * @return
     *     The organizer
     */
    @JsonProperty("organizer")
    public java.lang.Object getOrganizer() {
        return organizer;
    }

    /**
     * An organizer of an Event.
     * 
     * @param organizer
     *     The organizer
     */
    @JsonProperty("organizer")
    public void setOrganizer(java.lang.Object organizer) {
        this.organizer = organizer;
    }

    public Publication withOrganizer(java.lang.Object organizer) {
        this.organizer = organizer;
        return this;
    }

    /**
     * The time admission will commence.
     * 
     * @return
     *     The doorTime
     */
    @JsonProperty("doorTime")
    public Date getDoorTime() {
        return doorTime;
    }

    /**
     * The time admission will commence.
     * 
     * @param doorTime
     *     The doorTime
     */
    @JsonProperty("doorTime")
    public void setDoorTime(Date doorTime) {
        this.doorTime = doorTime;
    }

    public Publication withDoorTime(Date doorTime) {
        this.doorTime = doorTime;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public Publication withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * A person attending the event.
     * 
     * @return
     *     The attendees
     */
    @JsonProperty("attendees")
    public java.lang.Object getAttendees() {
        return attendees;
    }

    /**
     * A person attending the event.
     * 
     * @param attendees
     *     The attendees
     */
    @JsonProperty("attendees")
    public void setAttendees(java.lang.Object attendees) {
        this.attendees = attendees;
    }

    public Publication withAttendees(java.lang.Object attendees) {
        this.attendees = attendees;
        return this;
    }

    /**
     * Used in conjunction with eventStatus for rescheduled or cancelled events. This property contains the previously scheduled start date. For rescheduled events, the startDate property should be used for the newly scheduled start date. In the (rare) case of an event that has been postponed and rescheduled multiple times, this field may be repeated.
     * 
     * @return
     *     The previousStartDate
     */
    @JsonProperty("previousStartDate")
    public Date getPreviousStartDate() {
        return previousStartDate;
    }

    /**
     * Used in conjunction with eventStatus for rescheduled or cancelled events. This property contains the previously scheduled start date. For rescheduled events, the startDate property should be used for the newly scheduled start date. In the (rare) case of an event that has been postponed and rescheduled multiple times, this field may be repeated.
     * 
     * @param previousStartDate
     *     The previousStartDate
     */
    @JsonProperty("previousStartDate")
    public void setPreviousStartDate(Date previousStartDate) {
        this.previousStartDate = previousStartDate;
    }

    public Publication withPreviousStartDate(Date previousStartDate) {
        this.previousStartDate = previousStartDate;
        return this;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @return
     *     The review
     */
    @JsonProperty("review")
    public Review getReview() {
        return review;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @param review
     *     The review
     */
    @JsonProperty("review")
    public void setReview(Review review) {
        this.review = review;
    }

    public Publication withReview(Review review) {
        this.review = review;
        return this;
    }

    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     * @return
     *     The offers
     */
    @JsonProperty("offers")
    public Offer getOffers() {
        return offers;
    }

    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     * @param offers
     *     The offers
     */
    @JsonProperty("offers")
    public void setOffers(Offer offers) {
        this.offers = offers;
    }

    public Publication withOffers(Offer offers) {
        this.offers = offers;
        return this;
    }

    /**
     * The location of for example where the event is happening, an organization is located, or where an action takes place.
     * 
     * @return
     *     The location
     */
    @JsonProperty("location")
    public java.lang.Object getLocation() {
        return location;
    }

    /**
     * The location of for example where the event is happening, an organization is located, or where an action takes place.
     * 
     * @param location
     *     The location
     */
    @JsonProperty("location")
    public void setLocation(java.lang.Object location) {
        this.location = location;
    }

    public Publication withLocation(java.lang.Object location) {
        this.location = location;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public Publication withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * BroadcastService
     * <p>
     * A delivery service through which content is provided via broadcast over the air or online.
     * 
     * @return
     *     The publishedOn
     */
    @JsonProperty("publishedOn")
    public PublishedOn getPublishedOn() {
        return publishedOn;
    }

    /**
     * BroadcastService
     * <p>
     * A delivery service through which content is provided via broadcast over the air or online.
     * 
     * @param publishedOn
     *     The publishedOn
     */
    @JsonProperty("publishedOn")
    public void setPublishedOn(PublishedOn publishedOn) {
        this.publishedOn = publishedOn;
    }

    public Publication withPublishedOn(PublishedOn publishedOn) {
        this.publishedOn = publishedOn;
        return this;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @return
     *     The workFeatured
     */
    @JsonProperty("workFeatured")
    public ExampleOfWork getWorkFeatured() {
        return workFeatured;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @param workFeatured
     *     The workFeatured
     */
    @JsonProperty("workFeatured")
    public void setWorkFeatured(ExampleOfWork workFeatured) {
        this.workFeatured = workFeatured;
    }

    public Publication withWorkFeatured(ExampleOfWork workFeatured) {
        this.workFeatured = workFeatured;
        return this;
    }

    /**
     * A person or organization attending the event.
     * 
     * @return
     *     The attendee
     */
    @JsonProperty("attendee")
    public java.lang.Object getAttendee() {
        return attendee;
    }

    /**
     * A person or organization attending the event.
     * 
     * @param attendee
     *     The attendee
     */
    @JsonProperty("attendee")
    public void setAttendee(java.lang.Object attendee) {
        this.attendee = attendee;
    }

    public Publication withAttendee(java.lang.Object attendee) {
        this.attendee = attendee;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public Publication withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @return
     *     The superEvent
     */
    @JsonProperty("superEvent")
    public Event getSuperEvent() {
        return superEvent;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @param superEvent
     *     The superEvent
     */
    @JsonProperty("superEvent")
    public void setSuperEvent(Event superEvent) {
        this.superEvent = superEvent;
    }

    public Publication withSuperEvent(Event superEvent) {
        this.superEvent = superEvent;
        return this;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @return
     *     The recordedIn
     */
    @JsonProperty("recordedIn")
    public ExampleOfWork getRecordedIn() {
        return recordedIn;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @param recordedIn
     *     The recordedIn
     */
    @JsonProperty("recordedIn")
    public void setRecordedIn(ExampleOfWork recordedIn) {
        this.recordedIn = recordedIn;
    }

    public Publication withRecordedIn(ExampleOfWork recordedIn) {
        this.recordedIn = recordedIn;
        return this;
    }

    /**
     * A flag to signal that the publication is accessible for free.
     * 
     * @return
     *     The free
     */
    @JsonProperty("free")
    public Boolean getFree() {
        return free;
    }

    /**
     * A flag to signal that the publication is accessible for free.
     * 
     * @param free
     *     The free
     */
    @JsonProperty("free")
    public void setFree(Boolean free) {
        this.free = free;
    }

    public Publication withFree(Boolean free) {
        this.free = free;
        return this;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @return
     *     The subEvent
     */
    @JsonProperty("subEvent")
    public Event getSubEvent() {
        return subEvent;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @param subEvent
     *     The subEvent
     */
    @JsonProperty("subEvent")
    public void setSubEvent(Event subEvent) {
        this.subEvent = subEvent;
    }

    public Publication withSubEvent(Event subEvent) {
        this.subEvent = subEvent;
        return this;
    }

    /**
     * An agent associated with the publication event.
     * 
     * @return
     *     The publishedBy
     */
    @JsonProperty("publishedBy")
    public java.lang.Object getPublishedBy() {
        return publishedBy;
    }

    /**
     * An agent associated with the publication event.
     * 
     * @param publishedBy
     *     The publishedBy
     */
    @JsonProperty("publishedBy")
    public void setPublishedBy(java.lang.Object publishedBy) {
        this.publishedBy = publishedBy;
    }

    public Publication withPublishedBy(java.lang.Object publishedBy) {
        this.publishedBy = publishedBy;
        return this;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public Publication withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Publication withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * The main performer or performers of the event&#x2014;for example, a presenter, musician, or actor.
     * 
     * @return
     *     The performers
     */
    @JsonProperty("performers")
    public java.lang.Object getPerformers() {
        return performers;
    }

    /**
     * The main performer or performers of the event&#x2014;for example, a presenter, musician, or actor.
     * 
     * @param performers
     *     The performers
     */
    @JsonProperty("performers")
    public void setPerformers(java.lang.Object performers) {
        this.performers = performers;
    }

    public Publication withPerformers(java.lang.Object performers) {
        this.performers = performers;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public Publication withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * The end date and time of the item (in <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 date format</a>).
     * 
     * @return
     *     The endDate
     */
    @JsonProperty("endDate")
    public Date getEndDate() {
        return endDate;
    }

    /**
     * The end date and time of the item (in <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 date format</a>).
     * 
     * @param endDate
     *     The endDate
     */
    @JsonProperty("endDate")
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Publication withEndDate(Date endDate) {
        this.endDate = endDate;
        return this;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @return
     *     The subEvents
     */
    @JsonProperty("subEvents")
    public Event getSubEvents() {
        return subEvents;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @param subEvents
     *     The subEvents
     */
    @JsonProperty("subEvents")
    public void setSubEvents(Event subEvents) {
        this.subEvents = subEvents;
    }

    public Publication withSubEvents(Event subEvents) {
        this.subEvents = subEvents;
        return this;
    }

    /**
     * EventStatusType
     * <p>
     * EventStatusType is an enumeration type whose instances represent several states that an Event may be in.
     * 
     * @return
     *     The eventStatus
     */
    @JsonProperty("eventStatus")
    public EventStatus getEventStatus() {
        return eventStatus;
    }

    /**
     * EventStatusType
     * <p>
     * EventStatusType is an enumeration type whose instances represent several states that an Event may be in.
     * 
     * @param eventStatus
     *     The eventStatus
     */
    @JsonProperty("eventStatus")
    public void setEventStatus(EventStatus eventStatus) {
        this.eventStatus = eventStatus;
    }

    public Publication withEventStatus(EventStatus eventStatus) {
        this.eventStatus = eventStatus;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(startDate).append(performer).append(workPerformed).append(inLanguage).append(sameAs).append(image).append(isAccessibleForFree).append(aggregateRating).append(typicalAgeRange).append(duration).append(additionalType).append(organizer).append(doorTime).append(alternateName).append(attendees).append(previousStartDate).append(review).append(offers).append(location).append(mainEntityOfPage).append(publishedOn).append(workFeatured).append(attendee).append(description).append(superEvent).append(recordedIn).append(free).append(subEvent).append(publishedBy).append(potentialAction).append(name).append(performers).append(url).append(endDate).append(subEvents).append(eventStatus).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Publication) == false) {
            return false;
        }
        Publication rhs = ((Publication) other);
        return new EqualsBuilder().append(startDate, rhs.startDate).append(performer, rhs.performer).append(workPerformed, rhs.workPerformed).append(inLanguage, rhs.inLanguage).append(sameAs, rhs.sameAs).append(image, rhs.image).append(isAccessibleForFree, rhs.isAccessibleForFree).append(aggregateRating, rhs.aggregateRating).append(typicalAgeRange, rhs.typicalAgeRange).append(duration, rhs.duration).append(additionalType, rhs.additionalType).append(organizer, rhs.organizer).append(doorTime, rhs.doorTime).append(alternateName, rhs.alternateName).append(attendees, rhs.attendees).append(previousStartDate, rhs.previousStartDate).append(review, rhs.review).append(offers, rhs.offers).append(location, rhs.location).append(mainEntityOfPage, rhs.mainEntityOfPage).append(publishedOn, rhs.publishedOn).append(workFeatured, rhs.workFeatured).append(attendee, rhs.attendee).append(description, rhs.description).append(superEvent, rhs.superEvent).append(recordedIn, rhs.recordedIn).append(free, rhs.free).append(subEvent, rhs.subEvent).append(publishedBy, rhs.publishedBy).append(potentialAction, rhs.potentialAction).append(name, rhs.name).append(performers, rhs.performers).append(url, rhs.url).append(endDate, rhs.endDate).append(subEvents, rhs.subEvents).append(eventStatus, rhs.eventStatus).isEquals();
    }

}
