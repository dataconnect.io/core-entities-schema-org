
package io.dataconnect.model;

import java.net.URI;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * OpeningHoursSpecification
 * <p>
 * A structured value providing information about the opening hours of a place or a certain service inside a place.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "dayOfWeek",
    "closes",
    "potentialAction",
    "description",
    "sameAs",
    "image",
    "url",
    "validThrough",
    "mainEntityOfPage",
    "additionalType",
    "alternateName",
    "validFrom",
    "opens",
    "name"
})
public class HoursAvailable {

    /**
     * DayOfWeek
     * <p>
     * The day of the week, e.g. used to specify to which day the opening hours of an OpeningHoursSpecification refer.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#Monday <br />
     *     http://purl.org/goodrelations/v1#Tuesday <br />
     *     http://purl.org/goodrelations/v1#Wednesday <br />
     *     http://purl.org/goodrelations/v1#Thursday <br />
     *     http://purl.org/goodrelations/v1#Friday <br />
     *     http://purl.org/goodrelations/v1#Saturday <br />
     *     http://purl.org/goodrelations/v1#Sunday <br />
     *     http://purl.org/goodrelations/v1#PublicHolidays <br />
     *         
     * 
     */
    @JsonProperty("dayOfWeek")
    @JsonPropertyDescription("")
    private DayOfWeek dayOfWeek;
    /**
     * The closing hour of the place or service on the given day(s) of the week.
     * 
     */
    @JsonProperty("closes")
    @JsonPropertyDescription("")
    private String closes;
    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * The end of the validity of offer, price specification, or opening hours data.
     * 
     */
    @JsonProperty("validThrough")
    @JsonPropertyDescription("")
    private Date validThrough;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * The date when the item becomes valid.
     * 
     */
    @JsonProperty("validFrom")
    @JsonPropertyDescription("")
    private Date validFrom;
    /**
     * The opening hour of the place or service on the given day(s) of the week.
     * 
     */
    @JsonProperty("opens")
    @JsonPropertyDescription("")
    private String opens;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;

    /**
     * DayOfWeek
     * <p>
     * The day of the week, e.g. used to specify to which day the opening hours of an OpeningHoursSpecification refer.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#Monday <br />
     *     http://purl.org/goodrelations/v1#Tuesday <br />
     *     http://purl.org/goodrelations/v1#Wednesday <br />
     *     http://purl.org/goodrelations/v1#Thursday <br />
     *     http://purl.org/goodrelations/v1#Friday <br />
     *     http://purl.org/goodrelations/v1#Saturday <br />
     *     http://purl.org/goodrelations/v1#Sunday <br />
     *     http://purl.org/goodrelations/v1#PublicHolidays <br />
     *         
     * 
     * @return
     *     The dayOfWeek
     */
    @JsonProperty("dayOfWeek")
    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    /**
     * DayOfWeek
     * <p>
     * The day of the week, e.g. used to specify to which day the opening hours of an OpeningHoursSpecification refer.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#Monday <br />
     *     http://purl.org/goodrelations/v1#Tuesday <br />
     *     http://purl.org/goodrelations/v1#Wednesday <br />
     *     http://purl.org/goodrelations/v1#Thursday <br />
     *     http://purl.org/goodrelations/v1#Friday <br />
     *     http://purl.org/goodrelations/v1#Saturday <br />
     *     http://purl.org/goodrelations/v1#Sunday <br />
     *     http://purl.org/goodrelations/v1#PublicHolidays <br />
     *         
     * 
     * @param dayOfWeek
     *     The dayOfWeek
     */
    @JsonProperty("dayOfWeek")
    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public HoursAvailable withDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
        return this;
    }

    /**
     * The closing hour of the place or service on the given day(s) of the week.
     * 
     * @return
     *     The closes
     */
    @JsonProperty("closes")
    public String getCloses() {
        return closes;
    }

    /**
     * The closing hour of the place or service on the given day(s) of the week.
     * 
     * @param closes
     *     The closes
     */
    @JsonProperty("closes")
    public void setCloses(String closes) {
        this.closes = closes;
    }

    public HoursAvailable withCloses(String closes) {
        this.closes = closes;
        return this;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public HoursAvailable withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public HoursAvailable withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public HoursAvailable withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public HoursAvailable withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public HoursAvailable withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * The end of the validity of offer, price specification, or opening hours data.
     * 
     * @return
     *     The validThrough
     */
    @JsonProperty("validThrough")
    public Date getValidThrough() {
        return validThrough;
    }

    /**
     * The end of the validity of offer, price specification, or opening hours data.
     * 
     * @param validThrough
     *     The validThrough
     */
    @JsonProperty("validThrough")
    public void setValidThrough(Date validThrough) {
        this.validThrough = validThrough;
    }

    public HoursAvailable withValidThrough(Date validThrough) {
        this.validThrough = validThrough;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public HoursAvailable withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public HoursAvailable withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public HoursAvailable withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * The date when the item becomes valid.
     * 
     * @return
     *     The validFrom
     */
    @JsonProperty("validFrom")
    public Date getValidFrom() {
        return validFrom;
    }

    /**
     * The date when the item becomes valid.
     * 
     * @param validFrom
     *     The validFrom
     */
    @JsonProperty("validFrom")
    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public HoursAvailable withValidFrom(Date validFrom) {
        this.validFrom = validFrom;
        return this;
    }

    /**
     * The opening hour of the place or service on the given day(s) of the week.
     * 
     * @return
     *     The opens
     */
    @JsonProperty("opens")
    public String getOpens() {
        return opens;
    }

    /**
     * The opening hour of the place or service on the given day(s) of the week.
     * 
     * @param opens
     *     The opens
     */
    @JsonProperty("opens")
    public void setOpens(String opens) {
        this.opens = opens;
    }

    public HoursAvailable withOpens(String opens) {
        this.opens = opens;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public HoursAvailable withName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(dayOfWeek).append(closes).append(potentialAction).append(description).append(sameAs).append(image).append(url).append(validThrough).append(mainEntityOfPage).append(additionalType).append(alternateName).append(validFrom).append(opens).append(name).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof HoursAvailable) == false) {
            return false;
        }
        HoursAvailable rhs = ((HoursAvailable) other);
        return new EqualsBuilder().append(dayOfWeek, rhs.dayOfWeek).append(closes, rhs.closes).append(potentialAction, rhs.potentialAction).append(description, rhs.description).append(sameAs, rhs.sameAs).append(image, rhs.image).append(url, rhs.url).append(validThrough, rhs.validThrough).append(mainEntityOfPage, rhs.mainEntityOfPage).append(additionalType, rhs.additionalType).append(alternateName, rhs.alternateName).append(validFrom, rhs.validFrom).append(opens, rhs.opens).append(name, rhs.name).isEquals();
    }

}
