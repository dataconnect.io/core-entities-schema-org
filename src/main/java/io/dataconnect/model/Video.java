
package io.dataconnect.model;

import java.net.URI;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * VideoObject
 * <p>
 * A video file.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "educationalUse",
    "producer",
    "text",
    "datePublished",
    "alternativeHeadline",
    "accountablePerson",
    "keywords",
    "headline",
    "character",
    "contentRating",
    "associatedArticle",
    "exampleOfWork",
    "publishingPrinciples",
    "embedUrl",
    "dateCreated",
    "publisherImprint",
    "potentialAction",
    "name",
    "associatedMedia",
    "audience",
    "accessibilityControl",
    "translationOfWork",
    "copyrightYear",
    "videoQuality",
    "encodesCreativeWork",
    "creator",
    "musicBy",
    "commentCount",
    "video",
    "encodings",
    "duration",
    "fileFormat",
    "discussionUrl",
    "productionCompany",
    "review",
    "isFamilyFriendly",
    "version",
    "locationCreated",
    "provider",
    "isPartOf",
    "accessibilityHazard",
    "thumbnail",
    "contentSize",
    "contentUrl",
    "educationalAlignment",
    "awards",
    "genre",
    "bitrate",
    "translator",
    "about",
    "license",
    "workExample",
    "encodingFormat",
    "caption",
    "mentions",
    "comment",
    "isBasedOnUrl",
    "encoding",
    "sameAs",
    "image",
    "height",
    "aggregateRating",
    "contributor",
    "thumbnailUrl",
    "mainEntity",
    "alternateName",
    "accessibilityFeature",
    "interactivityType",
    "publication",
    "width",
    "requiresSubscription",
    "editor",
    "mainEntityOfPage",
    "recordedAt",
    "citation",
    "publisher",
    "hasPart",
    "award",
    "offers",
    "copyrightHolder",
    "accessibilityAPI",
    "reviews",
    "learningResourceType",
    "sourceOrganization",
    "videoFrameSize",
    "regionsAllowed",
    "inLanguage",
    "typicalAgeRange",
    "additionalType",
    "author",
    "actor",
    "actors",
    "dateModified",
    "interactionStatistic",
    "description",
    "expires",
    "releasedEvent",
    "director",
    "uploadDate",
    "schemaVersion",
    "transcript",
    "contentLocation",
    "timeRequired",
    "workTranslation",
    "playerType",
    "url",
    "directors",
    "position",
    "audio"
})
public class Video {

    /**
     * The purpose of a work in the context of education; for example, 'assignment', 'group work'.
     * 
     */
    @JsonProperty("educationalUse")
    @JsonPropertyDescription("")
    private String educationalUse;
    /**
     * The person or organization who produced the work (e.g. music album, movie, tv/radio series etc.).
     * 
     */
    @JsonProperty("producer")
    @JsonPropertyDescription("")
    private java.lang.Object producer;
    /**
     * The textual content of this CreativeWork.
     * 
     */
    @JsonProperty("text")
    @JsonPropertyDescription("")
    private String text;
    /**
     * Date of first broadcast/publication.
     * 
     */
    @JsonProperty("datePublished")
    @JsonPropertyDescription("")
    private Date datePublished;
    /**
     * A secondary title of the CreativeWork.
     * 
     */
    @JsonProperty("alternativeHeadline")
    @JsonPropertyDescription("")
    private String alternativeHeadline;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("accountablePerson")
    @JsonPropertyDescription("")
    private AccountablePerson accountablePerson;
    /**
     * Keywords or tags used to describe this content. Multiple entries in a keywords list are typically delimited by commas.
     * 
     */
    @JsonProperty("keywords")
    @JsonPropertyDescription("")
    private String keywords;
    /**
     * Headline of the article.
     * 
     */
    @JsonProperty("headline")
    @JsonPropertyDescription("")
    private String headline;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("character")
    @JsonPropertyDescription("")
    private AccountablePerson character;
    /**
     * Official rating of a piece of content&#x2014;for example,'MPAA PG-13'.
     * 
     */
    @JsonProperty("contentRating")
    @JsonPropertyDescription("")
    private String contentRating;
    /**
     * NewsArticle
     * <p>
     * A news article.
     * 
     */
    @JsonProperty("associatedArticle")
    @JsonPropertyDescription("")
    private AssociatedArticle associatedArticle;
    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     */
    @JsonProperty("exampleOfWork")
    @JsonPropertyDescription("")
    private ExampleOfWork exampleOfWork;
    /**
     * Link to page describing the editorial principles of the organization primarily responsible for the creation of the CreativeWork.
     * 
     */
    @JsonProperty("publishingPrinciples")
    @JsonPropertyDescription("")
    private URI publishingPrinciples;
    /**
     * A URL pointing to a player for a specific video. In general, this is the information in the <code>src</code> element of an <code>embed</code> tag and should not be the same as the content of the <code>loc</code> tag.
     * 
     */
    @JsonProperty("embedUrl")
    @JsonPropertyDescription("")
    private URI embedUrl;
    /**
     * The date on which the CreativeWork was created or the item was added to a DataFeed.
     * 
     */
    @JsonProperty("dateCreated")
    @JsonPropertyDescription("")
    private java.lang.Object dateCreated;
    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     */
    @JsonProperty("publisherImprint")
    @JsonPropertyDescription("")
    private Affiliation publisherImprint;
    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;
    /**
     * MediaObject
     * <p>
     * An image, video, or audio object embedded in a web page. Note that a creative work may have many media objects associated with it on the same web page. For example, a page about a single song (MusicRecording) may have a music video (VideoObject), and a high and low bandwidth audio stream (2 AudioObject's).
     * 
     */
    @JsonProperty("associatedMedia")
    @JsonPropertyDescription("")
    private AssociatedMedia associatedMedia;
    /**
     * Audience
     * <p>
     * Intended audience for an item, i.e. the group for whom the item was created.
     * 
     */
    @JsonProperty("audience")
    @JsonPropertyDescription("")
    private Audience audience;
    /**
     * Identifies input methods that are sufficient to fully control the described resource (<a href="http://www.w3.org/wiki/WebSchemas/Accessibility">WebSchemas wiki lists possible values</a>).
     * 
     */
    @JsonProperty("accessibilityControl")
    @JsonPropertyDescription("")
    private String accessibilityControl;
    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     */
    @JsonProperty("translationOfWork")
    @JsonPropertyDescription("")
    private ExampleOfWork translationOfWork;
    /**
     * The year during which the claimed copyright for the CreativeWork was first asserted.
     * 
     */
    @JsonProperty("copyrightYear")
    @JsonPropertyDescription("")
    private Double copyrightYear;
    /**
     * The quality of the video.
     * 
     */
    @JsonProperty("videoQuality")
    @JsonPropertyDescription("")
    private String videoQuality;
    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     */
    @JsonProperty("encodesCreativeWork")
    @JsonPropertyDescription("")
    private ExampleOfWork encodesCreativeWork;
    /**
     * The creator/author of this CreativeWork. This is the same as the Author property for CreativeWork.
     * 
     */
    @JsonProperty("creator")
    @JsonPropertyDescription("")
    private java.lang.Object creator;
    /**
     * The composer of the soundtrack.
     * 
     */
    @JsonProperty("musicBy")
    @JsonPropertyDescription("")
    private java.lang.Object musicBy;
    /**
     * The number of comments this CreativeWork (e.g. Article, Question or Answer) has received. This is most applicable to works published in Web sites with commenting system; additional comments may exist elsewhere.
     * 
     */
    @JsonProperty("commentCount")
    @JsonPropertyDescription("")
    private Double commentCount;
    /**
     * VideoObject
     * <p>
     * A video file.
     * 
     */
    @JsonProperty("video")
    @JsonPropertyDescription("")
    private Video video;
    /**
     * MediaObject
     * <p>
     * An image, video, or audio object embedded in a web page. Note that a creative work may have many media objects associated with it on the same web page. For example, a page about a single song (MusicRecording) may have a music video (VideoObject), and a high and low bandwidth audio stream (2 AudioObject's).
     * 
     */
    @JsonProperty("encodings")
    @JsonPropertyDescription("")
    private AssociatedMedia encodings;
    /**
     * Duration
     * <p>
     * Quantity: Duration (use  <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 duration format</a>).
     * 
     */
    @JsonProperty("duration")
    @JsonPropertyDescription("")
    private Duration duration;
    /**
     * Media type (aka MIME format, see <a href="http://www.iana.org/assignments/media-types/media-types.xhtml">IANA site</a>) of the content e.g. application/zip of a SoftwareApplication binary. In cases where a CreativeWork has several media type representations, 'encoding' can be used to indicate each MediaObject alongside particular fileFormat information.
     * 
     */
    @JsonProperty("fileFormat")
    @JsonPropertyDescription("")
    private String fileFormat;
    /**
     * A link to the page containing the comments of the CreativeWork.
     * 
     */
    @JsonProperty("discussionUrl")
    @JsonPropertyDescription("")
    private URI discussionUrl;
    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     */
    @JsonProperty("productionCompany")
    @JsonPropertyDescription("")
    private Affiliation productionCompany;
    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     */
    @JsonProperty("review")
    @JsonPropertyDescription("")
    private Review review;
    /**
     * Indicates whether this content is family friendly.
     * 
     */
    @JsonProperty("isFamilyFriendly")
    @JsonPropertyDescription("")
    private Boolean isFamilyFriendly;
    /**
     * The version of the CreativeWork embodied by a specified resource.
     * 
     */
    @JsonProperty("version")
    @JsonPropertyDescription("")
    private Double version;
    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     */
    @JsonProperty("locationCreated")
    @JsonPropertyDescription("")
    private ContainsPlace locationCreated;
    /**
     * The service provider, service operator, or service performer; the goods producer. Another party (a seller) may offer those services or goods on behalf of the provider. A provider may also serve as the seller.
     * 
     */
    @JsonProperty("provider")
    @JsonPropertyDescription("")
    private java.lang.Object provider;
    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     */
    @JsonProperty("isPartOf")
    @JsonPropertyDescription("")
    private ExampleOfWork isPartOf;
    /**
     * A characteristic of the described resource that is physiologically dangerous to some users. Related to WCAG 2.0 guideline 2.3 (<a href="http://www.w3.org/wiki/WebSchemas/Accessibility">WebSchemas wiki lists possible values</a>).
     * 
     */
    @JsonProperty("accessibilityHazard")
    @JsonPropertyDescription("")
    private String accessibilityHazard;
    /**
     * ImageObject
     * <p>
     * An image file.
     * 
     */
    @JsonProperty("thumbnail")
    @JsonPropertyDescription("")
    private Thumbnail thumbnail;
    /**
     * File size in (mega/kilo) bytes.
     * 
     */
    @JsonProperty("contentSize")
    @JsonPropertyDescription("")
    private String contentSize;
    /**
     * Actual bytes of the media object, for example the image file or video file.
     * 
     */
    @JsonProperty("contentUrl")
    @JsonPropertyDescription("")
    private URI contentUrl;
    /**
     * AlignmentObject
     * <p>
     * An intangible item that describes an alignment between a learning resource and a node in an educational framework.
     * 
     */
    @JsonProperty("educationalAlignment")
    @JsonPropertyDescription("")
    private EducationalAlignment educationalAlignment;
    /**
     * Awards won by or for this item.
     * 
     */
    @JsonProperty("awards")
    @JsonPropertyDescription("")
    private String awards;
    /**
     * Genre of the creative work or group.
     * 
     */
    @JsonProperty("genre")
    @JsonPropertyDescription("")
    private java.lang.Object genre;
    /**
     * The bitrate of the media object.
     * 
     */
    @JsonProperty("bitrate")
    @JsonPropertyDescription("")
    private String bitrate;
    /**
     * An agent responsible for rendering a translated work from a source work
     * 
     */
    @JsonProperty("translator")
    @JsonPropertyDescription("")
    private java.lang.Object translator;
    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     */
    @JsonProperty("about")
    @JsonPropertyDescription("")
    private io.dataconnect.model.Object about;
    /**
     * A license document that applies to this content, typically indicated by URL.
     * 
     */
    @JsonProperty("license")
    @JsonPropertyDescription("")
    private java.lang.Object license;
    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     */
    @JsonProperty("workExample")
    @JsonPropertyDescription("")
    private ExampleOfWork workExample;
    /**
     * mp3, mpeg4, etc.
     * 
     */
    @JsonProperty("encodingFormat")
    @JsonPropertyDescription("")
    private String encodingFormat;
    /**
     * The caption for this object.
     * 
     */
    @JsonProperty("caption")
    @JsonPropertyDescription("")
    private String caption;
    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     */
    @JsonProperty("mentions")
    @JsonPropertyDescription("")
    private io.dataconnect.model.Object mentions;
    /**
     * Comment
     * <p>
     * A comment on an item - for example, a comment on a blog post. The comment's content is expressed via the "text" property, and its topic via "about", properties shared with all CreativeWorks.
     * 
     */
    @JsonProperty("comment")
    @JsonPropertyDescription("")
    private Comment comment;
    /**
     * A resource that was used in the creation of this resource. This term can be repeated for multiple sources. For example, http://example.com/great-multiplication-intro.html.
     * 
     */
    @JsonProperty("isBasedOnUrl")
    @JsonPropertyDescription("")
    private URI isBasedOnUrl;
    /**
     * MediaObject
     * <p>
     * An image, video, or audio object embedded in a web page. Note that a creative work may have many media objects associated with it on the same web page. For example, a page about a single song (MusicRecording) may have a music video (VideoObject), and a high and low bandwidth audio stream (2 AudioObject's).
     * 
     */
    @JsonProperty("encoding")
    @JsonPropertyDescription("")
    private AssociatedMedia encoding;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * The height of the item.
     * 
     */
    @JsonProperty("height")
    @JsonPropertyDescription("")
    private java.lang.Object height;
    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     */
    @JsonProperty("aggregateRating")
    @JsonPropertyDescription("")
    private AggregateRating aggregateRating;
    /**
     * A secondary contributor to the CreativeWork.
     * 
     */
    @JsonProperty("contributor")
    @JsonPropertyDescription("")
    private java.lang.Object contributor;
    /**
     * A thumbnail image relevant to the Thing.
     * 
     */
    @JsonProperty("thumbnailUrl")
    @JsonPropertyDescription("")
    private URI thumbnailUrl;
    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     */
    @JsonProperty("mainEntity")
    @JsonPropertyDescription("")
    private io.dataconnect.model.Object mainEntity;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * Content features of the resource, such as accessible media, alternatives and supported enhancements for accessibility (<a href="http://www.w3.org/wiki/WebSchemas/Accessibility">WebSchemas wiki lists possible values</a>).
     * 
     */
    @JsonProperty("accessibilityFeature")
    @JsonPropertyDescription("")
    private String accessibilityFeature;
    /**
     * The predominant mode of learning supported by the learning resource. Acceptable values are 'active', 'expositive', or 'mixed'.
     * 
     */
    @JsonProperty("interactivityType")
    @JsonPropertyDescription("")
    private String interactivityType;
    /**
     * PublicationEvent
     * <p>
     * A PublicationEvent corresponds indifferently to the event of publication for a CreativeWork of any type e.g. a broadcast event, an on-demand event, a book/journal publication via a variety of delivery media.
     * 
     */
    @JsonProperty("publication")
    @JsonPropertyDescription("")
    private Publication publication;
    /**
     * The width of the item.
     * 
     */
    @JsonProperty("width")
    @JsonPropertyDescription("")
    private java.lang.Object width;
    /**
     * Indicates if use of the media require a subscription  (either paid or free). Allowed values are <code>true</code> or <code>false</code> (note that an earlier version had 'yes', 'no').
     * 
     */
    @JsonProperty("requiresSubscription")
    @JsonPropertyDescription("")
    private Boolean requiresSubscription;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("editor")
    @JsonPropertyDescription("")
    private AccountablePerson editor;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     */
    @JsonProperty("recordedAt")
    @JsonPropertyDescription("")
    private Event recordedAt;
    /**
     * A citation or reference to another creative work, such as another publication, web page, scholarly article, etc.
     * 
     */
    @JsonProperty("citation")
    @JsonPropertyDescription("")
    private java.lang.Object citation;
    /**
     * The publisher of the creative work.
     * 
     */
    @JsonProperty("publisher")
    @JsonPropertyDescription("")
    private java.lang.Object publisher;
    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     */
    @JsonProperty("hasPart")
    @JsonPropertyDescription("")
    private ExampleOfWork hasPart;
    /**
     * An award won by or for this item.
     * 
     */
    @JsonProperty("award")
    @JsonPropertyDescription("")
    private String award;
    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     */
    @JsonProperty("offers")
    @JsonPropertyDescription("")
    private Offer offers;
    /**
     * The party holding the legal copyright to the CreativeWork.
     * 
     */
    @JsonProperty("copyrightHolder")
    @JsonPropertyDescription("")
    private java.lang.Object copyrightHolder;
    /**
     * Indicates that the resource is compatible with the referenced accessibility API (<a href="http://www.w3.org/wiki/WebSchemas/Accessibility">WebSchemas wiki lists possible values</a>).
     * 
     */
    @JsonProperty("accessibilityAPI")
    @JsonPropertyDescription("")
    private String accessibilityAPI;
    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     */
    @JsonProperty("reviews")
    @JsonPropertyDescription("")
    private Review reviews;
    /**
     * The predominant type or kind characterizing the learning resource. For example, 'presentation', 'handout'.
     * 
     */
    @JsonProperty("learningResourceType")
    @JsonPropertyDescription("")
    private String learningResourceType;
    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     */
    @JsonProperty("sourceOrganization")
    @JsonPropertyDescription("")
    private Affiliation sourceOrganization;
    /**
     * The frame size of the video.
     * 
     */
    @JsonProperty("videoFrameSize")
    @JsonPropertyDescription("")
    private String videoFrameSize;
    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     */
    @JsonProperty("regionsAllowed")
    @JsonPropertyDescription("")
    private ContainsPlace regionsAllowed;
    /**
     * The language of the content or performance or used in an action. Please use one of the language codes from the <a href='http://tools.ietf.org/html/bcp47'>IETF BCP 47 standard</a>.
     * 
     */
    @JsonProperty("inLanguage")
    @JsonPropertyDescription("")
    private java.lang.Object inLanguage;
    /**
     * The typical expected age range, e.g. '7-9', '11-'.
     * 
     */
    @JsonProperty("typicalAgeRange")
    @JsonPropertyDescription("")
    private String typicalAgeRange;
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * The author of this content. Please note that author is special in that HTML 5 provides a special mechanism for indicating authorship via the rel tag. That is equivalent to this and may be used interchangeably.
     * 
     */
    @JsonProperty("author")
    @JsonPropertyDescription("")
    private java.lang.Object author;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("actor")
    @JsonPropertyDescription("")
    private AccountablePerson actor;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("actors")
    @JsonPropertyDescription("")
    private AccountablePerson actors;
    /**
     * The date on which the CreativeWork was most recently modified or when the item's entry was modified within a DataFeed.
     * 
     */
    @JsonProperty("dateModified")
    @JsonPropertyDescription("")
    private java.lang.Object dateModified;
    /**
     * InteractionCounter
     * <p>
     * A summary of how users have interacted with this CreativeWork. In most cases, authors will use a subtype to specify the specific type of interaction.
     * 
     */
    @JsonProperty("interactionStatistic")
    @JsonPropertyDescription("")
    private InteractionStatistic interactionStatistic;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;
    /**
     * Date the content expires and is no longer useful or available. Useful for videos.
     * 
     */
    @JsonProperty("expires")
    @JsonPropertyDescription("")
    private Date expires;
    /**
     * PublicationEvent
     * <p>
     * A PublicationEvent corresponds indifferently to the event of publication for a CreativeWork of any type e.g. a broadcast event, an on-demand event, a book/journal publication via a variety of delivery media.
     * 
     */
    @JsonProperty("releasedEvent")
    @JsonPropertyDescription("")
    private Publication releasedEvent;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("director")
    @JsonPropertyDescription("")
    private AccountablePerson director;
    /**
     * Date when this media object was uploaded to this site.
     * 
     */
    @JsonProperty("uploadDate")
    @JsonPropertyDescription("")
    private Date uploadDate;
    /**
     * Indicates (by URL or string) a particular version of a schema used in some CreativeWork. For example, a document could declare a schemaVersion using a URL such as http://schema.org/version/2.0/ if precise indication of schema version was required by some application. 
     * 
     */
    @JsonProperty("schemaVersion")
    @JsonPropertyDescription("")
    private java.lang.Object schemaVersion;
    /**
     * If this MediaObject is an AudioObject or VideoObject, the transcript of that object.
     * 
     */
    @JsonProperty("transcript")
    @JsonPropertyDescription("")
    private String transcript;
    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     */
    @JsonProperty("contentLocation")
    @JsonPropertyDescription("")
    private ContainsPlace contentLocation;
    /**
     * Duration
     * <p>
     * Quantity: Duration (use  <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 duration format</a>).
     * 
     */
    @JsonProperty("timeRequired")
    @JsonPropertyDescription("")
    private Duration timeRequired;
    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     */
    @JsonProperty("workTranslation")
    @JsonPropertyDescription("")
    private ExampleOfWork workTranslation;
    /**
     * Player type required&#x2014;for example, Flash or Silverlight.
     * 
     */
    @JsonProperty("playerType")
    @JsonPropertyDescription("")
    private String playerType;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("directors")
    @JsonPropertyDescription("")
    private AccountablePerson directors;
    /**
     * The position of an item in a series or sequence of items.
     * 
     */
    @JsonProperty("position")
    @JsonPropertyDescription("")
    private java.lang.Object position;
    /**
     * AudioObject
     * <p>
     * An audio file.
     * 
     */
    @JsonProperty("audio")
    @JsonPropertyDescription("")
    private Audio audio;

    /**
     * The purpose of a work in the context of education; for example, 'assignment', 'group work'.
     * 
     * @return
     *     The educationalUse
     */
    @JsonProperty("educationalUse")
    public String getEducationalUse() {
        return educationalUse;
    }

    /**
     * The purpose of a work in the context of education; for example, 'assignment', 'group work'.
     * 
     * @param educationalUse
     *     The educationalUse
     */
    @JsonProperty("educationalUse")
    public void setEducationalUse(String educationalUse) {
        this.educationalUse = educationalUse;
    }

    public Video withEducationalUse(String educationalUse) {
        this.educationalUse = educationalUse;
        return this;
    }

    /**
     * The person or organization who produced the work (e.g. music album, movie, tv/radio series etc.).
     * 
     * @return
     *     The producer
     */
    @JsonProperty("producer")
    public java.lang.Object getProducer() {
        return producer;
    }

    /**
     * The person or organization who produced the work (e.g. music album, movie, tv/radio series etc.).
     * 
     * @param producer
     *     The producer
     */
    @JsonProperty("producer")
    public void setProducer(java.lang.Object producer) {
        this.producer = producer;
    }

    public Video withProducer(java.lang.Object producer) {
        this.producer = producer;
        return this;
    }

    /**
     * The textual content of this CreativeWork.
     * 
     * @return
     *     The text
     */
    @JsonProperty("text")
    public String getText() {
        return text;
    }

    /**
     * The textual content of this CreativeWork.
     * 
     * @param text
     *     The text
     */
    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    public Video withText(String text) {
        this.text = text;
        return this;
    }

    /**
     * Date of first broadcast/publication.
     * 
     * @return
     *     The datePublished
     */
    @JsonProperty("datePublished")
    public Date getDatePublished() {
        return datePublished;
    }

    /**
     * Date of first broadcast/publication.
     * 
     * @param datePublished
     *     The datePublished
     */
    @JsonProperty("datePublished")
    public void setDatePublished(Date datePublished) {
        this.datePublished = datePublished;
    }

    public Video withDatePublished(Date datePublished) {
        this.datePublished = datePublished;
        return this;
    }

    /**
     * A secondary title of the CreativeWork.
     * 
     * @return
     *     The alternativeHeadline
     */
    @JsonProperty("alternativeHeadline")
    public String getAlternativeHeadline() {
        return alternativeHeadline;
    }

    /**
     * A secondary title of the CreativeWork.
     * 
     * @param alternativeHeadline
     *     The alternativeHeadline
     */
    @JsonProperty("alternativeHeadline")
    public void setAlternativeHeadline(String alternativeHeadline) {
        this.alternativeHeadline = alternativeHeadline;
    }

    public Video withAlternativeHeadline(String alternativeHeadline) {
        this.alternativeHeadline = alternativeHeadline;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The accountablePerson
     */
    @JsonProperty("accountablePerson")
    public AccountablePerson getAccountablePerson() {
        return accountablePerson;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param accountablePerson
     *     The accountablePerson
     */
    @JsonProperty("accountablePerson")
    public void setAccountablePerson(AccountablePerson accountablePerson) {
        this.accountablePerson = accountablePerson;
    }

    public Video withAccountablePerson(AccountablePerson accountablePerson) {
        this.accountablePerson = accountablePerson;
        return this;
    }

    /**
     * Keywords or tags used to describe this content. Multiple entries in a keywords list are typically delimited by commas.
     * 
     * @return
     *     The keywords
     */
    @JsonProperty("keywords")
    public String getKeywords() {
        return keywords;
    }

    /**
     * Keywords or tags used to describe this content. Multiple entries in a keywords list are typically delimited by commas.
     * 
     * @param keywords
     *     The keywords
     */
    @JsonProperty("keywords")
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public Video withKeywords(String keywords) {
        this.keywords = keywords;
        return this;
    }

    /**
     * Headline of the article.
     * 
     * @return
     *     The headline
     */
    @JsonProperty("headline")
    public String getHeadline() {
        return headline;
    }

    /**
     * Headline of the article.
     * 
     * @param headline
     *     The headline
     */
    @JsonProperty("headline")
    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public Video withHeadline(String headline) {
        this.headline = headline;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The character
     */
    @JsonProperty("character")
    public AccountablePerson getCharacter() {
        return character;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param character
     *     The character
     */
    @JsonProperty("character")
    public void setCharacter(AccountablePerson character) {
        this.character = character;
    }

    public Video withCharacter(AccountablePerson character) {
        this.character = character;
        return this;
    }

    /**
     * Official rating of a piece of content&#x2014;for example,'MPAA PG-13'.
     * 
     * @return
     *     The contentRating
     */
    @JsonProperty("contentRating")
    public String getContentRating() {
        return contentRating;
    }

    /**
     * Official rating of a piece of content&#x2014;for example,'MPAA PG-13'.
     * 
     * @param contentRating
     *     The contentRating
     */
    @JsonProperty("contentRating")
    public void setContentRating(String contentRating) {
        this.contentRating = contentRating;
    }

    public Video withContentRating(String contentRating) {
        this.contentRating = contentRating;
        return this;
    }

    /**
     * NewsArticle
     * <p>
     * A news article.
     * 
     * @return
     *     The associatedArticle
     */
    @JsonProperty("associatedArticle")
    public AssociatedArticle getAssociatedArticle() {
        return associatedArticle;
    }

    /**
     * NewsArticle
     * <p>
     * A news article.
     * 
     * @param associatedArticle
     *     The associatedArticle
     */
    @JsonProperty("associatedArticle")
    public void setAssociatedArticle(AssociatedArticle associatedArticle) {
        this.associatedArticle = associatedArticle;
    }

    public Video withAssociatedArticle(AssociatedArticle associatedArticle) {
        this.associatedArticle = associatedArticle;
        return this;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @return
     *     The exampleOfWork
     */
    @JsonProperty("exampleOfWork")
    public ExampleOfWork getExampleOfWork() {
        return exampleOfWork;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @param exampleOfWork
     *     The exampleOfWork
     */
    @JsonProperty("exampleOfWork")
    public void setExampleOfWork(ExampleOfWork exampleOfWork) {
        this.exampleOfWork = exampleOfWork;
    }

    public Video withExampleOfWork(ExampleOfWork exampleOfWork) {
        this.exampleOfWork = exampleOfWork;
        return this;
    }

    /**
     * Link to page describing the editorial principles of the organization primarily responsible for the creation of the CreativeWork.
     * 
     * @return
     *     The publishingPrinciples
     */
    @JsonProperty("publishingPrinciples")
    public URI getPublishingPrinciples() {
        return publishingPrinciples;
    }

    /**
     * Link to page describing the editorial principles of the organization primarily responsible for the creation of the CreativeWork.
     * 
     * @param publishingPrinciples
     *     The publishingPrinciples
     */
    @JsonProperty("publishingPrinciples")
    public void setPublishingPrinciples(URI publishingPrinciples) {
        this.publishingPrinciples = publishingPrinciples;
    }

    public Video withPublishingPrinciples(URI publishingPrinciples) {
        this.publishingPrinciples = publishingPrinciples;
        return this;
    }

    /**
     * A URL pointing to a player for a specific video. In general, this is the information in the <code>src</code> element of an <code>embed</code> tag and should not be the same as the content of the <code>loc</code> tag.
     * 
     * @return
     *     The embedUrl
     */
    @JsonProperty("embedUrl")
    public URI getEmbedUrl() {
        return embedUrl;
    }

    /**
     * A URL pointing to a player for a specific video. In general, this is the information in the <code>src</code> element of an <code>embed</code> tag and should not be the same as the content of the <code>loc</code> tag.
     * 
     * @param embedUrl
     *     The embedUrl
     */
    @JsonProperty("embedUrl")
    public void setEmbedUrl(URI embedUrl) {
        this.embedUrl = embedUrl;
    }

    public Video withEmbedUrl(URI embedUrl) {
        this.embedUrl = embedUrl;
        return this;
    }

    /**
     * The date on which the CreativeWork was created or the item was added to a DataFeed.
     * 
     * @return
     *     The dateCreated
     */
    @JsonProperty("dateCreated")
    public java.lang.Object getDateCreated() {
        return dateCreated;
    }

    /**
     * The date on which the CreativeWork was created or the item was added to a DataFeed.
     * 
     * @param dateCreated
     *     The dateCreated
     */
    @JsonProperty("dateCreated")
    public void setDateCreated(java.lang.Object dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Video withDateCreated(java.lang.Object dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @return
     *     The publisherImprint
     */
    @JsonProperty("publisherImprint")
    public Affiliation getPublisherImprint() {
        return publisherImprint;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @param publisherImprint
     *     The publisherImprint
     */
    @JsonProperty("publisherImprint")
    public void setPublisherImprint(Affiliation publisherImprint) {
        this.publisherImprint = publisherImprint;
    }

    public Video withPublisherImprint(Affiliation publisherImprint) {
        this.publisherImprint = publisherImprint;
        return this;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public Video withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Video withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * MediaObject
     * <p>
     * An image, video, or audio object embedded in a web page. Note that a creative work may have many media objects associated with it on the same web page. For example, a page about a single song (MusicRecording) may have a music video (VideoObject), and a high and low bandwidth audio stream (2 AudioObject's).
     * 
     * @return
     *     The associatedMedia
     */
    @JsonProperty("associatedMedia")
    public AssociatedMedia getAssociatedMedia() {
        return associatedMedia;
    }

    /**
     * MediaObject
     * <p>
     * An image, video, or audio object embedded in a web page. Note that a creative work may have many media objects associated with it on the same web page. For example, a page about a single song (MusicRecording) may have a music video (VideoObject), and a high and low bandwidth audio stream (2 AudioObject's).
     * 
     * @param associatedMedia
     *     The associatedMedia
     */
    @JsonProperty("associatedMedia")
    public void setAssociatedMedia(AssociatedMedia associatedMedia) {
        this.associatedMedia = associatedMedia;
    }

    public Video withAssociatedMedia(AssociatedMedia associatedMedia) {
        this.associatedMedia = associatedMedia;
        return this;
    }

    /**
     * Audience
     * <p>
     * Intended audience for an item, i.e. the group for whom the item was created.
     * 
     * @return
     *     The audience
     */
    @JsonProperty("audience")
    public Audience getAudience() {
        return audience;
    }

    /**
     * Audience
     * <p>
     * Intended audience for an item, i.e. the group for whom the item was created.
     * 
     * @param audience
     *     The audience
     */
    @JsonProperty("audience")
    public void setAudience(Audience audience) {
        this.audience = audience;
    }

    public Video withAudience(Audience audience) {
        this.audience = audience;
        return this;
    }

    /**
     * Identifies input methods that are sufficient to fully control the described resource (<a href="http://www.w3.org/wiki/WebSchemas/Accessibility">WebSchemas wiki lists possible values</a>).
     * 
     * @return
     *     The accessibilityControl
     */
    @JsonProperty("accessibilityControl")
    public String getAccessibilityControl() {
        return accessibilityControl;
    }

    /**
     * Identifies input methods that are sufficient to fully control the described resource (<a href="http://www.w3.org/wiki/WebSchemas/Accessibility">WebSchemas wiki lists possible values</a>).
     * 
     * @param accessibilityControl
     *     The accessibilityControl
     */
    @JsonProperty("accessibilityControl")
    public void setAccessibilityControl(String accessibilityControl) {
        this.accessibilityControl = accessibilityControl;
    }

    public Video withAccessibilityControl(String accessibilityControl) {
        this.accessibilityControl = accessibilityControl;
        return this;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @return
     *     The translationOfWork
     */
    @JsonProperty("translationOfWork")
    public ExampleOfWork getTranslationOfWork() {
        return translationOfWork;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @param translationOfWork
     *     The translationOfWork
     */
    @JsonProperty("translationOfWork")
    public void setTranslationOfWork(ExampleOfWork translationOfWork) {
        this.translationOfWork = translationOfWork;
    }

    public Video withTranslationOfWork(ExampleOfWork translationOfWork) {
        this.translationOfWork = translationOfWork;
        return this;
    }

    /**
     * The year during which the claimed copyright for the CreativeWork was first asserted.
     * 
     * @return
     *     The copyrightYear
     */
    @JsonProperty("copyrightYear")
    public Double getCopyrightYear() {
        return copyrightYear;
    }

    /**
     * The year during which the claimed copyright for the CreativeWork was first asserted.
     * 
     * @param copyrightYear
     *     The copyrightYear
     */
    @JsonProperty("copyrightYear")
    public void setCopyrightYear(Double copyrightYear) {
        this.copyrightYear = copyrightYear;
    }

    public Video withCopyrightYear(Double copyrightYear) {
        this.copyrightYear = copyrightYear;
        return this;
    }

    /**
     * The quality of the video.
     * 
     * @return
     *     The videoQuality
     */
    @JsonProperty("videoQuality")
    public String getVideoQuality() {
        return videoQuality;
    }

    /**
     * The quality of the video.
     * 
     * @param videoQuality
     *     The videoQuality
     */
    @JsonProperty("videoQuality")
    public void setVideoQuality(String videoQuality) {
        this.videoQuality = videoQuality;
    }

    public Video withVideoQuality(String videoQuality) {
        this.videoQuality = videoQuality;
        return this;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @return
     *     The encodesCreativeWork
     */
    @JsonProperty("encodesCreativeWork")
    public ExampleOfWork getEncodesCreativeWork() {
        return encodesCreativeWork;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @param encodesCreativeWork
     *     The encodesCreativeWork
     */
    @JsonProperty("encodesCreativeWork")
    public void setEncodesCreativeWork(ExampleOfWork encodesCreativeWork) {
        this.encodesCreativeWork = encodesCreativeWork;
    }

    public Video withEncodesCreativeWork(ExampleOfWork encodesCreativeWork) {
        this.encodesCreativeWork = encodesCreativeWork;
        return this;
    }

    /**
     * The creator/author of this CreativeWork. This is the same as the Author property for CreativeWork.
     * 
     * @return
     *     The creator
     */
    @JsonProperty("creator")
    public java.lang.Object getCreator() {
        return creator;
    }

    /**
     * The creator/author of this CreativeWork. This is the same as the Author property for CreativeWork.
     * 
     * @param creator
     *     The creator
     */
    @JsonProperty("creator")
    public void setCreator(java.lang.Object creator) {
        this.creator = creator;
    }

    public Video withCreator(java.lang.Object creator) {
        this.creator = creator;
        return this;
    }

    /**
     * The composer of the soundtrack.
     * 
     * @return
     *     The musicBy
     */
    @JsonProperty("musicBy")
    public java.lang.Object getMusicBy() {
        return musicBy;
    }

    /**
     * The composer of the soundtrack.
     * 
     * @param musicBy
     *     The musicBy
     */
    @JsonProperty("musicBy")
    public void setMusicBy(java.lang.Object musicBy) {
        this.musicBy = musicBy;
    }

    public Video withMusicBy(java.lang.Object musicBy) {
        this.musicBy = musicBy;
        return this;
    }

    /**
     * The number of comments this CreativeWork (e.g. Article, Question or Answer) has received. This is most applicable to works published in Web sites with commenting system; additional comments may exist elsewhere.
     * 
     * @return
     *     The commentCount
     */
    @JsonProperty("commentCount")
    public Double getCommentCount() {
        return commentCount;
    }

    /**
     * The number of comments this CreativeWork (e.g. Article, Question or Answer) has received. This is most applicable to works published in Web sites with commenting system; additional comments may exist elsewhere.
     * 
     * @param commentCount
     *     The commentCount
     */
    @JsonProperty("commentCount")
    public void setCommentCount(Double commentCount) {
        this.commentCount = commentCount;
    }

    public Video withCommentCount(Double commentCount) {
        this.commentCount = commentCount;
        return this;
    }

    /**
     * VideoObject
     * <p>
     * A video file.
     * 
     * @return
     *     The video
     */
    @JsonProperty("video")
    public Video getVideo() {
        return video;
    }

    /**
     * VideoObject
     * <p>
     * A video file.
     * 
     * @param video
     *     The video
     */
    @JsonProperty("video")
    public void setVideo(Video video) {
        this.video = video;
    }

    public Video withVideo(Video video) {
        this.video = video;
        return this;
    }

    /**
     * MediaObject
     * <p>
     * An image, video, or audio object embedded in a web page. Note that a creative work may have many media objects associated with it on the same web page. For example, a page about a single song (MusicRecording) may have a music video (VideoObject), and a high and low bandwidth audio stream (2 AudioObject's).
     * 
     * @return
     *     The encodings
     */
    @JsonProperty("encodings")
    public AssociatedMedia getEncodings() {
        return encodings;
    }

    /**
     * MediaObject
     * <p>
     * An image, video, or audio object embedded in a web page. Note that a creative work may have many media objects associated with it on the same web page. For example, a page about a single song (MusicRecording) may have a music video (VideoObject), and a high and low bandwidth audio stream (2 AudioObject's).
     * 
     * @param encodings
     *     The encodings
     */
    @JsonProperty("encodings")
    public void setEncodings(AssociatedMedia encodings) {
        this.encodings = encodings;
    }

    public Video withEncodings(AssociatedMedia encodings) {
        this.encodings = encodings;
        return this;
    }

    /**
     * Duration
     * <p>
     * Quantity: Duration (use  <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 duration format</a>).
     * 
     * @return
     *     The duration
     */
    @JsonProperty("duration")
    public Duration getDuration() {
        return duration;
    }

    /**
     * Duration
     * <p>
     * Quantity: Duration (use  <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 duration format</a>).
     * 
     * @param duration
     *     The duration
     */
    @JsonProperty("duration")
    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Video withDuration(Duration duration) {
        this.duration = duration;
        return this;
    }

    /**
     * Media type (aka MIME format, see <a href="http://www.iana.org/assignments/media-types/media-types.xhtml">IANA site</a>) of the content e.g. application/zip of a SoftwareApplication binary. In cases where a CreativeWork has several media type representations, 'encoding' can be used to indicate each MediaObject alongside particular fileFormat information.
     * 
     * @return
     *     The fileFormat
     */
    @JsonProperty("fileFormat")
    public String getFileFormat() {
        return fileFormat;
    }

    /**
     * Media type (aka MIME format, see <a href="http://www.iana.org/assignments/media-types/media-types.xhtml">IANA site</a>) of the content e.g. application/zip of a SoftwareApplication binary. In cases where a CreativeWork has several media type representations, 'encoding' can be used to indicate each MediaObject alongside particular fileFormat information.
     * 
     * @param fileFormat
     *     The fileFormat
     */
    @JsonProperty("fileFormat")
    public void setFileFormat(String fileFormat) {
        this.fileFormat = fileFormat;
    }

    public Video withFileFormat(String fileFormat) {
        this.fileFormat = fileFormat;
        return this;
    }

    /**
     * A link to the page containing the comments of the CreativeWork.
     * 
     * @return
     *     The discussionUrl
     */
    @JsonProperty("discussionUrl")
    public URI getDiscussionUrl() {
        return discussionUrl;
    }

    /**
     * A link to the page containing the comments of the CreativeWork.
     * 
     * @param discussionUrl
     *     The discussionUrl
     */
    @JsonProperty("discussionUrl")
    public void setDiscussionUrl(URI discussionUrl) {
        this.discussionUrl = discussionUrl;
    }

    public Video withDiscussionUrl(URI discussionUrl) {
        this.discussionUrl = discussionUrl;
        return this;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @return
     *     The productionCompany
     */
    @JsonProperty("productionCompany")
    public Affiliation getProductionCompany() {
        return productionCompany;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @param productionCompany
     *     The productionCompany
     */
    @JsonProperty("productionCompany")
    public void setProductionCompany(Affiliation productionCompany) {
        this.productionCompany = productionCompany;
    }

    public Video withProductionCompany(Affiliation productionCompany) {
        this.productionCompany = productionCompany;
        return this;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @return
     *     The review
     */
    @JsonProperty("review")
    public Review getReview() {
        return review;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @param review
     *     The review
     */
    @JsonProperty("review")
    public void setReview(Review review) {
        this.review = review;
    }

    public Video withReview(Review review) {
        this.review = review;
        return this;
    }

    /**
     * Indicates whether this content is family friendly.
     * 
     * @return
     *     The isFamilyFriendly
     */
    @JsonProperty("isFamilyFriendly")
    public Boolean getIsFamilyFriendly() {
        return isFamilyFriendly;
    }

    /**
     * Indicates whether this content is family friendly.
     * 
     * @param isFamilyFriendly
     *     The isFamilyFriendly
     */
    @JsonProperty("isFamilyFriendly")
    public void setIsFamilyFriendly(Boolean isFamilyFriendly) {
        this.isFamilyFriendly = isFamilyFriendly;
    }

    public Video withIsFamilyFriendly(Boolean isFamilyFriendly) {
        this.isFamilyFriendly = isFamilyFriendly;
        return this;
    }

    /**
     * The version of the CreativeWork embodied by a specified resource.
     * 
     * @return
     *     The version
     */
    @JsonProperty("version")
    public Double getVersion() {
        return version;
    }

    /**
     * The version of the CreativeWork embodied by a specified resource.
     * 
     * @param version
     *     The version
     */
    @JsonProperty("version")
    public void setVersion(Double version) {
        this.version = version;
    }

    public Video withVersion(Double version) {
        this.version = version;
        return this;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @return
     *     The locationCreated
     */
    @JsonProperty("locationCreated")
    public ContainsPlace getLocationCreated() {
        return locationCreated;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @param locationCreated
     *     The locationCreated
     */
    @JsonProperty("locationCreated")
    public void setLocationCreated(ContainsPlace locationCreated) {
        this.locationCreated = locationCreated;
    }

    public Video withLocationCreated(ContainsPlace locationCreated) {
        this.locationCreated = locationCreated;
        return this;
    }

    /**
     * The service provider, service operator, or service performer; the goods producer. Another party (a seller) may offer those services or goods on behalf of the provider. A provider may also serve as the seller.
     * 
     * @return
     *     The provider
     */
    @JsonProperty("provider")
    public java.lang.Object getProvider() {
        return provider;
    }

    /**
     * The service provider, service operator, or service performer; the goods producer. Another party (a seller) may offer those services or goods on behalf of the provider. A provider may also serve as the seller.
     * 
     * @param provider
     *     The provider
     */
    @JsonProperty("provider")
    public void setProvider(java.lang.Object provider) {
        this.provider = provider;
    }

    public Video withProvider(java.lang.Object provider) {
        this.provider = provider;
        return this;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @return
     *     The isPartOf
     */
    @JsonProperty("isPartOf")
    public ExampleOfWork getIsPartOf() {
        return isPartOf;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @param isPartOf
     *     The isPartOf
     */
    @JsonProperty("isPartOf")
    public void setIsPartOf(ExampleOfWork isPartOf) {
        this.isPartOf = isPartOf;
    }

    public Video withIsPartOf(ExampleOfWork isPartOf) {
        this.isPartOf = isPartOf;
        return this;
    }

    /**
     * A characteristic of the described resource that is physiologically dangerous to some users. Related to WCAG 2.0 guideline 2.3 (<a href="http://www.w3.org/wiki/WebSchemas/Accessibility">WebSchemas wiki lists possible values</a>).
     * 
     * @return
     *     The accessibilityHazard
     */
    @JsonProperty("accessibilityHazard")
    public String getAccessibilityHazard() {
        return accessibilityHazard;
    }

    /**
     * A characteristic of the described resource that is physiologically dangerous to some users. Related to WCAG 2.0 guideline 2.3 (<a href="http://www.w3.org/wiki/WebSchemas/Accessibility">WebSchemas wiki lists possible values</a>).
     * 
     * @param accessibilityHazard
     *     The accessibilityHazard
     */
    @JsonProperty("accessibilityHazard")
    public void setAccessibilityHazard(String accessibilityHazard) {
        this.accessibilityHazard = accessibilityHazard;
    }

    public Video withAccessibilityHazard(String accessibilityHazard) {
        this.accessibilityHazard = accessibilityHazard;
        return this;
    }

    /**
     * ImageObject
     * <p>
     * An image file.
     * 
     * @return
     *     The thumbnail
     */
    @JsonProperty("thumbnail")
    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    /**
     * ImageObject
     * <p>
     * An image file.
     * 
     * @param thumbnail
     *     The thumbnail
     */
    @JsonProperty("thumbnail")
    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Video withThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
        return this;
    }

    /**
     * File size in (mega/kilo) bytes.
     * 
     * @return
     *     The contentSize
     */
    @JsonProperty("contentSize")
    public String getContentSize() {
        return contentSize;
    }

    /**
     * File size in (mega/kilo) bytes.
     * 
     * @param contentSize
     *     The contentSize
     */
    @JsonProperty("contentSize")
    public void setContentSize(String contentSize) {
        this.contentSize = contentSize;
    }

    public Video withContentSize(String contentSize) {
        this.contentSize = contentSize;
        return this;
    }

    /**
     * Actual bytes of the media object, for example the image file or video file.
     * 
     * @return
     *     The contentUrl
     */
    @JsonProperty("contentUrl")
    public URI getContentUrl() {
        return contentUrl;
    }

    /**
     * Actual bytes of the media object, for example the image file or video file.
     * 
     * @param contentUrl
     *     The contentUrl
     */
    @JsonProperty("contentUrl")
    public void setContentUrl(URI contentUrl) {
        this.contentUrl = contentUrl;
    }

    public Video withContentUrl(URI contentUrl) {
        this.contentUrl = contentUrl;
        return this;
    }

    /**
     * AlignmentObject
     * <p>
     * An intangible item that describes an alignment between a learning resource and a node in an educational framework.
     * 
     * @return
     *     The educationalAlignment
     */
    @JsonProperty("educationalAlignment")
    public EducationalAlignment getEducationalAlignment() {
        return educationalAlignment;
    }

    /**
     * AlignmentObject
     * <p>
     * An intangible item that describes an alignment between a learning resource and a node in an educational framework.
     * 
     * @param educationalAlignment
     *     The educationalAlignment
     */
    @JsonProperty("educationalAlignment")
    public void setEducationalAlignment(EducationalAlignment educationalAlignment) {
        this.educationalAlignment = educationalAlignment;
    }

    public Video withEducationalAlignment(EducationalAlignment educationalAlignment) {
        this.educationalAlignment = educationalAlignment;
        return this;
    }

    /**
     * Awards won by or for this item.
     * 
     * @return
     *     The awards
     */
    @JsonProperty("awards")
    public String getAwards() {
        return awards;
    }

    /**
     * Awards won by or for this item.
     * 
     * @param awards
     *     The awards
     */
    @JsonProperty("awards")
    public void setAwards(String awards) {
        this.awards = awards;
    }

    public Video withAwards(String awards) {
        this.awards = awards;
        return this;
    }

    /**
     * Genre of the creative work or group.
     * 
     * @return
     *     The genre
     */
    @JsonProperty("genre")
    public java.lang.Object getGenre() {
        return genre;
    }

    /**
     * Genre of the creative work or group.
     * 
     * @param genre
     *     The genre
     */
    @JsonProperty("genre")
    public void setGenre(java.lang.Object genre) {
        this.genre = genre;
    }

    public Video withGenre(java.lang.Object genre) {
        this.genre = genre;
        return this;
    }

    /**
     * The bitrate of the media object.
     * 
     * @return
     *     The bitrate
     */
    @JsonProperty("bitrate")
    public String getBitrate() {
        return bitrate;
    }

    /**
     * The bitrate of the media object.
     * 
     * @param bitrate
     *     The bitrate
     */
    @JsonProperty("bitrate")
    public void setBitrate(String bitrate) {
        this.bitrate = bitrate;
    }

    public Video withBitrate(String bitrate) {
        this.bitrate = bitrate;
        return this;
    }

    /**
     * An agent responsible for rendering a translated work from a source work
     * 
     * @return
     *     The translator
     */
    @JsonProperty("translator")
    public java.lang.Object getTranslator() {
        return translator;
    }

    /**
     * An agent responsible for rendering a translated work from a source work
     * 
     * @param translator
     *     The translator
     */
    @JsonProperty("translator")
    public void setTranslator(java.lang.Object translator) {
        this.translator = translator;
    }

    public Video withTranslator(java.lang.Object translator) {
        this.translator = translator;
        return this;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @return
     *     The about
     */
    @JsonProperty("about")
    public io.dataconnect.model.Object getAbout() {
        return about;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @param about
     *     The about
     */
    @JsonProperty("about")
    public void setAbout(io.dataconnect.model.Object about) {
        this.about = about;
    }

    public Video withAbout(io.dataconnect.model.Object about) {
        this.about = about;
        return this;
    }

    /**
     * A license document that applies to this content, typically indicated by URL.
     * 
     * @return
     *     The license
     */
    @JsonProperty("license")
    public java.lang.Object getLicense() {
        return license;
    }

    /**
     * A license document that applies to this content, typically indicated by URL.
     * 
     * @param license
     *     The license
     */
    @JsonProperty("license")
    public void setLicense(java.lang.Object license) {
        this.license = license;
    }

    public Video withLicense(java.lang.Object license) {
        this.license = license;
        return this;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @return
     *     The workExample
     */
    @JsonProperty("workExample")
    public ExampleOfWork getWorkExample() {
        return workExample;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @param workExample
     *     The workExample
     */
    @JsonProperty("workExample")
    public void setWorkExample(ExampleOfWork workExample) {
        this.workExample = workExample;
    }

    public Video withWorkExample(ExampleOfWork workExample) {
        this.workExample = workExample;
        return this;
    }

    /**
     * mp3, mpeg4, etc.
     * 
     * @return
     *     The encodingFormat
     */
    @JsonProperty("encodingFormat")
    public String getEncodingFormat() {
        return encodingFormat;
    }

    /**
     * mp3, mpeg4, etc.
     * 
     * @param encodingFormat
     *     The encodingFormat
     */
    @JsonProperty("encodingFormat")
    public void setEncodingFormat(String encodingFormat) {
        this.encodingFormat = encodingFormat;
    }

    public Video withEncodingFormat(String encodingFormat) {
        this.encodingFormat = encodingFormat;
        return this;
    }

    /**
     * The caption for this object.
     * 
     * @return
     *     The caption
     */
    @JsonProperty("caption")
    public String getCaption() {
        return caption;
    }

    /**
     * The caption for this object.
     * 
     * @param caption
     *     The caption
     */
    @JsonProperty("caption")
    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Video withCaption(String caption) {
        this.caption = caption;
        return this;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @return
     *     The mentions
     */
    @JsonProperty("mentions")
    public io.dataconnect.model.Object getMentions() {
        return mentions;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @param mentions
     *     The mentions
     */
    @JsonProperty("mentions")
    public void setMentions(io.dataconnect.model.Object mentions) {
        this.mentions = mentions;
    }

    public Video withMentions(io.dataconnect.model.Object mentions) {
        this.mentions = mentions;
        return this;
    }

    /**
     * Comment
     * <p>
     * A comment on an item - for example, a comment on a blog post. The comment's content is expressed via the "text" property, and its topic via "about", properties shared with all CreativeWorks.
     * 
     * @return
     *     The comment
     */
    @JsonProperty("comment")
    public Comment getComment() {
        return comment;
    }

    /**
     * Comment
     * <p>
     * A comment on an item - for example, a comment on a blog post. The comment's content is expressed via the "text" property, and its topic via "about", properties shared with all CreativeWorks.
     * 
     * @param comment
     *     The comment
     */
    @JsonProperty("comment")
    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public Video withComment(Comment comment) {
        this.comment = comment;
        return this;
    }

    /**
     * A resource that was used in the creation of this resource. This term can be repeated for multiple sources. For example, http://example.com/great-multiplication-intro.html.
     * 
     * @return
     *     The isBasedOnUrl
     */
    @JsonProperty("isBasedOnUrl")
    public URI getIsBasedOnUrl() {
        return isBasedOnUrl;
    }

    /**
     * A resource that was used in the creation of this resource. This term can be repeated for multiple sources. For example, http://example.com/great-multiplication-intro.html.
     * 
     * @param isBasedOnUrl
     *     The isBasedOnUrl
     */
    @JsonProperty("isBasedOnUrl")
    public void setIsBasedOnUrl(URI isBasedOnUrl) {
        this.isBasedOnUrl = isBasedOnUrl;
    }

    public Video withIsBasedOnUrl(URI isBasedOnUrl) {
        this.isBasedOnUrl = isBasedOnUrl;
        return this;
    }

    /**
     * MediaObject
     * <p>
     * An image, video, or audio object embedded in a web page. Note that a creative work may have many media objects associated with it on the same web page. For example, a page about a single song (MusicRecording) may have a music video (VideoObject), and a high and low bandwidth audio stream (2 AudioObject's).
     * 
     * @return
     *     The encoding
     */
    @JsonProperty("encoding")
    public AssociatedMedia getEncoding() {
        return encoding;
    }

    /**
     * MediaObject
     * <p>
     * An image, video, or audio object embedded in a web page. Note that a creative work may have many media objects associated with it on the same web page. For example, a page about a single song (MusicRecording) may have a music video (VideoObject), and a high and low bandwidth audio stream (2 AudioObject's).
     * 
     * @param encoding
     *     The encoding
     */
    @JsonProperty("encoding")
    public void setEncoding(AssociatedMedia encoding) {
        this.encoding = encoding;
    }

    public Video withEncoding(AssociatedMedia encoding) {
        this.encoding = encoding;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public Video withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public Video withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * The height of the item.
     * 
     * @return
     *     The height
     */
    @JsonProperty("height")
    public java.lang.Object getHeight() {
        return height;
    }

    /**
     * The height of the item.
     * 
     * @param height
     *     The height
     */
    @JsonProperty("height")
    public void setHeight(java.lang.Object height) {
        this.height = height;
    }

    public Video withHeight(java.lang.Object height) {
        this.height = height;
        return this;
    }

    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     * @return
     *     The aggregateRating
     */
    @JsonProperty("aggregateRating")
    public AggregateRating getAggregateRating() {
        return aggregateRating;
    }

    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     * @param aggregateRating
     *     The aggregateRating
     */
    @JsonProperty("aggregateRating")
    public void setAggregateRating(AggregateRating aggregateRating) {
        this.aggregateRating = aggregateRating;
    }

    public Video withAggregateRating(AggregateRating aggregateRating) {
        this.aggregateRating = aggregateRating;
        return this;
    }

    /**
     * A secondary contributor to the CreativeWork.
     * 
     * @return
     *     The contributor
     */
    @JsonProperty("contributor")
    public java.lang.Object getContributor() {
        return contributor;
    }

    /**
     * A secondary contributor to the CreativeWork.
     * 
     * @param contributor
     *     The contributor
     */
    @JsonProperty("contributor")
    public void setContributor(java.lang.Object contributor) {
        this.contributor = contributor;
    }

    public Video withContributor(java.lang.Object contributor) {
        this.contributor = contributor;
        return this;
    }

    /**
     * A thumbnail image relevant to the Thing.
     * 
     * @return
     *     The thumbnailUrl
     */
    @JsonProperty("thumbnailUrl")
    public URI getThumbnailUrl() {
        return thumbnailUrl;
    }

    /**
     * A thumbnail image relevant to the Thing.
     * 
     * @param thumbnailUrl
     *     The thumbnailUrl
     */
    @JsonProperty("thumbnailUrl")
    public void setThumbnailUrl(URI thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public Video withThumbnailUrl(URI thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
        return this;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @return
     *     The mainEntity
     */
    @JsonProperty("mainEntity")
    public io.dataconnect.model.Object getMainEntity() {
        return mainEntity;
    }

    /**
     * Thing
     * <p>
     * The most generic type of item.
     * 
     * @param mainEntity
     *     The mainEntity
     */
    @JsonProperty("mainEntity")
    public void setMainEntity(io.dataconnect.model.Object mainEntity) {
        this.mainEntity = mainEntity;
    }

    public Video withMainEntity(io.dataconnect.model.Object mainEntity) {
        this.mainEntity = mainEntity;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public Video withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * Content features of the resource, such as accessible media, alternatives and supported enhancements for accessibility (<a href="http://www.w3.org/wiki/WebSchemas/Accessibility">WebSchemas wiki lists possible values</a>).
     * 
     * @return
     *     The accessibilityFeature
     */
    @JsonProperty("accessibilityFeature")
    public String getAccessibilityFeature() {
        return accessibilityFeature;
    }

    /**
     * Content features of the resource, such as accessible media, alternatives and supported enhancements for accessibility (<a href="http://www.w3.org/wiki/WebSchemas/Accessibility">WebSchemas wiki lists possible values</a>).
     * 
     * @param accessibilityFeature
     *     The accessibilityFeature
     */
    @JsonProperty("accessibilityFeature")
    public void setAccessibilityFeature(String accessibilityFeature) {
        this.accessibilityFeature = accessibilityFeature;
    }

    public Video withAccessibilityFeature(String accessibilityFeature) {
        this.accessibilityFeature = accessibilityFeature;
        return this;
    }

    /**
     * The predominant mode of learning supported by the learning resource. Acceptable values are 'active', 'expositive', or 'mixed'.
     * 
     * @return
     *     The interactivityType
     */
    @JsonProperty("interactivityType")
    public String getInteractivityType() {
        return interactivityType;
    }

    /**
     * The predominant mode of learning supported by the learning resource. Acceptable values are 'active', 'expositive', or 'mixed'.
     * 
     * @param interactivityType
     *     The interactivityType
     */
    @JsonProperty("interactivityType")
    public void setInteractivityType(String interactivityType) {
        this.interactivityType = interactivityType;
    }

    public Video withInteractivityType(String interactivityType) {
        this.interactivityType = interactivityType;
        return this;
    }

    /**
     * PublicationEvent
     * <p>
     * A PublicationEvent corresponds indifferently to the event of publication for a CreativeWork of any type e.g. a broadcast event, an on-demand event, a book/journal publication via a variety of delivery media.
     * 
     * @return
     *     The publication
     */
    @JsonProperty("publication")
    public Publication getPublication() {
        return publication;
    }

    /**
     * PublicationEvent
     * <p>
     * A PublicationEvent corresponds indifferently to the event of publication for a CreativeWork of any type e.g. a broadcast event, an on-demand event, a book/journal publication via a variety of delivery media.
     * 
     * @param publication
     *     The publication
     */
    @JsonProperty("publication")
    public void setPublication(Publication publication) {
        this.publication = publication;
    }

    public Video withPublication(Publication publication) {
        this.publication = publication;
        return this;
    }

    /**
     * The width of the item.
     * 
     * @return
     *     The width
     */
    @JsonProperty("width")
    public java.lang.Object getWidth() {
        return width;
    }

    /**
     * The width of the item.
     * 
     * @param width
     *     The width
     */
    @JsonProperty("width")
    public void setWidth(java.lang.Object width) {
        this.width = width;
    }

    public Video withWidth(java.lang.Object width) {
        this.width = width;
        return this;
    }

    /**
     * Indicates if use of the media require a subscription  (either paid or free). Allowed values are <code>true</code> or <code>false</code> (note that an earlier version had 'yes', 'no').
     * 
     * @return
     *     The requiresSubscription
     */
    @JsonProperty("requiresSubscription")
    public Boolean getRequiresSubscription() {
        return requiresSubscription;
    }

    /**
     * Indicates if use of the media require a subscription  (either paid or free). Allowed values are <code>true</code> or <code>false</code> (note that an earlier version had 'yes', 'no').
     * 
     * @param requiresSubscription
     *     The requiresSubscription
     */
    @JsonProperty("requiresSubscription")
    public void setRequiresSubscription(Boolean requiresSubscription) {
        this.requiresSubscription = requiresSubscription;
    }

    public Video withRequiresSubscription(Boolean requiresSubscription) {
        this.requiresSubscription = requiresSubscription;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The editor
     */
    @JsonProperty("editor")
    public AccountablePerson getEditor() {
        return editor;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param editor
     *     The editor
     */
    @JsonProperty("editor")
    public void setEditor(AccountablePerson editor) {
        this.editor = editor;
    }

    public Video withEditor(AccountablePerson editor) {
        this.editor = editor;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public Video withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @return
     *     The recordedAt
     */
    @JsonProperty("recordedAt")
    public Event getRecordedAt() {
        return recordedAt;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @param recordedAt
     *     The recordedAt
     */
    @JsonProperty("recordedAt")
    public void setRecordedAt(Event recordedAt) {
        this.recordedAt = recordedAt;
    }

    public Video withRecordedAt(Event recordedAt) {
        this.recordedAt = recordedAt;
        return this;
    }

    /**
     * A citation or reference to another creative work, such as another publication, web page, scholarly article, etc.
     * 
     * @return
     *     The citation
     */
    @JsonProperty("citation")
    public java.lang.Object getCitation() {
        return citation;
    }

    /**
     * A citation or reference to another creative work, such as another publication, web page, scholarly article, etc.
     * 
     * @param citation
     *     The citation
     */
    @JsonProperty("citation")
    public void setCitation(java.lang.Object citation) {
        this.citation = citation;
    }

    public Video withCitation(java.lang.Object citation) {
        this.citation = citation;
        return this;
    }

    /**
     * The publisher of the creative work.
     * 
     * @return
     *     The publisher
     */
    @JsonProperty("publisher")
    public java.lang.Object getPublisher() {
        return publisher;
    }

    /**
     * The publisher of the creative work.
     * 
     * @param publisher
     *     The publisher
     */
    @JsonProperty("publisher")
    public void setPublisher(java.lang.Object publisher) {
        this.publisher = publisher;
    }

    public Video withPublisher(java.lang.Object publisher) {
        this.publisher = publisher;
        return this;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @return
     *     The hasPart
     */
    @JsonProperty("hasPart")
    public ExampleOfWork getHasPart() {
        return hasPart;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @param hasPart
     *     The hasPart
     */
    @JsonProperty("hasPart")
    public void setHasPart(ExampleOfWork hasPart) {
        this.hasPart = hasPart;
    }

    public Video withHasPart(ExampleOfWork hasPart) {
        this.hasPart = hasPart;
        return this;
    }

    /**
     * An award won by or for this item.
     * 
     * @return
     *     The award
     */
    @JsonProperty("award")
    public String getAward() {
        return award;
    }

    /**
     * An award won by or for this item.
     * 
     * @param award
     *     The award
     */
    @JsonProperty("award")
    public void setAward(String award) {
        this.award = award;
    }

    public Video withAward(String award) {
        this.award = award;
        return this;
    }

    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     * @return
     *     The offers
     */
    @JsonProperty("offers")
    public Offer getOffers() {
        return offers;
    }

    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     * @param offers
     *     The offers
     */
    @JsonProperty("offers")
    public void setOffers(Offer offers) {
        this.offers = offers;
    }

    public Video withOffers(Offer offers) {
        this.offers = offers;
        return this;
    }

    /**
     * The party holding the legal copyright to the CreativeWork.
     * 
     * @return
     *     The copyrightHolder
     */
    @JsonProperty("copyrightHolder")
    public java.lang.Object getCopyrightHolder() {
        return copyrightHolder;
    }

    /**
     * The party holding the legal copyright to the CreativeWork.
     * 
     * @param copyrightHolder
     *     The copyrightHolder
     */
    @JsonProperty("copyrightHolder")
    public void setCopyrightHolder(java.lang.Object copyrightHolder) {
        this.copyrightHolder = copyrightHolder;
    }

    public Video withCopyrightHolder(java.lang.Object copyrightHolder) {
        this.copyrightHolder = copyrightHolder;
        return this;
    }

    /**
     * Indicates that the resource is compatible with the referenced accessibility API (<a href="http://www.w3.org/wiki/WebSchemas/Accessibility">WebSchemas wiki lists possible values</a>).
     * 
     * @return
     *     The accessibilityAPI
     */
    @JsonProperty("accessibilityAPI")
    public String getAccessibilityAPI() {
        return accessibilityAPI;
    }

    /**
     * Indicates that the resource is compatible with the referenced accessibility API (<a href="http://www.w3.org/wiki/WebSchemas/Accessibility">WebSchemas wiki lists possible values</a>).
     * 
     * @param accessibilityAPI
     *     The accessibilityAPI
     */
    @JsonProperty("accessibilityAPI")
    public void setAccessibilityAPI(String accessibilityAPI) {
        this.accessibilityAPI = accessibilityAPI;
    }

    public Video withAccessibilityAPI(String accessibilityAPI) {
        this.accessibilityAPI = accessibilityAPI;
        return this;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @return
     *     The reviews
     */
    @JsonProperty("reviews")
    public Review getReviews() {
        return reviews;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @param reviews
     *     The reviews
     */
    @JsonProperty("reviews")
    public void setReviews(Review reviews) {
        this.reviews = reviews;
    }

    public Video withReviews(Review reviews) {
        this.reviews = reviews;
        return this;
    }

    /**
     * The predominant type or kind characterizing the learning resource. For example, 'presentation', 'handout'.
     * 
     * @return
     *     The learningResourceType
     */
    @JsonProperty("learningResourceType")
    public String getLearningResourceType() {
        return learningResourceType;
    }

    /**
     * The predominant type or kind characterizing the learning resource. For example, 'presentation', 'handout'.
     * 
     * @param learningResourceType
     *     The learningResourceType
     */
    @JsonProperty("learningResourceType")
    public void setLearningResourceType(String learningResourceType) {
        this.learningResourceType = learningResourceType;
    }

    public Video withLearningResourceType(String learningResourceType) {
        this.learningResourceType = learningResourceType;
        return this;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @return
     *     The sourceOrganization
     */
    @JsonProperty("sourceOrganization")
    public Affiliation getSourceOrganization() {
        return sourceOrganization;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @param sourceOrganization
     *     The sourceOrganization
     */
    @JsonProperty("sourceOrganization")
    public void setSourceOrganization(Affiliation sourceOrganization) {
        this.sourceOrganization = sourceOrganization;
    }

    public Video withSourceOrganization(Affiliation sourceOrganization) {
        this.sourceOrganization = sourceOrganization;
        return this;
    }

    /**
     * The frame size of the video.
     * 
     * @return
     *     The videoFrameSize
     */
    @JsonProperty("videoFrameSize")
    public String getVideoFrameSize() {
        return videoFrameSize;
    }

    /**
     * The frame size of the video.
     * 
     * @param videoFrameSize
     *     The videoFrameSize
     */
    @JsonProperty("videoFrameSize")
    public void setVideoFrameSize(String videoFrameSize) {
        this.videoFrameSize = videoFrameSize;
    }

    public Video withVideoFrameSize(String videoFrameSize) {
        this.videoFrameSize = videoFrameSize;
        return this;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @return
     *     The regionsAllowed
     */
    @JsonProperty("regionsAllowed")
    public ContainsPlace getRegionsAllowed() {
        return regionsAllowed;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @param regionsAllowed
     *     The regionsAllowed
     */
    @JsonProperty("regionsAllowed")
    public void setRegionsAllowed(ContainsPlace regionsAllowed) {
        this.regionsAllowed = regionsAllowed;
    }

    public Video withRegionsAllowed(ContainsPlace regionsAllowed) {
        this.regionsAllowed = regionsAllowed;
        return this;
    }

    /**
     * The language of the content or performance or used in an action. Please use one of the language codes from the <a href='http://tools.ietf.org/html/bcp47'>IETF BCP 47 standard</a>.
     * 
     * @return
     *     The inLanguage
     */
    @JsonProperty("inLanguage")
    public java.lang.Object getInLanguage() {
        return inLanguage;
    }

    /**
     * The language of the content or performance or used in an action. Please use one of the language codes from the <a href='http://tools.ietf.org/html/bcp47'>IETF BCP 47 standard</a>.
     * 
     * @param inLanguage
     *     The inLanguage
     */
    @JsonProperty("inLanguage")
    public void setInLanguage(java.lang.Object inLanguage) {
        this.inLanguage = inLanguage;
    }

    public Video withInLanguage(java.lang.Object inLanguage) {
        this.inLanguage = inLanguage;
        return this;
    }

    /**
     * The typical expected age range, e.g. '7-9', '11-'.
     * 
     * @return
     *     The typicalAgeRange
     */
    @JsonProperty("typicalAgeRange")
    public String getTypicalAgeRange() {
        return typicalAgeRange;
    }

    /**
     * The typical expected age range, e.g. '7-9', '11-'.
     * 
     * @param typicalAgeRange
     *     The typicalAgeRange
     */
    @JsonProperty("typicalAgeRange")
    public void setTypicalAgeRange(String typicalAgeRange) {
        this.typicalAgeRange = typicalAgeRange;
    }

    public Video withTypicalAgeRange(String typicalAgeRange) {
        this.typicalAgeRange = typicalAgeRange;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public Video withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * The author of this content. Please note that author is special in that HTML 5 provides a special mechanism for indicating authorship via the rel tag. That is equivalent to this and may be used interchangeably.
     * 
     * @return
     *     The author
     */
    @JsonProperty("author")
    public java.lang.Object getAuthor() {
        return author;
    }

    /**
     * The author of this content. Please note that author is special in that HTML 5 provides a special mechanism for indicating authorship via the rel tag. That is equivalent to this and may be used interchangeably.
     * 
     * @param author
     *     The author
     */
    @JsonProperty("author")
    public void setAuthor(java.lang.Object author) {
        this.author = author;
    }

    public Video withAuthor(java.lang.Object author) {
        this.author = author;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The actor
     */
    @JsonProperty("actor")
    public AccountablePerson getActor() {
        return actor;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param actor
     *     The actor
     */
    @JsonProperty("actor")
    public void setActor(AccountablePerson actor) {
        this.actor = actor;
    }

    public Video withActor(AccountablePerson actor) {
        this.actor = actor;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The actors
     */
    @JsonProperty("actors")
    public AccountablePerson getActors() {
        return actors;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param actors
     *     The actors
     */
    @JsonProperty("actors")
    public void setActors(AccountablePerson actors) {
        this.actors = actors;
    }

    public Video withActors(AccountablePerson actors) {
        this.actors = actors;
        return this;
    }

    /**
     * The date on which the CreativeWork was most recently modified or when the item's entry was modified within a DataFeed.
     * 
     * @return
     *     The dateModified
     */
    @JsonProperty("dateModified")
    public java.lang.Object getDateModified() {
        return dateModified;
    }

    /**
     * The date on which the CreativeWork was most recently modified or when the item's entry was modified within a DataFeed.
     * 
     * @param dateModified
     *     The dateModified
     */
    @JsonProperty("dateModified")
    public void setDateModified(java.lang.Object dateModified) {
        this.dateModified = dateModified;
    }

    public Video withDateModified(java.lang.Object dateModified) {
        this.dateModified = dateModified;
        return this;
    }

    /**
     * InteractionCounter
     * <p>
     * A summary of how users have interacted with this CreativeWork. In most cases, authors will use a subtype to specify the specific type of interaction.
     * 
     * @return
     *     The interactionStatistic
     */
    @JsonProperty("interactionStatistic")
    public InteractionStatistic getInteractionStatistic() {
        return interactionStatistic;
    }

    /**
     * InteractionCounter
     * <p>
     * A summary of how users have interacted with this CreativeWork. In most cases, authors will use a subtype to specify the specific type of interaction.
     * 
     * @param interactionStatistic
     *     The interactionStatistic
     */
    @JsonProperty("interactionStatistic")
    public void setInteractionStatistic(InteractionStatistic interactionStatistic) {
        this.interactionStatistic = interactionStatistic;
    }

    public Video withInteractionStatistic(InteractionStatistic interactionStatistic) {
        this.interactionStatistic = interactionStatistic;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public Video withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * Date the content expires and is no longer useful or available. Useful for videos.
     * 
     * @return
     *     The expires
     */
    @JsonProperty("expires")
    public Date getExpires() {
        return expires;
    }

    /**
     * Date the content expires and is no longer useful or available. Useful for videos.
     * 
     * @param expires
     *     The expires
     */
    @JsonProperty("expires")
    public void setExpires(Date expires) {
        this.expires = expires;
    }

    public Video withExpires(Date expires) {
        this.expires = expires;
        return this;
    }

    /**
     * PublicationEvent
     * <p>
     * A PublicationEvent corresponds indifferently to the event of publication for a CreativeWork of any type e.g. a broadcast event, an on-demand event, a book/journal publication via a variety of delivery media.
     * 
     * @return
     *     The releasedEvent
     */
    @JsonProperty("releasedEvent")
    public Publication getReleasedEvent() {
        return releasedEvent;
    }

    /**
     * PublicationEvent
     * <p>
     * A PublicationEvent corresponds indifferently to the event of publication for a CreativeWork of any type e.g. a broadcast event, an on-demand event, a book/journal publication via a variety of delivery media.
     * 
     * @param releasedEvent
     *     The releasedEvent
     */
    @JsonProperty("releasedEvent")
    public void setReleasedEvent(Publication releasedEvent) {
        this.releasedEvent = releasedEvent;
    }

    public Video withReleasedEvent(Publication releasedEvent) {
        this.releasedEvent = releasedEvent;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The director
     */
    @JsonProperty("director")
    public AccountablePerson getDirector() {
        return director;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param director
     *     The director
     */
    @JsonProperty("director")
    public void setDirector(AccountablePerson director) {
        this.director = director;
    }

    public Video withDirector(AccountablePerson director) {
        this.director = director;
        return this;
    }

    /**
     * Date when this media object was uploaded to this site.
     * 
     * @return
     *     The uploadDate
     */
    @JsonProperty("uploadDate")
    public Date getUploadDate() {
        return uploadDate;
    }

    /**
     * Date when this media object was uploaded to this site.
     * 
     * @param uploadDate
     *     The uploadDate
     */
    @JsonProperty("uploadDate")
    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public Video withUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
        return this;
    }

    /**
     * Indicates (by URL or string) a particular version of a schema used in some CreativeWork. For example, a document could declare a schemaVersion using a URL such as http://schema.org/version/2.0/ if precise indication of schema version was required by some application. 
     * 
     * @return
     *     The schemaVersion
     */
    @JsonProperty("schemaVersion")
    public java.lang.Object getSchemaVersion() {
        return schemaVersion;
    }

    /**
     * Indicates (by URL or string) a particular version of a schema used in some CreativeWork. For example, a document could declare a schemaVersion using a URL such as http://schema.org/version/2.0/ if precise indication of schema version was required by some application. 
     * 
     * @param schemaVersion
     *     The schemaVersion
     */
    @JsonProperty("schemaVersion")
    public void setSchemaVersion(java.lang.Object schemaVersion) {
        this.schemaVersion = schemaVersion;
    }

    public Video withSchemaVersion(java.lang.Object schemaVersion) {
        this.schemaVersion = schemaVersion;
        return this;
    }

    /**
     * If this MediaObject is an AudioObject or VideoObject, the transcript of that object.
     * 
     * @return
     *     The transcript
     */
    @JsonProperty("transcript")
    public String getTranscript() {
        return transcript;
    }

    /**
     * If this MediaObject is an AudioObject or VideoObject, the transcript of that object.
     * 
     * @param transcript
     *     The transcript
     */
    @JsonProperty("transcript")
    public void setTranscript(String transcript) {
        this.transcript = transcript;
    }

    public Video withTranscript(String transcript) {
        this.transcript = transcript;
        return this;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @return
     *     The contentLocation
     */
    @JsonProperty("contentLocation")
    public ContainsPlace getContentLocation() {
        return contentLocation;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @param contentLocation
     *     The contentLocation
     */
    @JsonProperty("contentLocation")
    public void setContentLocation(ContainsPlace contentLocation) {
        this.contentLocation = contentLocation;
    }

    public Video withContentLocation(ContainsPlace contentLocation) {
        this.contentLocation = contentLocation;
        return this;
    }

    /**
     * Duration
     * <p>
     * Quantity: Duration (use  <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 duration format</a>).
     * 
     * @return
     *     The timeRequired
     */
    @JsonProperty("timeRequired")
    public Duration getTimeRequired() {
        return timeRequired;
    }

    /**
     * Duration
     * <p>
     * Quantity: Duration (use  <a href='http://en.wikipedia.org/wiki/ISO_8601'>ISO 8601 duration format</a>).
     * 
     * @param timeRequired
     *     The timeRequired
     */
    @JsonProperty("timeRequired")
    public void setTimeRequired(Duration timeRequired) {
        this.timeRequired = timeRequired;
    }

    public Video withTimeRequired(Duration timeRequired) {
        this.timeRequired = timeRequired;
        return this;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @return
     *     The workTranslation
     */
    @JsonProperty("workTranslation")
    public ExampleOfWork getWorkTranslation() {
        return workTranslation;
    }

    /**
     * CreativeWork
     * <p>
     * The most generic kind of creative work, including books, movies, photographs, software programs, etc.
     * 
     * @param workTranslation
     *     The workTranslation
     */
    @JsonProperty("workTranslation")
    public void setWorkTranslation(ExampleOfWork workTranslation) {
        this.workTranslation = workTranslation;
    }

    public Video withWorkTranslation(ExampleOfWork workTranslation) {
        this.workTranslation = workTranslation;
        return this;
    }

    /**
     * Player type required&#x2014;for example, Flash or Silverlight.
     * 
     * @return
     *     The playerType
     */
    @JsonProperty("playerType")
    public String getPlayerType() {
        return playerType;
    }

    /**
     * Player type required&#x2014;for example, Flash or Silverlight.
     * 
     * @param playerType
     *     The playerType
     */
    @JsonProperty("playerType")
    public void setPlayerType(String playerType) {
        this.playerType = playerType;
    }

    public Video withPlayerType(String playerType) {
        this.playerType = playerType;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public Video withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The directors
     */
    @JsonProperty("directors")
    public AccountablePerson getDirectors() {
        return directors;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param directors
     *     The directors
     */
    @JsonProperty("directors")
    public void setDirectors(AccountablePerson directors) {
        this.directors = directors;
    }

    public Video withDirectors(AccountablePerson directors) {
        this.directors = directors;
        return this;
    }

    /**
     * The position of an item in a series or sequence of items.
     * 
     * @return
     *     The position
     */
    @JsonProperty("position")
    public java.lang.Object getPosition() {
        return position;
    }

    /**
     * The position of an item in a series or sequence of items.
     * 
     * @param position
     *     The position
     */
    @JsonProperty("position")
    public void setPosition(java.lang.Object position) {
        this.position = position;
    }

    public Video withPosition(java.lang.Object position) {
        this.position = position;
        return this;
    }

    /**
     * AudioObject
     * <p>
     * An audio file.
     * 
     * @return
     *     The audio
     */
    @JsonProperty("audio")
    public Audio getAudio() {
        return audio;
    }

    /**
     * AudioObject
     * <p>
     * An audio file.
     * 
     * @param audio
     *     The audio
     */
    @JsonProperty("audio")
    public void setAudio(Audio audio) {
        this.audio = audio;
    }

    public Video withAudio(Audio audio) {
        this.audio = audio;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(educationalUse).append(producer).append(text).append(datePublished).append(alternativeHeadline).append(accountablePerson).append(keywords).append(headline).append(character).append(contentRating).append(associatedArticle).append(exampleOfWork).append(publishingPrinciples).append(embedUrl).append(dateCreated).append(publisherImprint).append(potentialAction).append(name).append(associatedMedia).append(audience).append(accessibilityControl).append(translationOfWork).append(copyrightYear).append(videoQuality).append(encodesCreativeWork).append(creator).append(musicBy).append(commentCount).append(video).append(encodings).append(duration).append(fileFormat).append(discussionUrl).append(productionCompany).append(review).append(isFamilyFriendly).append(version).append(locationCreated).append(provider).append(isPartOf).append(accessibilityHazard).append(thumbnail).append(contentSize).append(contentUrl).append(educationalAlignment).append(awards).append(genre).append(bitrate).append(translator).append(about).append(license).append(workExample).append(encodingFormat).append(caption).append(mentions).append(comment).append(isBasedOnUrl).append(encoding).append(sameAs).append(image).append(height).append(aggregateRating).append(contributor).append(thumbnailUrl).append(mainEntity).append(alternateName).append(accessibilityFeature).append(interactivityType).append(publication).append(width).append(requiresSubscription).append(editor).append(mainEntityOfPage).append(recordedAt).append(citation).append(publisher).append(hasPart).append(award).append(offers).append(copyrightHolder).append(accessibilityAPI).append(reviews).append(learningResourceType).append(sourceOrganization).append(videoFrameSize).append(regionsAllowed).append(inLanguage).append(typicalAgeRange).append(additionalType).append(author).append(actor).append(actors).append(dateModified).append(interactionStatistic).append(description).append(expires).append(releasedEvent).append(director).append(uploadDate).append(schemaVersion).append(transcript).append(contentLocation).append(timeRequired).append(workTranslation).append(playerType).append(url).append(directors).append(position).append(audio).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Video) == false) {
            return false;
        }
        Video rhs = ((Video) other);
        return new EqualsBuilder().append(educationalUse, rhs.educationalUse).append(producer, rhs.producer).append(text, rhs.text).append(datePublished, rhs.datePublished).append(alternativeHeadline, rhs.alternativeHeadline).append(accountablePerson, rhs.accountablePerson).append(keywords, rhs.keywords).append(headline, rhs.headline).append(character, rhs.character).append(contentRating, rhs.contentRating).append(associatedArticle, rhs.associatedArticle).append(exampleOfWork, rhs.exampleOfWork).append(publishingPrinciples, rhs.publishingPrinciples).append(embedUrl, rhs.embedUrl).append(dateCreated, rhs.dateCreated).append(publisherImprint, rhs.publisherImprint).append(potentialAction, rhs.potentialAction).append(name, rhs.name).append(associatedMedia, rhs.associatedMedia).append(audience, rhs.audience).append(accessibilityControl, rhs.accessibilityControl).append(translationOfWork, rhs.translationOfWork).append(copyrightYear, rhs.copyrightYear).append(videoQuality, rhs.videoQuality).append(encodesCreativeWork, rhs.encodesCreativeWork).append(creator, rhs.creator).append(musicBy, rhs.musicBy).append(commentCount, rhs.commentCount).append(video, rhs.video).append(encodings, rhs.encodings).append(duration, rhs.duration).append(fileFormat, rhs.fileFormat).append(discussionUrl, rhs.discussionUrl).append(productionCompany, rhs.productionCompany).append(review, rhs.review).append(isFamilyFriendly, rhs.isFamilyFriendly).append(version, rhs.version).append(locationCreated, rhs.locationCreated).append(provider, rhs.provider).append(isPartOf, rhs.isPartOf).append(accessibilityHazard, rhs.accessibilityHazard).append(thumbnail, rhs.thumbnail).append(contentSize, rhs.contentSize).append(contentUrl, rhs.contentUrl).append(educationalAlignment, rhs.educationalAlignment).append(awards, rhs.awards).append(genre, rhs.genre).append(bitrate, rhs.bitrate).append(translator, rhs.translator).append(about, rhs.about).append(license, rhs.license).append(workExample, rhs.workExample).append(encodingFormat, rhs.encodingFormat).append(caption, rhs.caption).append(mentions, rhs.mentions).append(comment, rhs.comment).append(isBasedOnUrl, rhs.isBasedOnUrl).append(encoding, rhs.encoding).append(sameAs, rhs.sameAs).append(image, rhs.image).append(height, rhs.height).append(aggregateRating, rhs.aggregateRating).append(contributor, rhs.contributor).append(thumbnailUrl, rhs.thumbnailUrl).append(mainEntity, rhs.mainEntity).append(alternateName, rhs.alternateName).append(accessibilityFeature, rhs.accessibilityFeature).append(interactivityType, rhs.interactivityType).append(publication, rhs.publication).append(width, rhs.width).append(requiresSubscription, rhs.requiresSubscription).append(editor, rhs.editor).append(mainEntityOfPage, rhs.mainEntityOfPage).append(recordedAt, rhs.recordedAt).append(citation, rhs.citation).append(publisher, rhs.publisher).append(hasPart, rhs.hasPart).append(award, rhs.award).append(offers, rhs.offers).append(copyrightHolder, rhs.copyrightHolder).append(accessibilityAPI, rhs.accessibilityAPI).append(reviews, rhs.reviews).append(learningResourceType, rhs.learningResourceType).append(sourceOrganization, rhs.sourceOrganization).append(videoFrameSize, rhs.videoFrameSize).append(regionsAllowed, rhs.regionsAllowed).append(inLanguage, rhs.inLanguage).append(typicalAgeRange, rhs.typicalAgeRange).append(additionalType, rhs.additionalType).append(author, rhs.author).append(actor, rhs.actor).append(actors, rhs.actors).append(dateModified, rhs.dateModified).append(interactionStatistic, rhs.interactionStatistic).append(description, rhs.description).append(expires, rhs.expires).append(releasedEvent, rhs.releasedEvent).append(director, rhs.director).append(uploadDate, rhs.uploadDate).append(schemaVersion, rhs.schemaVersion).append(transcript, rhs.transcript).append(contentLocation, rhs.contentLocation).append(timeRequired, rhs.timeRequired).append(workTranslation, rhs.workTranslation).append(playerType, rhs.playerType).append(url, rhs.url).append(directors, rhs.directors).append(position, rhs.position).append(audio, rhs.audio).isEquals();
    }

}
