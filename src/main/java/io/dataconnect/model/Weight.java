
package io.dataconnect.model;

import java.net.URI;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * QuantitativeValue
 * <p>
 *  A point value or interval for product characteristics and other purposes.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "potentialAction",
    "valueReference",
    "description",
    "sameAs",
    "image",
    "maxValue",
    "value",
    "minValue",
    "additionalType",
    "url",
    "additionalProperty",
    "unitText",
    "mainEntityOfPage",
    "unitCode",
    "alternateName",
    "name"
})
public class Weight {

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * A pointer to a secondary value that provides additional information on the original value, e.g. a reference temperature.
     * 
     */
    @JsonProperty("valueReference")
    @JsonPropertyDescription("")
    private java.lang.Object valueReference;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * The upper value of some characteristic or property.
     * 
     */
    @JsonProperty("maxValue")
    @JsonPropertyDescription("")
    private Double maxValue;
    /**
     * The value of the quantitative value or property value node. For QuantitativeValue, the recommended type for values is 'Number'. For PropertyValue, it can be 'Text;', 'Number', 'Boolean', or 'StructuredValue'.
     * 
     */
    @JsonProperty("value")
    @JsonPropertyDescription("")
    private java.lang.Object value;
    /**
     * The lower value of some characteristic or property.
     * 
     */
    @JsonProperty("minValue")
    @JsonPropertyDescription("")
    private Double minValue;
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * PropertyValue
     * <p>
     * A property-value pair, e.g. representing a feature of a product or place. Use the 'name' property for the name of the property. If there is an additional human-readable version of the value, put that into the 'description' property.
     *         <br/><br/>
     *         Always use specific schema.org properties when a) they exist and b) you can populate them. Using PropertyValue as a substitute will typically not trigger the same effect as using the original, specific property.
     *     
     * 
     */
    @JsonProperty("additionalProperty")
    @JsonPropertyDescription("")
    private AdditionalProperty additionalProperty;
    /**
     * A string or text indicating the unit of measurement. Useful if you cannot provide a standard unit code for
     * <a href='unitCode'>unitCode</a>.
     * 
     */
    @JsonProperty("unitText")
    @JsonPropertyDescription("")
    private String unitText;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * The unit of measurement given using the UN/CEFACT Common Code (3 characters) or a URL. Other codes than the UN/CEFACT Common Code may be used with a prefix followed by a colon.
     * 
     */
    @JsonProperty("unitCode")
    @JsonPropertyDescription("")
    private java.lang.Object unitCode;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public Weight withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * A pointer to a secondary value that provides additional information on the original value, e.g. a reference temperature.
     * 
     * @return
     *     The valueReference
     */
    @JsonProperty("valueReference")
    public java.lang.Object getValueReference() {
        return valueReference;
    }

    /**
     * A pointer to a secondary value that provides additional information on the original value, e.g. a reference temperature.
     * 
     * @param valueReference
     *     The valueReference
     */
    @JsonProperty("valueReference")
    public void setValueReference(java.lang.Object valueReference) {
        this.valueReference = valueReference;
    }

    public Weight withValueReference(java.lang.Object valueReference) {
        this.valueReference = valueReference;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public Weight withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public Weight withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public Weight withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * The upper value of some characteristic or property.
     * 
     * @return
     *     The maxValue
     */
    @JsonProperty("maxValue")
    public Double getMaxValue() {
        return maxValue;
    }

    /**
     * The upper value of some characteristic or property.
     * 
     * @param maxValue
     *     The maxValue
     */
    @JsonProperty("maxValue")
    public void setMaxValue(Double maxValue) {
        this.maxValue = maxValue;
    }

    public Weight withMaxValue(Double maxValue) {
        this.maxValue = maxValue;
        return this;
    }

    /**
     * The value of the quantitative value or property value node. For QuantitativeValue, the recommended type for values is 'Number'. For PropertyValue, it can be 'Text;', 'Number', 'Boolean', or 'StructuredValue'.
     * 
     * @return
     *     The value
     */
    @JsonProperty("value")
    public java.lang.Object getValue() {
        return value;
    }

    /**
     * The value of the quantitative value or property value node. For QuantitativeValue, the recommended type for values is 'Number'. For PropertyValue, it can be 'Text;', 'Number', 'Boolean', or 'StructuredValue'.
     * 
     * @param value
     *     The value
     */
    @JsonProperty("value")
    public void setValue(java.lang.Object value) {
        this.value = value;
    }

    public Weight withValue(java.lang.Object value) {
        this.value = value;
        return this;
    }

    /**
     * The lower value of some characteristic or property.
     * 
     * @return
     *     The minValue
     */
    @JsonProperty("minValue")
    public Double getMinValue() {
        return minValue;
    }

    /**
     * The lower value of some characteristic or property.
     * 
     * @param minValue
     *     The minValue
     */
    @JsonProperty("minValue")
    public void setMinValue(Double minValue) {
        this.minValue = minValue;
    }

    public Weight withMinValue(Double minValue) {
        this.minValue = minValue;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public Weight withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public Weight withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * PropertyValue
     * <p>
     * A property-value pair, e.g. representing a feature of a product or place. Use the 'name' property for the name of the property. If there is an additional human-readable version of the value, put that into the 'description' property.
     *         <br/><br/>
     *         Always use specific schema.org properties when a) they exist and b) you can populate them. Using PropertyValue as a substitute will typically not trigger the same effect as using the original, specific property.
     *     
     * 
     * @return
     *     The additionalProperty
     */
    @JsonProperty("additionalProperty")
    public AdditionalProperty getAdditionalProperty() {
        return additionalProperty;
    }

    /**
     * PropertyValue
     * <p>
     * A property-value pair, e.g. representing a feature of a product or place. Use the 'name' property for the name of the property. If there is an additional human-readable version of the value, put that into the 'description' property.
     *         <br/><br/>
     *         Always use specific schema.org properties when a) they exist and b) you can populate them. Using PropertyValue as a substitute will typically not trigger the same effect as using the original, specific property.
     *     
     * 
     * @param additionalProperty
     *     The additionalProperty
     */
    @JsonProperty("additionalProperty")
    public void setAdditionalProperty(AdditionalProperty additionalProperty) {
        this.additionalProperty = additionalProperty;
    }

    public Weight withAdditionalProperty(AdditionalProperty additionalProperty) {
        this.additionalProperty = additionalProperty;
        return this;
    }

    /**
     * A string or text indicating the unit of measurement. Useful if you cannot provide a standard unit code for
     * <a href='unitCode'>unitCode</a>.
     * 
     * @return
     *     The unitText
     */
    @JsonProperty("unitText")
    public String getUnitText() {
        return unitText;
    }

    /**
     * A string or text indicating the unit of measurement. Useful if you cannot provide a standard unit code for
     * <a href='unitCode'>unitCode</a>.
     * 
     * @param unitText
     *     The unitText
     */
    @JsonProperty("unitText")
    public void setUnitText(String unitText) {
        this.unitText = unitText;
    }

    public Weight withUnitText(String unitText) {
        this.unitText = unitText;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public Weight withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * The unit of measurement given using the UN/CEFACT Common Code (3 characters) or a URL. Other codes than the UN/CEFACT Common Code may be used with a prefix followed by a colon.
     * 
     * @return
     *     The unitCode
     */
    @JsonProperty("unitCode")
    public java.lang.Object getUnitCode() {
        return unitCode;
    }

    /**
     * The unit of measurement given using the UN/CEFACT Common Code (3 characters) or a URL. Other codes than the UN/CEFACT Common Code may be used with a prefix followed by a colon.
     * 
     * @param unitCode
     *     The unitCode
     */
    @JsonProperty("unitCode")
    public void setUnitCode(java.lang.Object unitCode) {
        this.unitCode = unitCode;
    }

    public Weight withUnitCode(java.lang.Object unitCode) {
        this.unitCode = unitCode;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public Weight withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Weight withName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(potentialAction).append(valueReference).append(description).append(sameAs).append(image).append(maxValue).append(value).append(minValue).append(additionalType).append(url).append(additionalProperty).append(unitText).append(mainEntityOfPage).append(unitCode).append(alternateName).append(name).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Weight) == false) {
            return false;
        }
        Weight rhs = ((Weight) other);
        return new EqualsBuilder().append(potentialAction, rhs.potentialAction).append(valueReference, rhs.valueReference).append(description, rhs.description).append(sameAs, rhs.sameAs).append(image, rhs.image).append(maxValue, rhs.maxValue).append(value, rhs.value).append(minValue, rhs.minValue).append(additionalType, rhs.additionalType).append(url, rhs.url).append(additionalProperty, rhs.additionalProperty).append(unitText, rhs.unitText).append(mainEntityOfPage, rhs.mainEntityOfPage).append(unitCode, rhs.unitCode).append(alternateName, rhs.alternateName).append(name, rhs.name).isEquals();
    }

}
