
package io.dataconnect.model;

import java.net.URI;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Demand
 * <p>
 * A demand entity represents the public, not necessarily binding, not necessarily exclusive, announcement by an organization or person to seek a certain type of goods or services. For describing demand using this type, the very same properties used for Offer apply.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "warranty",
    "ineligibleRegion",
    "sameAs",
    "eligibleRegion",
    "image",
    "gtin8",
    "additionalType",
    "availability",
    "inventoryLevel",
    "includesObject",
    "availableDeliveryMethod",
    "seller",
    "availabilityStarts",
    "eligibleDuration",
    "mainEntityOfPage",
    "businessFunction",
    "advanceBookingRequirement",
    "sku",
    "availableAtOrFrom",
    "description",
    "mpn",
    "eligibleCustomerType",
    "priceSpecification",
    "acceptedPaymentMethod",
    "eligibleTransactionVolume",
    "deliveryLeadTime",
    "eligibleQuantity",
    "gtin13",
    "availabilityEnds",
    "validThrough",
    "alternateName",
    "potentialAction",
    "name",
    "url",
    "serialNumber",
    "gtin14",
    "itemOffered",
    "areaServed",
    "gtin12",
    "validFrom",
    "itemCondition"
})
public class Seeks {

    /**
     * WarrantyPromise
     * <p>
     * A structured value representing the duration and scope of services that will be provided to a customer free of charge in case of a defect or malfunction of a product.
     * 
     */
    @JsonProperty("warranty")
    @JsonPropertyDescription("")
    private Warranty warranty;
    /**
     * The ISO 3166-1 (ISO 3166-1 alpha-2) or ISO 3166-2 code, the place, or the GeoShape for the geo-political region(s) for which the offer or delivery charge specification is not valid, e.g. a region where the transaction is not allowed.
     *       <br><br> See also <a href="/eligibleRegion">eligibleRegion</a>.
     *       
     * 
     */
    @JsonProperty("ineligibleRegion")
    @JsonPropertyDescription("")
    private java.lang.Object ineligibleRegion;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * The ISO 3166-1 (ISO 3166-1 alpha-2) or ISO 3166-2 code, the place, or the GeoShape for the geo-political region(s) for which the offer or delivery charge specification is valid.
     *       <br><br> See also <a href="/ineligibleRegion">ineligibleRegion</a>.
     *     
     * 
     */
    @JsonProperty("eligibleRegion")
    @JsonPropertyDescription("")
    private java.lang.Object eligibleRegion;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-8.aspx">GTIN-8</a> code of the product, or the product to which the offer refers. This code is also known as EAN/UCC-8 or 8-digit EAN. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     */
    @JsonProperty("gtin8")
    @JsonPropertyDescription("")
    private String gtin8;
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * ItemAvailability
     * <p>
     * A list of possible product availability options.
     * 
     */
    @JsonProperty("availability")
    @JsonPropertyDescription("")
    private Availability availability;
    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     */
    @JsonProperty("inventoryLevel")
    @JsonPropertyDescription("")
    private Weight inventoryLevel;
    /**
     * TypeAndQuantityNode
     * <p>
     * A structured value indicating the quantity, unit of measurement, and business function of goods included in a bundle offer.
     * 
     */
    @JsonProperty("includesObject")
    @JsonPropertyDescription("")
    private IncludesObject includesObject;
    /**
     * DeliveryMethod
     * <p>
     * A delivery method is a standardized procedure for transferring the product or service to the destination of fulfillment chosen by the customer. Delivery methods are characterized by the means of transportation used, and by the organization or group that is the contracting party for the sending organization or person.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#DeliveryModeDirectDownload <br />
     *     http://purl.org/goodrelations/v1#DeliveryModeFreight <br />
     *     http://purl.org/goodrelations/v1#DeliveryModeMail <br />
     *     http://purl.org/goodrelations/v1#DeliveryModeOwnFleet <br />
     *     http://purl.org/goodrelations/v1#DeliveryModePickUp <br />
     *     http://purl.org/goodrelations/v1#DHL <br />
     *     http://purl.org/goodrelations/v1#FederalExpress <br />
     *     http://purl.org/goodrelations/v1#UPS <br />
     *         
     * 
     */
    @JsonProperty("availableDeliveryMethod")
    @JsonPropertyDescription("")
    private AvailableDeliveryMethod availableDeliveryMethod;
    /**
     * An entity which offers (sells / leases / lends / loans) the services / goods.  A seller may also be a provider.
     * 
     */
    @JsonProperty("seller")
    @JsonPropertyDescription("")
    private java.lang.Object seller;
    /**
     * The beginning of the availability of the product or service included in the offer.
     * 
     */
    @JsonProperty("availabilityStarts")
    @JsonPropertyDescription("")
    private Date availabilityStarts;
    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     */
    @JsonProperty("eligibleDuration")
    @JsonPropertyDescription("")
    private Weight eligibleDuration;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * BusinessFunction
     * <p>
     * The business function specifies the type of activity or access (i.e., the bundle of rights) offered by the organization or business person through the offer. Typical are sell, rental or lease, maintenance or repair, manufacture / produce, recycle / dispose, engineering / construction, or installation. Proprietary specifications of access rights are also instances of this class.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#ConstructionInstallation <br />
     *     http://purl.org/goodrelations/v1#Dispose <br />
     *     http://purl.org/goodrelations/v1#LeaseOut <br />
     *     http://purl.org/goodrelations/v1#Maintain <br />
     *     http://purl.org/goodrelations/v1#ProvideService <br />
     *     http://purl.org/goodrelations/v1#Repair <br />
     *     http://purl.org/goodrelations/v1#Sell <br />
     *     http://purl.org/goodrelations/v1#Buy <br />
     *         
     * 
     */
    @JsonProperty("businessFunction")
    @JsonPropertyDescription("")
    private BusinessFunction businessFunction;
    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     */
    @JsonProperty("advanceBookingRequirement")
    @JsonPropertyDescription("")
    private Weight advanceBookingRequirement;
    /**
     * The Stock Keeping Unit (SKU), i.e. a merchant-specific identifier for a product or service, or the product to which the offer refers.
     * 
     */
    @JsonProperty("sku")
    @JsonPropertyDescription("")
    private String sku;
    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     */
    @JsonProperty("availableAtOrFrom")
    @JsonPropertyDescription("")
    private ContainsPlace availableAtOrFrom;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;
    /**
     * The Manufacturer Part Number (MPN) of the product, or the product to which the offer refers.
     * 
     */
    @JsonProperty("mpn")
    @JsonPropertyDescription("")
    private String mpn;
    /**
     * BusinessEntityType
     * <p>
     * A business entity type is a conceptual entity representing the legal form, the size, the main line of business, the position in the value chain, or any combination thereof, of an organization or business person.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#Business <br />
     *     http://purl.org/goodrelations/v1#Enduser <br />
     *     http://purl.org/goodrelations/v1#PublicInstitution <br />
     *     http://purl.org/goodrelations/v1#Reseller <br />
     * 
     *         
     * 
     */
    @JsonProperty("eligibleCustomerType")
    @JsonPropertyDescription("")
    private EligibleCustomerType eligibleCustomerType;
    /**
     * PriceSpecification
     * <p>
     * A structured value representing a monetary amount. Typically, only the subclasses of this type are used for markup.
     * 
     */
    @JsonProperty("priceSpecification")
    @JsonPropertyDescription("")
    private PriceSpecification priceSpecification;
    /**
     * PaymentMethod
     * <p>
     * A payment method is a standardized procedure for transferring the monetary amount for a purchase. Payment methods are characterized by the legal and technical structures used, and by the organization or group carrying out the transaction.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#ByBankTransferInAdvance <br />
     *     http://purl.org/goodrelations/v1#ByInvoice <br />
     *     http://purl.org/goodrelations/v1#Cash <br />
     *     http://purl.org/goodrelations/v1#CheckInAdvance <br />
     *     http://purl.org/goodrelations/v1#COD <br />
     *     http://purl.org/goodrelations/v1#DirectDebit <br />
     *     http://purl.org/goodrelations/v1#GoogleCheckout <br />
     *     http://purl.org/goodrelations/v1#PayPal <br />
     *     http://purl.org/goodrelations/v1#PaySwarm <br />
     *         
     * 
     */
    @JsonProperty("acceptedPaymentMethod")
    @JsonPropertyDescription("")
    private AcceptedPaymentMethod acceptedPaymentMethod;
    /**
     * PriceSpecification
     * <p>
     * A structured value representing a monetary amount. Typically, only the subclasses of this type are used for markup.
     * 
     */
    @JsonProperty("eligibleTransactionVolume")
    @JsonPropertyDescription("")
    private PriceSpecification eligibleTransactionVolume;
    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     */
    @JsonProperty("deliveryLeadTime")
    @JsonPropertyDescription("")
    private Weight deliveryLeadTime;
    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     */
    @JsonProperty("eligibleQuantity")
    @JsonPropertyDescription("")
    private Weight eligibleQuantity;
    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-13.aspx">GTIN-13</a> code of the product, or the product to which the offer refers. This is equivalent to 13-digit ISBN codes and EAN UCC-13. Former 12-digit UPC codes can be converted into a GTIN-13 code by simply adding a preceeding zero. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     */
    @JsonProperty("gtin13")
    @JsonPropertyDescription("")
    private String gtin13;
    /**
     * The end of the availability of the product or service included in the offer.
     * 
     */
    @JsonProperty("availabilityEnds")
    @JsonPropertyDescription("")
    private Date availabilityEnds;
    /**
     * The end of the validity of offer, price specification, or opening hours data.
     * 
     */
    @JsonProperty("validThrough")
    @JsonPropertyDescription("")
    private Date validThrough;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * The serial number or any alphanumeric identifier of a particular product. When attached to an offer, it is a shortcut for the serial number of the product included in the offer.
     * 
     */
    @JsonProperty("serialNumber")
    @JsonPropertyDescription("")
    private String serialNumber;
    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-14.aspx">GTIN-14</a> code of the product, or the product to which the offer refers. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     */
    @JsonProperty("gtin14")
    @JsonPropertyDescription("")
    private String gtin14;
    /**
     * The item being offered.
     * 
     */
    @JsonProperty("itemOffered")
    @JsonPropertyDescription("")
    private java.lang.Object itemOffered;
    /**
     * The geographic area where a service or offered item is provided.
     * 
     */
    @JsonProperty("areaServed")
    @JsonPropertyDescription("")
    private java.lang.Object areaServed;
    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-12.aspx">GTIN-12</a> code of the product, or the product to which the offer refers. The GTIN-12 is the 12-digit GS1 Identification Key composed of a U.P.C. Company Prefix, Item Reference, and Check Digit used to identify trade items. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     */
    @JsonProperty("gtin12")
    @JsonPropertyDescription("")
    private String gtin12;
    /**
     * The date when the item becomes valid.
     * 
     */
    @JsonProperty("validFrom")
    @JsonPropertyDescription("")
    private Date validFrom;
    /**
     * OfferItemCondition
     * <p>
     * A list of possible conditions for the item.
     * 
     */
    @JsonProperty("itemCondition")
    @JsonPropertyDescription("")
    private ItemCondition itemCondition;

    /**
     * WarrantyPromise
     * <p>
     * A structured value representing the duration and scope of services that will be provided to a customer free of charge in case of a defect or malfunction of a product.
     * 
     * @return
     *     The warranty
     */
    @JsonProperty("warranty")
    public Warranty getWarranty() {
        return warranty;
    }

    /**
     * WarrantyPromise
     * <p>
     * A structured value representing the duration and scope of services that will be provided to a customer free of charge in case of a defect or malfunction of a product.
     * 
     * @param warranty
     *     The warranty
     */
    @JsonProperty("warranty")
    public void setWarranty(Warranty warranty) {
        this.warranty = warranty;
    }

    public Seeks withWarranty(Warranty warranty) {
        this.warranty = warranty;
        return this;
    }

    /**
     * The ISO 3166-1 (ISO 3166-1 alpha-2) or ISO 3166-2 code, the place, or the GeoShape for the geo-political region(s) for which the offer or delivery charge specification is not valid, e.g. a region where the transaction is not allowed.
     *       <br><br> See also <a href="/eligibleRegion">eligibleRegion</a>.
     *       
     * 
     * @return
     *     The ineligibleRegion
     */
    @JsonProperty("ineligibleRegion")
    public java.lang.Object getIneligibleRegion() {
        return ineligibleRegion;
    }

    /**
     * The ISO 3166-1 (ISO 3166-1 alpha-2) or ISO 3166-2 code, the place, or the GeoShape for the geo-political region(s) for which the offer or delivery charge specification is not valid, e.g. a region where the transaction is not allowed.
     *       <br><br> See also <a href="/eligibleRegion">eligibleRegion</a>.
     *       
     * 
     * @param ineligibleRegion
     *     The ineligibleRegion
     */
    @JsonProperty("ineligibleRegion")
    public void setIneligibleRegion(java.lang.Object ineligibleRegion) {
        this.ineligibleRegion = ineligibleRegion;
    }

    public Seeks withIneligibleRegion(java.lang.Object ineligibleRegion) {
        this.ineligibleRegion = ineligibleRegion;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public Seeks withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * The ISO 3166-1 (ISO 3166-1 alpha-2) or ISO 3166-2 code, the place, or the GeoShape for the geo-political region(s) for which the offer or delivery charge specification is valid.
     *       <br><br> See also <a href="/ineligibleRegion">ineligibleRegion</a>.
     *     
     * 
     * @return
     *     The eligibleRegion
     */
    @JsonProperty("eligibleRegion")
    public java.lang.Object getEligibleRegion() {
        return eligibleRegion;
    }

    /**
     * The ISO 3166-1 (ISO 3166-1 alpha-2) or ISO 3166-2 code, the place, or the GeoShape for the geo-political region(s) for which the offer or delivery charge specification is valid.
     *       <br><br> See also <a href="/ineligibleRegion">ineligibleRegion</a>.
     *     
     * 
     * @param eligibleRegion
     *     The eligibleRegion
     */
    @JsonProperty("eligibleRegion")
    public void setEligibleRegion(java.lang.Object eligibleRegion) {
        this.eligibleRegion = eligibleRegion;
    }

    public Seeks withEligibleRegion(java.lang.Object eligibleRegion) {
        this.eligibleRegion = eligibleRegion;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public Seeks withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-8.aspx">GTIN-8</a> code of the product, or the product to which the offer refers. This code is also known as EAN/UCC-8 or 8-digit EAN. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     * @return
     *     The gtin8
     */
    @JsonProperty("gtin8")
    public String getGtin8() {
        return gtin8;
    }

    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-8.aspx">GTIN-8</a> code of the product, or the product to which the offer refers. This code is also known as EAN/UCC-8 or 8-digit EAN. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     * @param gtin8
     *     The gtin8
     */
    @JsonProperty("gtin8")
    public void setGtin8(String gtin8) {
        this.gtin8 = gtin8;
    }

    public Seeks withGtin8(String gtin8) {
        this.gtin8 = gtin8;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public Seeks withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * ItemAvailability
     * <p>
     * A list of possible product availability options.
     * 
     * @return
     *     The availability
     */
    @JsonProperty("availability")
    public Availability getAvailability() {
        return availability;
    }

    /**
     * ItemAvailability
     * <p>
     * A list of possible product availability options.
     * 
     * @param availability
     *     The availability
     */
    @JsonProperty("availability")
    public void setAvailability(Availability availability) {
        this.availability = availability;
    }

    public Seeks withAvailability(Availability availability) {
        this.availability = availability;
        return this;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @return
     *     The inventoryLevel
     */
    @JsonProperty("inventoryLevel")
    public Weight getInventoryLevel() {
        return inventoryLevel;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @param inventoryLevel
     *     The inventoryLevel
     */
    @JsonProperty("inventoryLevel")
    public void setInventoryLevel(Weight inventoryLevel) {
        this.inventoryLevel = inventoryLevel;
    }

    public Seeks withInventoryLevel(Weight inventoryLevel) {
        this.inventoryLevel = inventoryLevel;
        return this;
    }

    /**
     * TypeAndQuantityNode
     * <p>
     * A structured value indicating the quantity, unit of measurement, and business function of goods included in a bundle offer.
     * 
     * @return
     *     The includesObject
     */
    @JsonProperty("includesObject")
    public IncludesObject getIncludesObject() {
        return includesObject;
    }

    /**
     * TypeAndQuantityNode
     * <p>
     * A structured value indicating the quantity, unit of measurement, and business function of goods included in a bundle offer.
     * 
     * @param includesObject
     *     The includesObject
     */
    @JsonProperty("includesObject")
    public void setIncludesObject(IncludesObject includesObject) {
        this.includesObject = includesObject;
    }

    public Seeks withIncludesObject(IncludesObject includesObject) {
        this.includesObject = includesObject;
        return this;
    }

    /**
     * DeliveryMethod
     * <p>
     * A delivery method is a standardized procedure for transferring the product or service to the destination of fulfillment chosen by the customer. Delivery methods are characterized by the means of transportation used, and by the organization or group that is the contracting party for the sending organization or person.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#DeliveryModeDirectDownload <br />
     *     http://purl.org/goodrelations/v1#DeliveryModeFreight <br />
     *     http://purl.org/goodrelations/v1#DeliveryModeMail <br />
     *     http://purl.org/goodrelations/v1#DeliveryModeOwnFleet <br />
     *     http://purl.org/goodrelations/v1#DeliveryModePickUp <br />
     *     http://purl.org/goodrelations/v1#DHL <br />
     *     http://purl.org/goodrelations/v1#FederalExpress <br />
     *     http://purl.org/goodrelations/v1#UPS <br />
     *         
     * 
     * @return
     *     The availableDeliveryMethod
     */
    @JsonProperty("availableDeliveryMethod")
    public AvailableDeliveryMethod getAvailableDeliveryMethod() {
        return availableDeliveryMethod;
    }

    /**
     * DeliveryMethod
     * <p>
     * A delivery method is a standardized procedure for transferring the product or service to the destination of fulfillment chosen by the customer. Delivery methods are characterized by the means of transportation used, and by the organization or group that is the contracting party for the sending organization or person.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#DeliveryModeDirectDownload <br />
     *     http://purl.org/goodrelations/v1#DeliveryModeFreight <br />
     *     http://purl.org/goodrelations/v1#DeliveryModeMail <br />
     *     http://purl.org/goodrelations/v1#DeliveryModeOwnFleet <br />
     *     http://purl.org/goodrelations/v1#DeliveryModePickUp <br />
     *     http://purl.org/goodrelations/v1#DHL <br />
     *     http://purl.org/goodrelations/v1#FederalExpress <br />
     *     http://purl.org/goodrelations/v1#UPS <br />
     *         
     * 
     * @param availableDeliveryMethod
     *     The availableDeliveryMethod
     */
    @JsonProperty("availableDeliveryMethod")
    public void setAvailableDeliveryMethod(AvailableDeliveryMethod availableDeliveryMethod) {
        this.availableDeliveryMethod = availableDeliveryMethod;
    }

    public Seeks withAvailableDeliveryMethod(AvailableDeliveryMethod availableDeliveryMethod) {
        this.availableDeliveryMethod = availableDeliveryMethod;
        return this;
    }

    /**
     * An entity which offers (sells / leases / lends / loans) the services / goods.  A seller may also be a provider.
     * 
     * @return
     *     The seller
     */
    @JsonProperty("seller")
    public java.lang.Object getSeller() {
        return seller;
    }

    /**
     * An entity which offers (sells / leases / lends / loans) the services / goods.  A seller may also be a provider.
     * 
     * @param seller
     *     The seller
     */
    @JsonProperty("seller")
    public void setSeller(java.lang.Object seller) {
        this.seller = seller;
    }

    public Seeks withSeller(java.lang.Object seller) {
        this.seller = seller;
        return this;
    }

    /**
     * The beginning of the availability of the product or service included in the offer.
     * 
     * @return
     *     The availabilityStarts
     */
    @JsonProperty("availabilityStarts")
    public Date getAvailabilityStarts() {
        return availabilityStarts;
    }

    /**
     * The beginning of the availability of the product or service included in the offer.
     * 
     * @param availabilityStarts
     *     The availabilityStarts
     */
    @JsonProperty("availabilityStarts")
    public void setAvailabilityStarts(Date availabilityStarts) {
        this.availabilityStarts = availabilityStarts;
    }

    public Seeks withAvailabilityStarts(Date availabilityStarts) {
        this.availabilityStarts = availabilityStarts;
        return this;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @return
     *     The eligibleDuration
     */
    @JsonProperty("eligibleDuration")
    public Weight getEligibleDuration() {
        return eligibleDuration;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @param eligibleDuration
     *     The eligibleDuration
     */
    @JsonProperty("eligibleDuration")
    public void setEligibleDuration(Weight eligibleDuration) {
        this.eligibleDuration = eligibleDuration;
    }

    public Seeks withEligibleDuration(Weight eligibleDuration) {
        this.eligibleDuration = eligibleDuration;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public Seeks withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * BusinessFunction
     * <p>
     * The business function specifies the type of activity or access (i.e., the bundle of rights) offered by the organization or business person through the offer. Typical are sell, rental or lease, maintenance or repair, manufacture / produce, recycle / dispose, engineering / construction, or installation. Proprietary specifications of access rights are also instances of this class.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#ConstructionInstallation <br />
     *     http://purl.org/goodrelations/v1#Dispose <br />
     *     http://purl.org/goodrelations/v1#LeaseOut <br />
     *     http://purl.org/goodrelations/v1#Maintain <br />
     *     http://purl.org/goodrelations/v1#ProvideService <br />
     *     http://purl.org/goodrelations/v1#Repair <br />
     *     http://purl.org/goodrelations/v1#Sell <br />
     *     http://purl.org/goodrelations/v1#Buy <br />
     *         
     * 
     * @return
     *     The businessFunction
     */
    @JsonProperty("businessFunction")
    public BusinessFunction getBusinessFunction() {
        return businessFunction;
    }

    /**
     * BusinessFunction
     * <p>
     * The business function specifies the type of activity or access (i.e., the bundle of rights) offered by the organization or business person through the offer. Typical are sell, rental or lease, maintenance or repair, manufacture / produce, recycle / dispose, engineering / construction, or installation. Proprietary specifications of access rights are also instances of this class.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#ConstructionInstallation <br />
     *     http://purl.org/goodrelations/v1#Dispose <br />
     *     http://purl.org/goodrelations/v1#LeaseOut <br />
     *     http://purl.org/goodrelations/v1#Maintain <br />
     *     http://purl.org/goodrelations/v1#ProvideService <br />
     *     http://purl.org/goodrelations/v1#Repair <br />
     *     http://purl.org/goodrelations/v1#Sell <br />
     *     http://purl.org/goodrelations/v1#Buy <br />
     *         
     * 
     * @param businessFunction
     *     The businessFunction
     */
    @JsonProperty("businessFunction")
    public void setBusinessFunction(BusinessFunction businessFunction) {
        this.businessFunction = businessFunction;
    }

    public Seeks withBusinessFunction(BusinessFunction businessFunction) {
        this.businessFunction = businessFunction;
        return this;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @return
     *     The advanceBookingRequirement
     */
    @JsonProperty("advanceBookingRequirement")
    public Weight getAdvanceBookingRequirement() {
        return advanceBookingRequirement;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @param advanceBookingRequirement
     *     The advanceBookingRequirement
     */
    @JsonProperty("advanceBookingRequirement")
    public void setAdvanceBookingRequirement(Weight advanceBookingRequirement) {
        this.advanceBookingRequirement = advanceBookingRequirement;
    }

    public Seeks withAdvanceBookingRequirement(Weight advanceBookingRequirement) {
        this.advanceBookingRequirement = advanceBookingRequirement;
        return this;
    }

    /**
     * The Stock Keeping Unit (SKU), i.e. a merchant-specific identifier for a product or service, or the product to which the offer refers.
     * 
     * @return
     *     The sku
     */
    @JsonProperty("sku")
    public String getSku() {
        return sku;
    }

    /**
     * The Stock Keeping Unit (SKU), i.e. a merchant-specific identifier for a product or service, or the product to which the offer refers.
     * 
     * @param sku
     *     The sku
     */
    @JsonProperty("sku")
    public void setSku(String sku) {
        this.sku = sku;
    }

    public Seeks withSku(String sku) {
        this.sku = sku;
        return this;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @return
     *     The availableAtOrFrom
     */
    @JsonProperty("availableAtOrFrom")
    public ContainsPlace getAvailableAtOrFrom() {
        return availableAtOrFrom;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @param availableAtOrFrom
     *     The availableAtOrFrom
     */
    @JsonProperty("availableAtOrFrom")
    public void setAvailableAtOrFrom(ContainsPlace availableAtOrFrom) {
        this.availableAtOrFrom = availableAtOrFrom;
    }

    public Seeks withAvailableAtOrFrom(ContainsPlace availableAtOrFrom) {
        this.availableAtOrFrom = availableAtOrFrom;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public Seeks withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * The Manufacturer Part Number (MPN) of the product, or the product to which the offer refers.
     * 
     * @return
     *     The mpn
     */
    @JsonProperty("mpn")
    public String getMpn() {
        return mpn;
    }

    /**
     * The Manufacturer Part Number (MPN) of the product, or the product to which the offer refers.
     * 
     * @param mpn
     *     The mpn
     */
    @JsonProperty("mpn")
    public void setMpn(String mpn) {
        this.mpn = mpn;
    }

    public Seeks withMpn(String mpn) {
        this.mpn = mpn;
        return this;
    }

    /**
     * BusinessEntityType
     * <p>
     * A business entity type is a conceptual entity representing the legal form, the size, the main line of business, the position in the value chain, or any combination thereof, of an organization or business person.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#Business <br />
     *     http://purl.org/goodrelations/v1#Enduser <br />
     *     http://purl.org/goodrelations/v1#PublicInstitution <br />
     *     http://purl.org/goodrelations/v1#Reseller <br />
     * 
     *         
     * 
     * @return
     *     The eligibleCustomerType
     */
    @JsonProperty("eligibleCustomerType")
    public EligibleCustomerType getEligibleCustomerType() {
        return eligibleCustomerType;
    }

    /**
     * BusinessEntityType
     * <p>
     * A business entity type is a conceptual entity representing the legal form, the size, the main line of business, the position in the value chain, or any combination thereof, of an organization or business person.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#Business <br />
     *     http://purl.org/goodrelations/v1#Enduser <br />
     *     http://purl.org/goodrelations/v1#PublicInstitution <br />
     *     http://purl.org/goodrelations/v1#Reseller <br />
     * 
     *         
     * 
     * @param eligibleCustomerType
     *     The eligibleCustomerType
     */
    @JsonProperty("eligibleCustomerType")
    public void setEligibleCustomerType(EligibleCustomerType eligibleCustomerType) {
        this.eligibleCustomerType = eligibleCustomerType;
    }

    public Seeks withEligibleCustomerType(EligibleCustomerType eligibleCustomerType) {
        this.eligibleCustomerType = eligibleCustomerType;
        return this;
    }

    /**
     * PriceSpecification
     * <p>
     * A structured value representing a monetary amount. Typically, only the subclasses of this type are used for markup.
     * 
     * @return
     *     The priceSpecification
     */
    @JsonProperty("priceSpecification")
    public PriceSpecification getPriceSpecification() {
        return priceSpecification;
    }

    /**
     * PriceSpecification
     * <p>
     * A structured value representing a monetary amount. Typically, only the subclasses of this type are used for markup.
     * 
     * @param priceSpecification
     *     The priceSpecification
     */
    @JsonProperty("priceSpecification")
    public void setPriceSpecification(PriceSpecification priceSpecification) {
        this.priceSpecification = priceSpecification;
    }

    public Seeks withPriceSpecification(PriceSpecification priceSpecification) {
        this.priceSpecification = priceSpecification;
        return this;
    }

    /**
     * PaymentMethod
     * <p>
     * A payment method is a standardized procedure for transferring the monetary amount for a purchase. Payment methods are characterized by the legal and technical structures used, and by the organization or group carrying out the transaction.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#ByBankTransferInAdvance <br />
     *     http://purl.org/goodrelations/v1#ByInvoice <br />
     *     http://purl.org/goodrelations/v1#Cash <br />
     *     http://purl.org/goodrelations/v1#CheckInAdvance <br />
     *     http://purl.org/goodrelations/v1#COD <br />
     *     http://purl.org/goodrelations/v1#DirectDebit <br />
     *     http://purl.org/goodrelations/v1#GoogleCheckout <br />
     *     http://purl.org/goodrelations/v1#PayPal <br />
     *     http://purl.org/goodrelations/v1#PaySwarm <br />
     *         
     * 
     * @return
     *     The acceptedPaymentMethod
     */
    @JsonProperty("acceptedPaymentMethod")
    public AcceptedPaymentMethod getAcceptedPaymentMethod() {
        return acceptedPaymentMethod;
    }

    /**
     * PaymentMethod
     * <p>
     * A payment method is a standardized procedure for transferring the monetary amount for a purchase. Payment methods are characterized by the legal and technical structures used, and by the organization or group carrying out the transaction.
     * <br />
     *     Commonly used values:<br />
     * <br />
     *     http://purl.org/goodrelations/v1#ByBankTransferInAdvance <br />
     *     http://purl.org/goodrelations/v1#ByInvoice <br />
     *     http://purl.org/goodrelations/v1#Cash <br />
     *     http://purl.org/goodrelations/v1#CheckInAdvance <br />
     *     http://purl.org/goodrelations/v1#COD <br />
     *     http://purl.org/goodrelations/v1#DirectDebit <br />
     *     http://purl.org/goodrelations/v1#GoogleCheckout <br />
     *     http://purl.org/goodrelations/v1#PayPal <br />
     *     http://purl.org/goodrelations/v1#PaySwarm <br />
     *         
     * 
     * @param acceptedPaymentMethod
     *     The acceptedPaymentMethod
     */
    @JsonProperty("acceptedPaymentMethod")
    public void setAcceptedPaymentMethod(AcceptedPaymentMethod acceptedPaymentMethod) {
        this.acceptedPaymentMethod = acceptedPaymentMethod;
    }

    public Seeks withAcceptedPaymentMethod(AcceptedPaymentMethod acceptedPaymentMethod) {
        this.acceptedPaymentMethod = acceptedPaymentMethod;
        return this;
    }

    /**
     * PriceSpecification
     * <p>
     * A structured value representing a monetary amount. Typically, only the subclasses of this type are used for markup.
     * 
     * @return
     *     The eligibleTransactionVolume
     */
    @JsonProperty("eligibleTransactionVolume")
    public PriceSpecification getEligibleTransactionVolume() {
        return eligibleTransactionVolume;
    }

    /**
     * PriceSpecification
     * <p>
     * A structured value representing a monetary amount. Typically, only the subclasses of this type are used for markup.
     * 
     * @param eligibleTransactionVolume
     *     The eligibleTransactionVolume
     */
    @JsonProperty("eligibleTransactionVolume")
    public void setEligibleTransactionVolume(PriceSpecification eligibleTransactionVolume) {
        this.eligibleTransactionVolume = eligibleTransactionVolume;
    }

    public Seeks withEligibleTransactionVolume(PriceSpecification eligibleTransactionVolume) {
        this.eligibleTransactionVolume = eligibleTransactionVolume;
        return this;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @return
     *     The deliveryLeadTime
     */
    @JsonProperty("deliveryLeadTime")
    public Weight getDeliveryLeadTime() {
        return deliveryLeadTime;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @param deliveryLeadTime
     *     The deliveryLeadTime
     */
    @JsonProperty("deliveryLeadTime")
    public void setDeliveryLeadTime(Weight deliveryLeadTime) {
        this.deliveryLeadTime = deliveryLeadTime;
    }

    public Seeks withDeliveryLeadTime(Weight deliveryLeadTime) {
        this.deliveryLeadTime = deliveryLeadTime;
        return this;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @return
     *     The eligibleQuantity
     */
    @JsonProperty("eligibleQuantity")
    public Weight getEligibleQuantity() {
        return eligibleQuantity;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @param eligibleQuantity
     *     The eligibleQuantity
     */
    @JsonProperty("eligibleQuantity")
    public void setEligibleQuantity(Weight eligibleQuantity) {
        this.eligibleQuantity = eligibleQuantity;
    }

    public Seeks withEligibleQuantity(Weight eligibleQuantity) {
        this.eligibleQuantity = eligibleQuantity;
        return this;
    }

    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-13.aspx">GTIN-13</a> code of the product, or the product to which the offer refers. This is equivalent to 13-digit ISBN codes and EAN UCC-13. Former 12-digit UPC codes can be converted into a GTIN-13 code by simply adding a preceeding zero. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     * @return
     *     The gtin13
     */
    @JsonProperty("gtin13")
    public String getGtin13() {
        return gtin13;
    }

    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-13.aspx">GTIN-13</a> code of the product, or the product to which the offer refers. This is equivalent to 13-digit ISBN codes and EAN UCC-13. Former 12-digit UPC codes can be converted into a GTIN-13 code by simply adding a preceeding zero. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     * @param gtin13
     *     The gtin13
     */
    @JsonProperty("gtin13")
    public void setGtin13(String gtin13) {
        this.gtin13 = gtin13;
    }

    public Seeks withGtin13(String gtin13) {
        this.gtin13 = gtin13;
        return this;
    }

    /**
     * The end of the availability of the product or service included in the offer.
     * 
     * @return
     *     The availabilityEnds
     */
    @JsonProperty("availabilityEnds")
    public Date getAvailabilityEnds() {
        return availabilityEnds;
    }

    /**
     * The end of the availability of the product or service included in the offer.
     * 
     * @param availabilityEnds
     *     The availabilityEnds
     */
    @JsonProperty("availabilityEnds")
    public void setAvailabilityEnds(Date availabilityEnds) {
        this.availabilityEnds = availabilityEnds;
    }

    public Seeks withAvailabilityEnds(Date availabilityEnds) {
        this.availabilityEnds = availabilityEnds;
        return this;
    }

    /**
     * The end of the validity of offer, price specification, or opening hours data.
     * 
     * @return
     *     The validThrough
     */
    @JsonProperty("validThrough")
    public Date getValidThrough() {
        return validThrough;
    }

    /**
     * The end of the validity of offer, price specification, or opening hours data.
     * 
     * @param validThrough
     *     The validThrough
     */
    @JsonProperty("validThrough")
    public void setValidThrough(Date validThrough) {
        this.validThrough = validThrough;
    }

    public Seeks withValidThrough(Date validThrough) {
        this.validThrough = validThrough;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public Seeks withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public Seeks withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Seeks withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public Seeks withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * The serial number or any alphanumeric identifier of a particular product. When attached to an offer, it is a shortcut for the serial number of the product included in the offer.
     * 
     * @return
     *     The serialNumber
     */
    @JsonProperty("serialNumber")
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * The serial number or any alphanumeric identifier of a particular product. When attached to an offer, it is a shortcut for the serial number of the product included in the offer.
     * 
     * @param serialNumber
     *     The serialNumber
     */
    @JsonProperty("serialNumber")
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Seeks withSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
        return this;
    }

    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-14.aspx">GTIN-14</a> code of the product, or the product to which the offer refers. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     * @return
     *     The gtin14
     */
    @JsonProperty("gtin14")
    public String getGtin14() {
        return gtin14;
    }

    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-14.aspx">GTIN-14</a> code of the product, or the product to which the offer refers. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     * @param gtin14
     *     The gtin14
     */
    @JsonProperty("gtin14")
    public void setGtin14(String gtin14) {
        this.gtin14 = gtin14;
    }

    public Seeks withGtin14(String gtin14) {
        this.gtin14 = gtin14;
        return this;
    }

    /**
     * The item being offered.
     * 
     * @return
     *     The itemOffered
     */
    @JsonProperty("itemOffered")
    public java.lang.Object getItemOffered() {
        return itemOffered;
    }

    /**
     * The item being offered.
     * 
     * @param itemOffered
     *     The itemOffered
     */
    @JsonProperty("itemOffered")
    public void setItemOffered(java.lang.Object itemOffered) {
        this.itemOffered = itemOffered;
    }

    public Seeks withItemOffered(java.lang.Object itemOffered) {
        this.itemOffered = itemOffered;
        return this;
    }

    /**
     * The geographic area where a service or offered item is provided.
     * 
     * @return
     *     The areaServed
     */
    @JsonProperty("areaServed")
    public java.lang.Object getAreaServed() {
        return areaServed;
    }

    /**
     * The geographic area where a service or offered item is provided.
     * 
     * @param areaServed
     *     The areaServed
     */
    @JsonProperty("areaServed")
    public void setAreaServed(java.lang.Object areaServed) {
        this.areaServed = areaServed;
    }

    public Seeks withAreaServed(java.lang.Object areaServed) {
        this.areaServed = areaServed;
        return this;
    }

    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-12.aspx">GTIN-12</a> code of the product, or the product to which the offer refers. The GTIN-12 is the 12-digit GS1 Identification Key composed of a U.P.C. Company Prefix, Item Reference, and Check Digit used to identify trade items. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     * @return
     *     The gtin12
     */
    @JsonProperty("gtin12")
    public String getGtin12() {
        return gtin12;
    }

    /**
     * The <a href="http://apps.gs1.org/GDD/glossary/Pages/GTIN-12.aspx">GTIN-12</a> code of the product, or the product to which the offer refers. The GTIN-12 is the 12-digit GS1 Identification Key composed of a U.P.C. Company Prefix, Item Reference, and Check Digit used to identify trade items. See <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GS1 GTIN Summary</a> for more details.
     * 
     * @param gtin12
     *     The gtin12
     */
    @JsonProperty("gtin12")
    public void setGtin12(String gtin12) {
        this.gtin12 = gtin12;
    }

    public Seeks withGtin12(String gtin12) {
        this.gtin12 = gtin12;
        return this;
    }

    /**
     * The date when the item becomes valid.
     * 
     * @return
     *     The validFrom
     */
    @JsonProperty("validFrom")
    public Date getValidFrom() {
        return validFrom;
    }

    /**
     * The date when the item becomes valid.
     * 
     * @param validFrom
     *     The validFrom
     */
    @JsonProperty("validFrom")
    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Seeks withValidFrom(Date validFrom) {
        this.validFrom = validFrom;
        return this;
    }

    /**
     * OfferItemCondition
     * <p>
     * A list of possible conditions for the item.
     * 
     * @return
     *     The itemCondition
     */
    @JsonProperty("itemCondition")
    public ItemCondition getItemCondition() {
        return itemCondition;
    }

    /**
     * OfferItemCondition
     * <p>
     * A list of possible conditions for the item.
     * 
     * @param itemCondition
     *     The itemCondition
     */
    @JsonProperty("itemCondition")
    public void setItemCondition(ItemCondition itemCondition) {
        this.itemCondition = itemCondition;
    }

    public Seeks withItemCondition(ItemCondition itemCondition) {
        this.itemCondition = itemCondition;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(warranty).append(ineligibleRegion).append(sameAs).append(eligibleRegion).append(image).append(gtin8).append(additionalType).append(availability).append(inventoryLevel).append(includesObject).append(availableDeliveryMethod).append(seller).append(availabilityStarts).append(eligibleDuration).append(mainEntityOfPage).append(businessFunction).append(advanceBookingRequirement).append(sku).append(availableAtOrFrom).append(description).append(mpn).append(eligibleCustomerType).append(priceSpecification).append(acceptedPaymentMethod).append(eligibleTransactionVolume).append(deliveryLeadTime).append(eligibleQuantity).append(gtin13).append(availabilityEnds).append(validThrough).append(alternateName).append(potentialAction).append(name).append(url).append(serialNumber).append(gtin14).append(itemOffered).append(areaServed).append(gtin12).append(validFrom).append(itemCondition).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Seeks) == false) {
            return false;
        }
        Seeks rhs = ((Seeks) other);
        return new EqualsBuilder().append(warranty, rhs.warranty).append(ineligibleRegion, rhs.ineligibleRegion).append(sameAs, rhs.sameAs).append(eligibleRegion, rhs.eligibleRegion).append(image, rhs.image).append(gtin8, rhs.gtin8).append(additionalType, rhs.additionalType).append(availability, rhs.availability).append(inventoryLevel, rhs.inventoryLevel).append(includesObject, rhs.includesObject).append(availableDeliveryMethod, rhs.availableDeliveryMethod).append(seller, rhs.seller).append(availabilityStarts, rhs.availabilityStarts).append(eligibleDuration, rhs.eligibleDuration).append(mainEntityOfPage, rhs.mainEntityOfPage).append(businessFunction, rhs.businessFunction).append(advanceBookingRequirement, rhs.advanceBookingRequirement).append(sku, rhs.sku).append(availableAtOrFrom, rhs.availableAtOrFrom).append(description, rhs.description).append(mpn, rhs.mpn).append(eligibleCustomerType, rhs.eligibleCustomerType).append(priceSpecification, rhs.priceSpecification).append(acceptedPaymentMethod, rhs.acceptedPaymentMethod).append(eligibleTransactionVolume, rhs.eligibleTransactionVolume).append(deliveryLeadTime, rhs.deliveryLeadTime).append(eligibleQuantity, rhs.eligibleQuantity).append(gtin13, rhs.gtin13).append(availabilityEnds, rhs.availabilityEnds).append(validThrough, rhs.validThrough).append(alternateName, rhs.alternateName).append(potentialAction, rhs.potentialAction).append(name, rhs.name).append(url, rhs.url).append(serialNumber, rhs.serialNumber).append(gtin14, rhs.gtin14).append(itemOffered, rhs.itemOffered).append(areaServed, rhs.areaServed).append(gtin12, rhs.gtin12).append(validFrom, rhs.validFrom).append(itemCondition, rhs.itemCondition).isEquals();
    }

}
