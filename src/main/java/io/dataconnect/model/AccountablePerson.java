
package io.dataconnect.model;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Person
 * <p>
 * A person (alive, dead, undead, or fictional).
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "sibling",
    "honorificPrefix",
    "weight",
    "sameAs",
    "image",
    "relatedTo",
    "telephone",
    "birthDate",
    "height",
    "affiliation",
    "additionalName",
    "workLocation",
    "gender",
    "hasOfferCatalog",
    "additionalType",
    "contactPoint",
    "knows",
    "performerIn",
    "spouse",
    "worksFor",
    "taxID",
    "children",
    "parents",
    "faxNumber",
    "netWorth",
    "mainEntityOfPage",
    "siblings",
    "homeLocation",
    "email",
    "seeks",
    "colleague",
    "isicV4",
    "birthPlace",
    "description",
    "parent",
    "memberOf",
    "jobTitle",
    "brand",
    "familyName",
    "award",
    "awards",
    "address",
    "duns",
    "nationality",
    "alternateName",
    "deathDate",
    "makesOffer",
    "hasPOS",
    "colleagues",
    "potentialAction",
    "name",
    "naics",
    "url",
    "follows",
    "vatID",
    "honorificSuffix",
    "deathPlace",
    "owns",
    "givenName",
    "contactPoints",
    "alumniOf",
    "globalLocationNumber"
})
public class AccountablePerson {

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("sibling")
    @JsonPropertyDescription("")
    private AccountablePerson sibling;
    /**
     * An honorific prefix preceding a Person's name such as Dr/Mrs/Mr.
     * 
     */
    @JsonProperty("honorificPrefix")
    @JsonPropertyDescription("")
    private String honorificPrefix;
    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     */
    @JsonProperty("weight")
    @JsonPropertyDescription("")
    private Weight weight;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("relatedTo")
    @JsonPropertyDescription("")
    private AccountablePerson relatedTo;
    /**
     * The telephone number.
     * 
     */
    @JsonProperty("telephone")
    @JsonPropertyDescription("")
    private String telephone;
    /**
     * Date of birth.
     * 
     */
    @JsonProperty("birthDate")
    @JsonPropertyDescription("")
    private Date birthDate;
    /**
     * The height of the item.
     * 
     */
    @JsonProperty("height")
    @JsonPropertyDescription("")
    private java.lang.Object height;
    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     */
    @JsonProperty("affiliation")
    @JsonPropertyDescription("")
    private Affiliation affiliation;
    /**
     * An additional name for a Person, can be used for a middle name.
     * 
     */
    @JsonProperty("additionalName")
    @JsonPropertyDescription("")
    private String additionalName;
    /**
     * A contact location for a person's place of work.
     * 
     */
    @JsonProperty("workLocation")
    @JsonPropertyDescription("")
    private java.lang.Object workLocation;
    /**
     * Gender of the person.
     * 
     */
    @JsonProperty("gender")
    @JsonPropertyDescription("")
    private String gender;
    /**
     * This is a generated, and simplified, variant of https://schema.org/OfferCatalog. I has been interpreted as a plain array, this behaviour is hard-coded to the itemList types and should be improved.
     * 
     */
    @JsonProperty("hasOfferCatalog")
    @JsonPropertyDescription("")
    private List<java.lang.Object> hasOfferCatalog = new ArrayList<java.lang.Object>();
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     */
    @JsonProperty("contactPoint")
    @JsonPropertyDescription("")
    private ContactPoint contactPoint;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("knows")
    @JsonPropertyDescription("")
    private AccountablePerson knows;
    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     */
    @JsonProperty("performerIn")
    @JsonPropertyDescription("")
    private Event performerIn;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("spouse")
    @JsonPropertyDescription("")
    private AccountablePerson spouse;
    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     */
    @JsonProperty("worksFor")
    @JsonPropertyDescription("")
    private Affiliation worksFor;
    /**
     * The Tax / Fiscal ID of the organization or person, e.g. the TIN in the US or the CIF/NIF in Spain.
     * 
     */
    @JsonProperty("taxID")
    @JsonPropertyDescription("")
    private String taxID;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("children")
    @JsonPropertyDescription("")
    private AccountablePerson children;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("parents")
    @JsonPropertyDescription("")
    private AccountablePerson parents;
    /**
     * The fax number.
     * 
     */
    @JsonProperty("faxNumber")
    @JsonPropertyDescription("")
    private String faxNumber;
    /**
     * PriceSpecification
     * <p>
     * A structured value representing a monetary amount. Typically, only the subclasses of this type are used for markup.
     * 
     */
    @JsonProperty("netWorth")
    @JsonPropertyDescription("")
    private PriceSpecification netWorth;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("siblings")
    @JsonPropertyDescription("")
    private AccountablePerson siblings;
    /**
     * A contact location for a person's residence.
     * 
     */
    @JsonProperty("homeLocation")
    @JsonPropertyDescription("")
    private java.lang.Object homeLocation;
    /**
     * Email address.
     * 
     */
    @JsonProperty("email")
    @JsonPropertyDescription("")
    private String email;
    /**
     * Demand
     * <p>
     * A demand entity represents the public, not necessarily binding, not necessarily exclusive, announcement by an organization or person to seek a certain type of goods or services. For describing demand using this type, the very same properties used for Offer apply.
     * 
     */
    @JsonProperty("seeks")
    @JsonPropertyDescription("")
    private Seeks seeks;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("colleague")
    @JsonPropertyDescription("")
    private AccountablePerson colleague;
    /**
     * The International Standard of Industrial Classification of All Economic Activities (ISIC), Revision 4 code for a particular organization, business person, or place.
     * 
     */
    @JsonProperty("isicV4")
    @JsonPropertyDescription("")
    private String isicV4;
    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     */
    @JsonProperty("birthPlace")
    @JsonPropertyDescription("")
    private ContainsPlace birthPlace;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("parent")
    @JsonPropertyDescription("")
    private AccountablePerson parent;
    /**
     * An Organization (or ProgramMembership) to which this Person or Organization belongs.
     * 
     */
    @JsonProperty("memberOf")
    @JsonPropertyDescription("")
    private java.lang.Object memberOf;
    /**
     * The job title of the person (for example, Financial Manager).
     * 
     */
    @JsonProperty("jobTitle")
    @JsonPropertyDescription("")
    private String jobTitle;
    /**
     * The brand(s) associated with a product or service, or the brand(s) maintained by an organization or business person.
     * 
     */
    @JsonProperty("brand")
    @JsonPropertyDescription("")
    private java.lang.Object brand;
    /**
     * Family name. In the U.S., the last name of an Person. This can be used along with givenName instead of the name property.
     * 
     */
    @JsonProperty("familyName")
    @JsonPropertyDescription("")
    private String familyName;
    /**
     * An award won by or for this item.
     * 
     */
    @JsonProperty("award")
    @JsonPropertyDescription("")
    private String award;
    /**
     * Awards won by or for this item.
     * 
     */
    @JsonProperty("awards")
    @JsonPropertyDescription("")
    private String awards;
    /**
     * Physical address of the item.
     * 
     */
    @JsonProperty("address")
    @JsonPropertyDescription("")
    private java.lang.Object address;
    /**
     * The Dun & Bradstreet DUNS number for identifying an organization or business person.
     * 
     */
    @JsonProperty("duns")
    @JsonPropertyDescription("")
    private String duns;
    /**
     * Country
     * <p>
     * A country.
     * 
     */
    @JsonProperty("nationality")
    @JsonPropertyDescription("")
    private Nationality nationality;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * Date of death.
     * 
     */
    @JsonProperty("deathDate")
    @JsonPropertyDescription("")
    private Date deathDate;
    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     */
    @JsonProperty("makesOffer")
    @JsonPropertyDescription("")
    private Offer makesOffer;
    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     */
    @JsonProperty("hasPOS")
    @JsonPropertyDescription("")
    private ContainsPlace hasPOS;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("colleagues")
    @JsonPropertyDescription("")
    private AccountablePerson colleagues;
    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;
    /**
     * The North American Industry Classification System (NAICS) code for a particular organization or business person.
     * 
     */
    @JsonProperty("naics")
    @JsonPropertyDescription("")
    private String naics;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("follows")
    @JsonPropertyDescription("")
    private AccountablePerson follows;
    /**
     * The Value-added Tax ID of the organization or person.
     * 
     */
    @JsonProperty("vatID")
    @JsonPropertyDescription("")
    private String vatID;
    /**
     * An honorific suffix preceding a Person's name such as M.D. /PhD/MSCSW.
     * 
     */
    @JsonProperty("honorificSuffix")
    @JsonPropertyDescription("")
    private String honorificSuffix;
    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     */
    @JsonProperty("deathPlace")
    @JsonPropertyDescription("")
    private ContainsPlace deathPlace;
    /**
     * Products owned by the organization or person.
     * 
     */
    @JsonProperty("owns")
    @JsonPropertyDescription("")
    private java.lang.Object owns;
    /**
     * Given name. In the U.S., the first name of a Person. This can be used along with familyName instead of the name property.
     * 
     */
    @JsonProperty("givenName")
    @JsonPropertyDescription("")
    private String givenName;
    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     */
    @JsonProperty("contactPoints")
    @JsonPropertyDescription("")
    private ContactPoint contactPoints;
    /**
     * An organization that the person is an alumni of.
     * 
     */
    @JsonProperty("alumniOf")
    @JsonPropertyDescription("")
    private java.lang.Object alumniOf;
    /**
     * The <a href="http://www.gs1.org/gln">Global Location Number</a> (GLN, sometimes also referred to as International Location Number or ILN) of the respective organization, person, or place. The GLN is a 13-digit number used to identify parties and physical locations.
     * 
     */
    @JsonProperty("globalLocationNumber")
    @JsonPropertyDescription("")
    private String globalLocationNumber;

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The sibling
     */
    @JsonProperty("sibling")
    public AccountablePerson getSibling() {
        return sibling;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param sibling
     *     The sibling
     */
    @JsonProperty("sibling")
    public void setSibling(AccountablePerson sibling) {
        this.sibling = sibling;
    }

    public AccountablePerson withSibling(AccountablePerson sibling) {
        this.sibling = sibling;
        return this;
    }

    /**
     * An honorific prefix preceding a Person's name such as Dr/Mrs/Mr.
     * 
     * @return
     *     The honorificPrefix
     */
    @JsonProperty("honorificPrefix")
    public String getHonorificPrefix() {
        return honorificPrefix;
    }

    /**
     * An honorific prefix preceding a Person's name such as Dr/Mrs/Mr.
     * 
     * @param honorificPrefix
     *     The honorificPrefix
     */
    @JsonProperty("honorificPrefix")
    public void setHonorificPrefix(String honorificPrefix) {
        this.honorificPrefix = honorificPrefix;
    }

    public AccountablePerson withHonorificPrefix(String honorificPrefix) {
        this.honorificPrefix = honorificPrefix;
        return this;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @return
     *     The weight
     */
    @JsonProperty("weight")
    public Weight getWeight() {
        return weight;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @param weight
     *     The weight
     */
    @JsonProperty("weight")
    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    public AccountablePerson withWeight(Weight weight) {
        this.weight = weight;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public AccountablePerson withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public AccountablePerson withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The relatedTo
     */
    @JsonProperty("relatedTo")
    public AccountablePerson getRelatedTo() {
        return relatedTo;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param relatedTo
     *     The relatedTo
     */
    @JsonProperty("relatedTo")
    public void setRelatedTo(AccountablePerson relatedTo) {
        this.relatedTo = relatedTo;
    }

    public AccountablePerson withRelatedTo(AccountablePerson relatedTo) {
        this.relatedTo = relatedTo;
        return this;
    }

    /**
     * The telephone number.
     * 
     * @return
     *     The telephone
     */
    @JsonProperty("telephone")
    public String getTelephone() {
        return telephone;
    }

    /**
     * The telephone number.
     * 
     * @param telephone
     *     The telephone
     */
    @JsonProperty("telephone")
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public AccountablePerson withTelephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    /**
     * Date of birth.
     * 
     * @return
     *     The birthDate
     */
    @JsonProperty("birthDate")
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * Date of birth.
     * 
     * @param birthDate
     *     The birthDate
     */
    @JsonProperty("birthDate")
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public AccountablePerson withBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    /**
     * The height of the item.
     * 
     * @return
     *     The height
     */
    @JsonProperty("height")
    public java.lang.Object getHeight() {
        return height;
    }

    /**
     * The height of the item.
     * 
     * @param height
     *     The height
     */
    @JsonProperty("height")
    public void setHeight(java.lang.Object height) {
        this.height = height;
    }

    public AccountablePerson withHeight(java.lang.Object height) {
        this.height = height;
        return this;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @return
     *     The affiliation
     */
    @JsonProperty("affiliation")
    public Affiliation getAffiliation() {
        return affiliation;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @param affiliation
     *     The affiliation
     */
    @JsonProperty("affiliation")
    public void setAffiliation(Affiliation affiliation) {
        this.affiliation = affiliation;
    }

    public AccountablePerson withAffiliation(Affiliation affiliation) {
        this.affiliation = affiliation;
        return this;
    }

    /**
     * An additional name for a Person, can be used for a middle name.
     * 
     * @return
     *     The additionalName
     */
    @JsonProperty("additionalName")
    public String getAdditionalName() {
        return additionalName;
    }

    /**
     * An additional name for a Person, can be used for a middle name.
     * 
     * @param additionalName
     *     The additionalName
     */
    @JsonProperty("additionalName")
    public void setAdditionalName(String additionalName) {
        this.additionalName = additionalName;
    }

    public AccountablePerson withAdditionalName(String additionalName) {
        this.additionalName = additionalName;
        return this;
    }

    /**
     * A contact location for a person's place of work.
     * 
     * @return
     *     The workLocation
     */
    @JsonProperty("workLocation")
    public java.lang.Object getWorkLocation() {
        return workLocation;
    }

    /**
     * A contact location for a person's place of work.
     * 
     * @param workLocation
     *     The workLocation
     */
    @JsonProperty("workLocation")
    public void setWorkLocation(java.lang.Object workLocation) {
        this.workLocation = workLocation;
    }

    public AccountablePerson withWorkLocation(java.lang.Object workLocation) {
        this.workLocation = workLocation;
        return this;
    }

    /**
     * Gender of the person.
     * 
     * @return
     *     The gender
     */
    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    /**
     * Gender of the person.
     * 
     * @param gender
     *     The gender
     */
    @JsonProperty("gender")
    public void setGender(String gender) {
        this.gender = gender;
    }

    public AccountablePerson withGender(String gender) {
        this.gender = gender;
        return this;
    }

    /**
     * This is a generated, and simplified, variant of https://schema.org/OfferCatalog. I has been interpreted as a plain array, this behaviour is hard-coded to the itemList types and should be improved.
     * 
     * @return
     *     The hasOfferCatalog
     */
    @JsonProperty("hasOfferCatalog")
    public List<java.lang.Object> getHasOfferCatalog() {
        return hasOfferCatalog;
    }

    /**
     * This is a generated, and simplified, variant of https://schema.org/OfferCatalog. I has been interpreted as a plain array, this behaviour is hard-coded to the itemList types and should be improved.
     * 
     * @param hasOfferCatalog
     *     The hasOfferCatalog
     */
    @JsonProperty("hasOfferCatalog")
    public void setHasOfferCatalog(List<java.lang.Object> hasOfferCatalog) {
        this.hasOfferCatalog = hasOfferCatalog;
    }

    public AccountablePerson withHasOfferCatalog(List<java.lang.Object> hasOfferCatalog) {
        this.hasOfferCatalog = hasOfferCatalog;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public AccountablePerson withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     * @return
     *     The contactPoint
     */
    @JsonProperty("contactPoint")
    public ContactPoint getContactPoint() {
        return contactPoint;
    }

    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     * @param contactPoint
     *     The contactPoint
     */
    @JsonProperty("contactPoint")
    public void setContactPoint(ContactPoint contactPoint) {
        this.contactPoint = contactPoint;
    }

    public AccountablePerson withContactPoint(ContactPoint contactPoint) {
        this.contactPoint = contactPoint;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The knows
     */
    @JsonProperty("knows")
    public AccountablePerson getKnows() {
        return knows;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param knows
     *     The knows
     */
    @JsonProperty("knows")
    public void setKnows(AccountablePerson knows) {
        this.knows = knows;
    }

    public AccountablePerson withKnows(AccountablePerson knows) {
        this.knows = knows;
        return this;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @return
     *     The performerIn
     */
    @JsonProperty("performerIn")
    public Event getPerformerIn() {
        return performerIn;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @param performerIn
     *     The performerIn
     */
    @JsonProperty("performerIn")
    public void setPerformerIn(Event performerIn) {
        this.performerIn = performerIn;
    }

    public AccountablePerson withPerformerIn(Event performerIn) {
        this.performerIn = performerIn;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The spouse
     */
    @JsonProperty("spouse")
    public AccountablePerson getSpouse() {
        return spouse;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param spouse
     *     The spouse
     */
    @JsonProperty("spouse")
    public void setSpouse(AccountablePerson spouse) {
        this.spouse = spouse;
    }

    public AccountablePerson withSpouse(AccountablePerson spouse) {
        this.spouse = spouse;
        return this;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @return
     *     The worksFor
     */
    @JsonProperty("worksFor")
    public Affiliation getWorksFor() {
        return worksFor;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @param worksFor
     *     The worksFor
     */
    @JsonProperty("worksFor")
    public void setWorksFor(Affiliation worksFor) {
        this.worksFor = worksFor;
    }

    public AccountablePerson withWorksFor(Affiliation worksFor) {
        this.worksFor = worksFor;
        return this;
    }

    /**
     * The Tax / Fiscal ID of the organization or person, e.g. the TIN in the US or the CIF/NIF in Spain.
     * 
     * @return
     *     The taxID
     */
    @JsonProperty("taxID")
    public String getTaxID() {
        return taxID;
    }

    /**
     * The Tax / Fiscal ID of the organization or person, e.g. the TIN in the US or the CIF/NIF in Spain.
     * 
     * @param taxID
     *     The taxID
     */
    @JsonProperty("taxID")
    public void setTaxID(String taxID) {
        this.taxID = taxID;
    }

    public AccountablePerson withTaxID(String taxID) {
        this.taxID = taxID;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The children
     */
    @JsonProperty("children")
    public AccountablePerson getChildren() {
        return children;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param children
     *     The children
     */
    @JsonProperty("children")
    public void setChildren(AccountablePerson children) {
        this.children = children;
    }

    public AccountablePerson withChildren(AccountablePerson children) {
        this.children = children;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The parents
     */
    @JsonProperty("parents")
    public AccountablePerson getParents() {
        return parents;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param parents
     *     The parents
     */
    @JsonProperty("parents")
    public void setParents(AccountablePerson parents) {
        this.parents = parents;
    }

    public AccountablePerson withParents(AccountablePerson parents) {
        this.parents = parents;
        return this;
    }

    /**
     * The fax number.
     * 
     * @return
     *     The faxNumber
     */
    @JsonProperty("faxNumber")
    public String getFaxNumber() {
        return faxNumber;
    }

    /**
     * The fax number.
     * 
     * @param faxNumber
     *     The faxNumber
     */
    @JsonProperty("faxNumber")
    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public AccountablePerson withFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
        return this;
    }

    /**
     * PriceSpecification
     * <p>
     * A structured value representing a monetary amount. Typically, only the subclasses of this type are used for markup.
     * 
     * @return
     *     The netWorth
     */
    @JsonProperty("netWorth")
    public PriceSpecification getNetWorth() {
        return netWorth;
    }

    /**
     * PriceSpecification
     * <p>
     * A structured value representing a monetary amount. Typically, only the subclasses of this type are used for markup.
     * 
     * @param netWorth
     *     The netWorth
     */
    @JsonProperty("netWorth")
    public void setNetWorth(PriceSpecification netWorth) {
        this.netWorth = netWorth;
    }

    public AccountablePerson withNetWorth(PriceSpecification netWorth) {
        this.netWorth = netWorth;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public AccountablePerson withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The siblings
     */
    @JsonProperty("siblings")
    public AccountablePerson getSiblings() {
        return siblings;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param siblings
     *     The siblings
     */
    @JsonProperty("siblings")
    public void setSiblings(AccountablePerson siblings) {
        this.siblings = siblings;
    }

    public AccountablePerson withSiblings(AccountablePerson siblings) {
        this.siblings = siblings;
        return this;
    }

    /**
     * A contact location for a person's residence.
     * 
     * @return
     *     The homeLocation
     */
    @JsonProperty("homeLocation")
    public java.lang.Object getHomeLocation() {
        return homeLocation;
    }

    /**
     * A contact location for a person's residence.
     * 
     * @param homeLocation
     *     The homeLocation
     */
    @JsonProperty("homeLocation")
    public void setHomeLocation(java.lang.Object homeLocation) {
        this.homeLocation = homeLocation;
    }

    public AccountablePerson withHomeLocation(java.lang.Object homeLocation) {
        this.homeLocation = homeLocation;
        return this;
    }

    /**
     * Email address.
     * 
     * @return
     *     The email
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     * Email address.
     * 
     * @param email
     *     The email
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    public AccountablePerson withEmail(String email) {
        this.email = email;
        return this;
    }

    /**
     * Demand
     * <p>
     * A demand entity represents the public, not necessarily binding, not necessarily exclusive, announcement by an organization or person to seek a certain type of goods or services. For describing demand using this type, the very same properties used for Offer apply.
     * 
     * @return
     *     The seeks
     */
    @JsonProperty("seeks")
    public Seeks getSeeks() {
        return seeks;
    }

    /**
     * Demand
     * <p>
     * A demand entity represents the public, not necessarily binding, not necessarily exclusive, announcement by an organization or person to seek a certain type of goods or services. For describing demand using this type, the very same properties used for Offer apply.
     * 
     * @param seeks
     *     The seeks
     */
    @JsonProperty("seeks")
    public void setSeeks(Seeks seeks) {
        this.seeks = seeks;
    }

    public AccountablePerson withSeeks(Seeks seeks) {
        this.seeks = seeks;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The colleague
     */
    @JsonProperty("colleague")
    public AccountablePerson getColleague() {
        return colleague;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param colleague
     *     The colleague
     */
    @JsonProperty("colleague")
    public void setColleague(AccountablePerson colleague) {
        this.colleague = colleague;
    }

    public AccountablePerson withColleague(AccountablePerson colleague) {
        this.colleague = colleague;
        return this;
    }

    /**
     * The International Standard of Industrial Classification of All Economic Activities (ISIC), Revision 4 code for a particular organization, business person, or place.
     * 
     * @return
     *     The isicV4
     */
    @JsonProperty("isicV4")
    public String getIsicV4() {
        return isicV4;
    }

    /**
     * The International Standard of Industrial Classification of All Economic Activities (ISIC), Revision 4 code for a particular organization, business person, or place.
     * 
     * @param isicV4
     *     The isicV4
     */
    @JsonProperty("isicV4")
    public void setIsicV4(String isicV4) {
        this.isicV4 = isicV4;
    }

    public AccountablePerson withIsicV4(String isicV4) {
        this.isicV4 = isicV4;
        return this;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @return
     *     The birthPlace
     */
    @JsonProperty("birthPlace")
    public ContainsPlace getBirthPlace() {
        return birthPlace;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @param birthPlace
     *     The birthPlace
     */
    @JsonProperty("birthPlace")
    public void setBirthPlace(ContainsPlace birthPlace) {
        this.birthPlace = birthPlace;
    }

    public AccountablePerson withBirthPlace(ContainsPlace birthPlace) {
        this.birthPlace = birthPlace;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public AccountablePerson withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The parent
     */
    @JsonProperty("parent")
    public AccountablePerson getParent() {
        return parent;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param parent
     *     The parent
     */
    @JsonProperty("parent")
    public void setParent(AccountablePerson parent) {
        this.parent = parent;
    }

    public AccountablePerson withParent(AccountablePerson parent) {
        this.parent = parent;
        return this;
    }

    /**
     * An Organization (or ProgramMembership) to which this Person or Organization belongs.
     * 
     * @return
     *     The memberOf
     */
    @JsonProperty("memberOf")
    public java.lang.Object getMemberOf() {
        return memberOf;
    }

    /**
     * An Organization (or ProgramMembership) to which this Person or Organization belongs.
     * 
     * @param memberOf
     *     The memberOf
     */
    @JsonProperty("memberOf")
    public void setMemberOf(java.lang.Object memberOf) {
        this.memberOf = memberOf;
    }

    public AccountablePerson withMemberOf(java.lang.Object memberOf) {
        this.memberOf = memberOf;
        return this;
    }

    /**
     * The job title of the person (for example, Financial Manager).
     * 
     * @return
     *     The jobTitle
     */
    @JsonProperty("jobTitle")
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     * The job title of the person (for example, Financial Manager).
     * 
     * @param jobTitle
     *     The jobTitle
     */
    @JsonProperty("jobTitle")
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public AccountablePerson withJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
        return this;
    }

    /**
     * The brand(s) associated with a product or service, or the brand(s) maintained by an organization or business person.
     * 
     * @return
     *     The brand
     */
    @JsonProperty("brand")
    public java.lang.Object getBrand() {
        return brand;
    }

    /**
     * The brand(s) associated with a product or service, or the brand(s) maintained by an organization or business person.
     * 
     * @param brand
     *     The brand
     */
    @JsonProperty("brand")
    public void setBrand(java.lang.Object brand) {
        this.brand = brand;
    }

    public AccountablePerson withBrand(java.lang.Object brand) {
        this.brand = brand;
        return this;
    }

    /**
     * Family name. In the U.S., the last name of an Person. This can be used along with givenName instead of the name property.
     * 
     * @return
     *     The familyName
     */
    @JsonProperty("familyName")
    public String getFamilyName() {
        return familyName;
    }

    /**
     * Family name. In the U.S., the last name of an Person. This can be used along with givenName instead of the name property.
     * 
     * @param familyName
     *     The familyName
     */
    @JsonProperty("familyName")
    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public AccountablePerson withFamilyName(String familyName) {
        this.familyName = familyName;
        return this;
    }

    /**
     * An award won by or for this item.
     * 
     * @return
     *     The award
     */
    @JsonProperty("award")
    public String getAward() {
        return award;
    }

    /**
     * An award won by or for this item.
     * 
     * @param award
     *     The award
     */
    @JsonProperty("award")
    public void setAward(String award) {
        this.award = award;
    }

    public AccountablePerson withAward(String award) {
        this.award = award;
        return this;
    }

    /**
     * Awards won by or for this item.
     * 
     * @return
     *     The awards
     */
    @JsonProperty("awards")
    public String getAwards() {
        return awards;
    }

    /**
     * Awards won by or for this item.
     * 
     * @param awards
     *     The awards
     */
    @JsonProperty("awards")
    public void setAwards(String awards) {
        this.awards = awards;
    }

    public AccountablePerson withAwards(String awards) {
        this.awards = awards;
        return this;
    }

    /**
     * Physical address of the item.
     * 
     * @return
     *     The address
     */
    @JsonProperty("address")
    public java.lang.Object getAddress() {
        return address;
    }

    /**
     * Physical address of the item.
     * 
     * @param address
     *     The address
     */
    @JsonProperty("address")
    public void setAddress(java.lang.Object address) {
        this.address = address;
    }

    public AccountablePerson withAddress(java.lang.Object address) {
        this.address = address;
        return this;
    }

    /**
     * The Dun & Bradstreet DUNS number for identifying an organization or business person.
     * 
     * @return
     *     The duns
     */
    @JsonProperty("duns")
    public String getDuns() {
        return duns;
    }

    /**
     * The Dun & Bradstreet DUNS number for identifying an organization or business person.
     * 
     * @param duns
     *     The duns
     */
    @JsonProperty("duns")
    public void setDuns(String duns) {
        this.duns = duns;
    }

    public AccountablePerson withDuns(String duns) {
        this.duns = duns;
        return this;
    }

    /**
     * Country
     * <p>
     * A country.
     * 
     * @return
     *     The nationality
     */
    @JsonProperty("nationality")
    public Nationality getNationality() {
        return nationality;
    }

    /**
     * Country
     * <p>
     * A country.
     * 
     * @param nationality
     *     The nationality
     */
    @JsonProperty("nationality")
    public void setNationality(Nationality nationality) {
        this.nationality = nationality;
    }

    public AccountablePerson withNationality(Nationality nationality) {
        this.nationality = nationality;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public AccountablePerson withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * Date of death.
     * 
     * @return
     *     The deathDate
     */
    @JsonProperty("deathDate")
    public Date getDeathDate() {
        return deathDate;
    }

    /**
     * Date of death.
     * 
     * @param deathDate
     *     The deathDate
     */
    @JsonProperty("deathDate")
    public void setDeathDate(Date deathDate) {
        this.deathDate = deathDate;
    }

    public AccountablePerson withDeathDate(Date deathDate) {
        this.deathDate = deathDate;
        return this;
    }

    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     * @return
     *     The makesOffer
     */
    @JsonProperty("makesOffer")
    public Offer getMakesOffer() {
        return makesOffer;
    }

    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     * @param makesOffer
     *     The makesOffer
     */
    @JsonProperty("makesOffer")
    public void setMakesOffer(Offer makesOffer) {
        this.makesOffer = makesOffer;
    }

    public AccountablePerson withMakesOffer(Offer makesOffer) {
        this.makesOffer = makesOffer;
        return this;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @return
     *     The hasPOS
     */
    @JsonProperty("hasPOS")
    public ContainsPlace getHasPOS() {
        return hasPOS;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @param hasPOS
     *     The hasPOS
     */
    @JsonProperty("hasPOS")
    public void setHasPOS(ContainsPlace hasPOS) {
        this.hasPOS = hasPOS;
    }

    public AccountablePerson withHasPOS(ContainsPlace hasPOS) {
        this.hasPOS = hasPOS;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The colleagues
     */
    @JsonProperty("colleagues")
    public AccountablePerson getColleagues() {
        return colleagues;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param colleagues
     *     The colleagues
     */
    @JsonProperty("colleagues")
    public void setColleagues(AccountablePerson colleagues) {
        this.colleagues = colleagues;
    }

    public AccountablePerson withColleagues(AccountablePerson colleagues) {
        this.colleagues = colleagues;
        return this;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public AccountablePerson withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public AccountablePerson withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * The North American Industry Classification System (NAICS) code for a particular organization or business person.
     * 
     * @return
     *     The naics
     */
    @JsonProperty("naics")
    public String getNaics() {
        return naics;
    }

    /**
     * The North American Industry Classification System (NAICS) code for a particular organization or business person.
     * 
     * @param naics
     *     The naics
     */
    @JsonProperty("naics")
    public void setNaics(String naics) {
        this.naics = naics;
    }

    public AccountablePerson withNaics(String naics) {
        this.naics = naics;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public AccountablePerson withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The follows
     */
    @JsonProperty("follows")
    public AccountablePerson getFollows() {
        return follows;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param follows
     *     The follows
     */
    @JsonProperty("follows")
    public void setFollows(AccountablePerson follows) {
        this.follows = follows;
    }

    public AccountablePerson withFollows(AccountablePerson follows) {
        this.follows = follows;
        return this;
    }

    /**
     * The Value-added Tax ID of the organization or person.
     * 
     * @return
     *     The vatID
     */
    @JsonProperty("vatID")
    public String getVatID() {
        return vatID;
    }

    /**
     * The Value-added Tax ID of the organization or person.
     * 
     * @param vatID
     *     The vatID
     */
    @JsonProperty("vatID")
    public void setVatID(String vatID) {
        this.vatID = vatID;
    }

    public AccountablePerson withVatID(String vatID) {
        this.vatID = vatID;
        return this;
    }

    /**
     * An honorific suffix preceding a Person's name such as M.D. /PhD/MSCSW.
     * 
     * @return
     *     The honorificSuffix
     */
    @JsonProperty("honorificSuffix")
    public String getHonorificSuffix() {
        return honorificSuffix;
    }

    /**
     * An honorific suffix preceding a Person's name such as M.D. /PhD/MSCSW.
     * 
     * @param honorificSuffix
     *     The honorificSuffix
     */
    @JsonProperty("honorificSuffix")
    public void setHonorificSuffix(String honorificSuffix) {
        this.honorificSuffix = honorificSuffix;
    }

    public AccountablePerson withHonorificSuffix(String honorificSuffix) {
        this.honorificSuffix = honorificSuffix;
        return this;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @return
     *     The deathPlace
     */
    @JsonProperty("deathPlace")
    public ContainsPlace getDeathPlace() {
        return deathPlace;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @param deathPlace
     *     The deathPlace
     */
    @JsonProperty("deathPlace")
    public void setDeathPlace(ContainsPlace deathPlace) {
        this.deathPlace = deathPlace;
    }

    public AccountablePerson withDeathPlace(ContainsPlace deathPlace) {
        this.deathPlace = deathPlace;
        return this;
    }

    /**
     * Products owned by the organization or person.
     * 
     * @return
     *     The owns
     */
    @JsonProperty("owns")
    public java.lang.Object getOwns() {
        return owns;
    }

    /**
     * Products owned by the organization or person.
     * 
     * @param owns
     *     The owns
     */
    @JsonProperty("owns")
    public void setOwns(java.lang.Object owns) {
        this.owns = owns;
    }

    public AccountablePerson withOwns(java.lang.Object owns) {
        this.owns = owns;
        return this;
    }

    /**
     * Given name. In the U.S., the first name of a Person. This can be used along with familyName instead of the name property.
     * 
     * @return
     *     The givenName
     */
    @JsonProperty("givenName")
    public String getGivenName() {
        return givenName;
    }

    /**
     * Given name. In the U.S., the first name of a Person. This can be used along with familyName instead of the name property.
     * 
     * @param givenName
     *     The givenName
     */
    @JsonProperty("givenName")
    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public AccountablePerson withGivenName(String givenName) {
        this.givenName = givenName;
        return this;
    }

    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     * @return
     *     The contactPoints
     */
    @JsonProperty("contactPoints")
    public ContactPoint getContactPoints() {
        return contactPoints;
    }

    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     * @param contactPoints
     *     The contactPoints
     */
    @JsonProperty("contactPoints")
    public void setContactPoints(ContactPoint contactPoints) {
        this.contactPoints = contactPoints;
    }

    public AccountablePerson withContactPoints(ContactPoint contactPoints) {
        this.contactPoints = contactPoints;
        return this;
    }

    /**
     * An organization that the person is an alumni of.
     * 
     * @return
     *     The alumniOf
     */
    @JsonProperty("alumniOf")
    public java.lang.Object getAlumniOf() {
        return alumniOf;
    }

    /**
     * An organization that the person is an alumni of.
     * 
     * @param alumniOf
     *     The alumniOf
     */
    @JsonProperty("alumniOf")
    public void setAlumniOf(java.lang.Object alumniOf) {
        this.alumniOf = alumniOf;
    }

    public AccountablePerson withAlumniOf(java.lang.Object alumniOf) {
        this.alumniOf = alumniOf;
        return this;
    }

    /**
     * The <a href="http://www.gs1.org/gln">Global Location Number</a> (GLN, sometimes also referred to as International Location Number or ILN) of the respective organization, person, or place. The GLN is a 13-digit number used to identify parties and physical locations.
     * 
     * @return
     *     The globalLocationNumber
     */
    @JsonProperty("globalLocationNumber")
    public String getGlobalLocationNumber() {
        return globalLocationNumber;
    }

    /**
     * The <a href="http://www.gs1.org/gln">Global Location Number</a> (GLN, sometimes also referred to as International Location Number or ILN) of the respective organization, person, or place. The GLN is a 13-digit number used to identify parties and physical locations.
     * 
     * @param globalLocationNumber
     *     The globalLocationNumber
     */
    @JsonProperty("globalLocationNumber")
    public void setGlobalLocationNumber(String globalLocationNumber) {
        this.globalLocationNumber = globalLocationNumber;
    }

    public AccountablePerson withGlobalLocationNumber(String globalLocationNumber) {
        this.globalLocationNumber = globalLocationNumber;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(sibling).append(honorificPrefix).append(weight).append(sameAs).append(image).append(relatedTo).append(telephone).append(birthDate).append(height).append(affiliation).append(additionalName).append(workLocation).append(gender).append(hasOfferCatalog).append(additionalType).append(contactPoint).append(knows).append(performerIn).append(spouse).append(worksFor).append(taxID).append(children).append(parents).append(faxNumber).append(netWorth).append(mainEntityOfPage).append(siblings).append(homeLocation).append(email).append(seeks).append(colleague).append(isicV4).append(birthPlace).append(description).append(parent).append(memberOf).append(jobTitle).append(brand).append(familyName).append(award).append(awards).append(address).append(duns).append(nationality).append(alternateName).append(deathDate).append(makesOffer).append(hasPOS).append(colleagues).append(potentialAction).append(name).append(naics).append(url).append(follows).append(vatID).append(honorificSuffix).append(deathPlace).append(owns).append(givenName).append(contactPoints).append(alumniOf).append(globalLocationNumber).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AccountablePerson) == false) {
            return false;
        }
        AccountablePerson rhs = ((AccountablePerson) other);
        return new EqualsBuilder().append(sibling, rhs.sibling).append(honorificPrefix, rhs.honorificPrefix).append(weight, rhs.weight).append(sameAs, rhs.sameAs).append(image, rhs.image).append(relatedTo, rhs.relatedTo).append(telephone, rhs.telephone).append(birthDate, rhs.birthDate).append(height, rhs.height).append(affiliation, rhs.affiliation).append(additionalName, rhs.additionalName).append(workLocation, rhs.workLocation).append(gender, rhs.gender).append(hasOfferCatalog, rhs.hasOfferCatalog).append(additionalType, rhs.additionalType).append(contactPoint, rhs.contactPoint).append(knows, rhs.knows).append(performerIn, rhs.performerIn).append(spouse, rhs.spouse).append(worksFor, rhs.worksFor).append(taxID, rhs.taxID).append(children, rhs.children).append(parents, rhs.parents).append(faxNumber, rhs.faxNumber).append(netWorth, rhs.netWorth).append(mainEntityOfPage, rhs.mainEntityOfPage).append(siblings, rhs.siblings).append(homeLocation, rhs.homeLocation).append(email, rhs.email).append(seeks, rhs.seeks).append(colleague, rhs.colleague).append(isicV4, rhs.isicV4).append(birthPlace, rhs.birthPlace).append(description, rhs.description).append(parent, rhs.parent).append(memberOf, rhs.memberOf).append(jobTitle, rhs.jobTitle).append(brand, rhs.brand).append(familyName, rhs.familyName).append(award, rhs.award).append(awards, rhs.awards).append(address, rhs.address).append(duns, rhs.duns).append(nationality, rhs.nationality).append(alternateName, rhs.alternateName).append(deathDate, rhs.deathDate).append(makesOffer, rhs.makesOffer).append(hasPOS, rhs.hasPOS).append(colleagues, rhs.colleagues).append(potentialAction, rhs.potentialAction).append(name, rhs.name).append(naics, rhs.naics).append(url, rhs.url).append(follows, rhs.follows).append(vatID, rhs.vatID).append(honorificSuffix, rhs.honorificSuffix).append(deathPlace, rhs.deathPlace).append(owns, rhs.owns).append(givenName, rhs.givenName).append(contactPoints, rhs.contactPoints).append(alumniOf, rhs.alumniOf).append(globalLocationNumber, rhs.globalLocationNumber).isEquals();
    }

}
