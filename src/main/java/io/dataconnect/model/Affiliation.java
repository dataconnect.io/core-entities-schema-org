
package io.dataconnect.model;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Organization
 * <p>
 * An organization such as a school, NGO, corporation, club, etc.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "founder",
    "parentOrganization",
    "sameAs",
    "image",
    "foundingDate",
    "telephone",
    "faxNumber",
    "duns",
    "additionalType",
    "hasOfferCatalog",
    "logo",
    "contactPoint",
    "isicV4",
    "founders",
    "review",
    "aggregateRating",
    "taxID",
    "event",
    "member",
    "foundingLocation",
    "location",
    "mainEntityOfPage",
    "events",
    "seeks",
    "description",
    "numberOfEmployees",
    "brand",
    "legalName",
    "award",
    "employee",
    "dissolutionDate",
    "department",
    "awards",
    "members",
    "address",
    "subOrganization",
    "alternateName",
    "makesOffer",
    "hasPOS",
    "serviceArea",
    "potentialAction",
    "name",
    "naics",
    "url",
    "memberOf",
    "employees",
    "alumni",
    "email",
    "reviews",
    "areaServed",
    "owns",
    "vatID",
    "contactPoints",
    "globalLocationNumber"
})
public class Affiliation {

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("founder")
    @JsonPropertyDescription("")
    private AccountablePerson founder;
    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     */
    @JsonProperty("parentOrganization")
    @JsonPropertyDescription("")
    private Affiliation parentOrganization;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * The date that this organization was founded.
     * 
     */
    @JsonProperty("foundingDate")
    @JsonPropertyDescription("")
    private Date foundingDate;
    /**
     * The telephone number.
     * 
     */
    @JsonProperty("telephone")
    @JsonPropertyDescription("")
    private String telephone;
    /**
     * The fax number.
     * 
     */
    @JsonProperty("faxNumber")
    @JsonPropertyDescription("")
    private String faxNumber;
    /**
     * The Dun & Bradstreet DUNS number for identifying an organization or business person.
     * 
     */
    @JsonProperty("duns")
    @JsonPropertyDescription("")
    private String duns;
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * This is a generated, and simplified, variant of https://schema.org/OfferCatalog. I has been interpreted as a plain array, this behaviour is hard-coded to the itemList types and should be improved.
     * 
     */
    @JsonProperty("hasOfferCatalog")
    @JsonPropertyDescription("")
    private List<java.lang.Object> hasOfferCatalog = new ArrayList<java.lang.Object>();
    /**
     * An associated logo.
     * 
     */
    @JsonProperty("logo")
    @JsonPropertyDescription("")
    private java.lang.Object logo;
    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     */
    @JsonProperty("contactPoint")
    @JsonPropertyDescription("")
    private ContactPoint contactPoint;
    /**
     * The International Standard of Industrial Classification of All Economic Activities (ISIC), Revision 4 code for a particular organization, business person, or place.
     * 
     */
    @JsonProperty("isicV4")
    @JsonPropertyDescription("")
    private String isicV4;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("founders")
    @JsonPropertyDescription("")
    private AccountablePerson founders;
    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     */
    @JsonProperty("review")
    @JsonPropertyDescription("")
    private Review review;
    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     */
    @JsonProperty("aggregateRating")
    @JsonPropertyDescription("")
    private AggregateRating aggregateRating;
    /**
     * The Tax / Fiscal ID of the organization or person, e.g. the TIN in the US or the CIF/NIF in Spain.
     * 
     */
    @JsonProperty("taxID")
    @JsonPropertyDescription("")
    private String taxID;
    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     */
    @JsonProperty("event")
    @JsonPropertyDescription("")
    private Event event;
    /**
     * A member of an Organization or a ProgramMembership. Organizations can be members of organizations; ProgramMembership is typically for individuals.
     * 
     */
    @JsonProperty("member")
    @JsonPropertyDescription("")
    private java.lang.Object member;
    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     */
    @JsonProperty("foundingLocation")
    @JsonPropertyDescription("")
    private ContainsPlace foundingLocation;
    /**
     * The location of for example where the event is happening, an organization is located, or where an action takes place.
     * 
     */
    @JsonProperty("location")
    @JsonPropertyDescription("")
    private java.lang.Object location;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     */
    @JsonProperty("events")
    @JsonPropertyDescription("")
    private Event events;
    /**
     * Demand
     * <p>
     * A demand entity represents the public, not necessarily binding, not necessarily exclusive, announcement by an organization or person to seek a certain type of goods or services. For describing demand using this type, the very same properties used for Offer apply.
     * 
     */
    @JsonProperty("seeks")
    @JsonPropertyDescription("")
    private Seeks seeks;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;
    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     */
    @JsonProperty("numberOfEmployees")
    @JsonPropertyDescription("")
    private Weight numberOfEmployees;
    /**
     * The brand(s) associated with a product or service, or the brand(s) maintained by an organization or business person.
     * 
     */
    @JsonProperty("brand")
    @JsonPropertyDescription("")
    private java.lang.Object brand;
    /**
     * The official name of the organization, e.g. the registered company name.
     * 
     */
    @JsonProperty("legalName")
    @JsonPropertyDescription("")
    private String legalName;
    /**
     * An award won by or for this item.
     * 
     */
    @JsonProperty("award")
    @JsonPropertyDescription("")
    private String award;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("employee")
    @JsonPropertyDescription("")
    private AccountablePerson employee;
    /**
     * The date that this organization was dissolved.
     * 
     */
    @JsonProperty("dissolutionDate")
    @JsonPropertyDescription("")
    private Date dissolutionDate;
    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     */
    @JsonProperty("department")
    @JsonPropertyDescription("")
    private Affiliation department;
    /**
     * Awards won by or for this item.
     * 
     */
    @JsonProperty("awards")
    @JsonPropertyDescription("")
    private String awards;
    /**
     * A member of this organization.
     * 
     */
    @JsonProperty("members")
    @JsonPropertyDescription("")
    private java.lang.Object members;
    /**
     * Physical address of the item.
     * 
     */
    @JsonProperty("address")
    @JsonPropertyDescription("")
    private java.lang.Object address;
    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     */
    @JsonProperty("subOrganization")
    @JsonPropertyDescription("")
    private Affiliation subOrganization;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     */
    @JsonProperty("makesOffer")
    @JsonPropertyDescription("")
    private Offer makesOffer;
    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     */
    @JsonProperty("hasPOS")
    @JsonPropertyDescription("")
    private ContainsPlace hasPOS;
    /**
     * The geographic area where the service is provided.
     * 
     */
    @JsonProperty("serviceArea")
    @JsonPropertyDescription("")
    private java.lang.Object serviceArea;
    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;
    /**
     * The North American Industry Classification System (NAICS) code for a particular organization or business person.
     * 
     */
    @JsonProperty("naics")
    @JsonPropertyDescription("")
    private String naics;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * An Organization (or ProgramMembership) to which this Person or Organization belongs.
     * 
     */
    @JsonProperty("memberOf")
    @JsonPropertyDescription("")
    private java.lang.Object memberOf;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("employees")
    @JsonPropertyDescription("")
    private AccountablePerson employees;
    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     */
    @JsonProperty("alumni")
    @JsonPropertyDescription("")
    private AccountablePerson alumni;
    /**
     * Email address.
     * 
     */
    @JsonProperty("email")
    @JsonPropertyDescription("")
    private String email;
    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     */
    @JsonProperty("reviews")
    @JsonPropertyDescription("")
    private Review reviews;
    /**
     * The geographic area where a service or offered item is provided.
     * 
     */
    @JsonProperty("areaServed")
    @JsonPropertyDescription("")
    private java.lang.Object areaServed;
    /**
     * Products owned by the organization or person.
     * 
     */
    @JsonProperty("owns")
    @JsonPropertyDescription("")
    private java.lang.Object owns;
    /**
     * The Value-added Tax ID of the organization or person.
     * 
     */
    @JsonProperty("vatID")
    @JsonPropertyDescription("")
    private String vatID;
    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     */
    @JsonProperty("contactPoints")
    @JsonPropertyDescription("")
    private ContactPoint contactPoints;
    /**
     * The <a href="http://www.gs1.org/gln">Global Location Number</a> (GLN, sometimes also referred to as International Location Number or ILN) of the respective organization, person, or place. The GLN is a 13-digit number used to identify parties and physical locations.
     * 
     */
    @JsonProperty("globalLocationNumber")
    @JsonPropertyDescription("")
    private String globalLocationNumber;

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The founder
     */
    @JsonProperty("founder")
    public AccountablePerson getFounder() {
        return founder;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param founder
     *     The founder
     */
    @JsonProperty("founder")
    public void setFounder(AccountablePerson founder) {
        this.founder = founder;
    }

    public Affiliation withFounder(AccountablePerson founder) {
        this.founder = founder;
        return this;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @return
     *     The parentOrganization
     */
    @JsonProperty("parentOrganization")
    public Affiliation getParentOrganization() {
        return parentOrganization;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @param parentOrganization
     *     The parentOrganization
     */
    @JsonProperty("parentOrganization")
    public void setParentOrganization(Affiliation parentOrganization) {
        this.parentOrganization = parentOrganization;
    }

    public Affiliation withParentOrganization(Affiliation parentOrganization) {
        this.parentOrganization = parentOrganization;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public Affiliation withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public Affiliation withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * The date that this organization was founded.
     * 
     * @return
     *     The foundingDate
     */
    @JsonProperty("foundingDate")
    public Date getFoundingDate() {
        return foundingDate;
    }

    /**
     * The date that this organization was founded.
     * 
     * @param foundingDate
     *     The foundingDate
     */
    @JsonProperty("foundingDate")
    public void setFoundingDate(Date foundingDate) {
        this.foundingDate = foundingDate;
    }

    public Affiliation withFoundingDate(Date foundingDate) {
        this.foundingDate = foundingDate;
        return this;
    }

    /**
     * The telephone number.
     * 
     * @return
     *     The telephone
     */
    @JsonProperty("telephone")
    public String getTelephone() {
        return telephone;
    }

    /**
     * The telephone number.
     * 
     * @param telephone
     *     The telephone
     */
    @JsonProperty("telephone")
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Affiliation withTelephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    /**
     * The fax number.
     * 
     * @return
     *     The faxNumber
     */
    @JsonProperty("faxNumber")
    public String getFaxNumber() {
        return faxNumber;
    }

    /**
     * The fax number.
     * 
     * @param faxNumber
     *     The faxNumber
     */
    @JsonProperty("faxNumber")
    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public Affiliation withFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
        return this;
    }

    /**
     * The Dun & Bradstreet DUNS number for identifying an organization or business person.
     * 
     * @return
     *     The duns
     */
    @JsonProperty("duns")
    public String getDuns() {
        return duns;
    }

    /**
     * The Dun & Bradstreet DUNS number for identifying an organization or business person.
     * 
     * @param duns
     *     The duns
     */
    @JsonProperty("duns")
    public void setDuns(String duns) {
        this.duns = duns;
    }

    public Affiliation withDuns(String duns) {
        this.duns = duns;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public Affiliation withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * This is a generated, and simplified, variant of https://schema.org/OfferCatalog. I has been interpreted as a plain array, this behaviour is hard-coded to the itemList types and should be improved.
     * 
     * @return
     *     The hasOfferCatalog
     */
    @JsonProperty("hasOfferCatalog")
    public List<java.lang.Object> getHasOfferCatalog() {
        return hasOfferCatalog;
    }

    /**
     * This is a generated, and simplified, variant of https://schema.org/OfferCatalog. I has been interpreted as a plain array, this behaviour is hard-coded to the itemList types and should be improved.
     * 
     * @param hasOfferCatalog
     *     The hasOfferCatalog
     */
    @JsonProperty("hasOfferCatalog")
    public void setHasOfferCatalog(List<java.lang.Object> hasOfferCatalog) {
        this.hasOfferCatalog = hasOfferCatalog;
    }

    public Affiliation withHasOfferCatalog(List<java.lang.Object> hasOfferCatalog) {
        this.hasOfferCatalog = hasOfferCatalog;
        return this;
    }

    /**
     * An associated logo.
     * 
     * @return
     *     The logo
     */
    @JsonProperty("logo")
    public java.lang.Object getLogo() {
        return logo;
    }

    /**
     * An associated logo.
     * 
     * @param logo
     *     The logo
     */
    @JsonProperty("logo")
    public void setLogo(java.lang.Object logo) {
        this.logo = logo;
    }

    public Affiliation withLogo(java.lang.Object logo) {
        this.logo = logo;
        return this;
    }

    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     * @return
     *     The contactPoint
     */
    @JsonProperty("contactPoint")
    public ContactPoint getContactPoint() {
        return contactPoint;
    }

    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     * @param contactPoint
     *     The contactPoint
     */
    @JsonProperty("contactPoint")
    public void setContactPoint(ContactPoint contactPoint) {
        this.contactPoint = contactPoint;
    }

    public Affiliation withContactPoint(ContactPoint contactPoint) {
        this.contactPoint = contactPoint;
        return this;
    }

    /**
     * The International Standard of Industrial Classification of All Economic Activities (ISIC), Revision 4 code for a particular organization, business person, or place.
     * 
     * @return
     *     The isicV4
     */
    @JsonProperty("isicV4")
    public String getIsicV4() {
        return isicV4;
    }

    /**
     * The International Standard of Industrial Classification of All Economic Activities (ISIC), Revision 4 code for a particular organization, business person, or place.
     * 
     * @param isicV4
     *     The isicV4
     */
    @JsonProperty("isicV4")
    public void setIsicV4(String isicV4) {
        this.isicV4 = isicV4;
    }

    public Affiliation withIsicV4(String isicV4) {
        this.isicV4 = isicV4;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The founders
     */
    @JsonProperty("founders")
    public AccountablePerson getFounders() {
        return founders;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param founders
     *     The founders
     */
    @JsonProperty("founders")
    public void setFounders(AccountablePerson founders) {
        this.founders = founders;
    }

    public Affiliation withFounders(AccountablePerson founders) {
        this.founders = founders;
        return this;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @return
     *     The review
     */
    @JsonProperty("review")
    public Review getReview() {
        return review;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @param review
     *     The review
     */
    @JsonProperty("review")
    public void setReview(Review review) {
        this.review = review;
    }

    public Affiliation withReview(Review review) {
        this.review = review;
        return this;
    }

    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     * @return
     *     The aggregateRating
     */
    @JsonProperty("aggregateRating")
    public AggregateRating getAggregateRating() {
        return aggregateRating;
    }

    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     * @param aggregateRating
     *     The aggregateRating
     */
    @JsonProperty("aggregateRating")
    public void setAggregateRating(AggregateRating aggregateRating) {
        this.aggregateRating = aggregateRating;
    }

    public Affiliation withAggregateRating(AggregateRating aggregateRating) {
        this.aggregateRating = aggregateRating;
        return this;
    }

    /**
     * The Tax / Fiscal ID of the organization or person, e.g. the TIN in the US or the CIF/NIF in Spain.
     * 
     * @return
     *     The taxID
     */
    @JsonProperty("taxID")
    public String getTaxID() {
        return taxID;
    }

    /**
     * The Tax / Fiscal ID of the organization or person, e.g. the TIN in the US or the CIF/NIF in Spain.
     * 
     * @param taxID
     *     The taxID
     */
    @JsonProperty("taxID")
    public void setTaxID(String taxID) {
        this.taxID = taxID;
    }

    public Affiliation withTaxID(String taxID) {
        this.taxID = taxID;
        return this;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @return
     *     The event
     */
    @JsonProperty("event")
    public Event getEvent() {
        return event;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @param event
     *     The event
     */
    @JsonProperty("event")
    public void setEvent(Event event) {
        this.event = event;
    }

    public Affiliation withEvent(Event event) {
        this.event = event;
        return this;
    }

    /**
     * A member of an Organization or a ProgramMembership. Organizations can be members of organizations; ProgramMembership is typically for individuals.
     * 
     * @return
     *     The member
     */
    @JsonProperty("member")
    public java.lang.Object getMember() {
        return member;
    }

    /**
     * A member of an Organization or a ProgramMembership. Organizations can be members of organizations; ProgramMembership is typically for individuals.
     * 
     * @param member
     *     The member
     */
    @JsonProperty("member")
    public void setMember(java.lang.Object member) {
        this.member = member;
    }

    public Affiliation withMember(java.lang.Object member) {
        this.member = member;
        return this;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @return
     *     The foundingLocation
     */
    @JsonProperty("foundingLocation")
    public ContainsPlace getFoundingLocation() {
        return foundingLocation;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @param foundingLocation
     *     The foundingLocation
     */
    @JsonProperty("foundingLocation")
    public void setFoundingLocation(ContainsPlace foundingLocation) {
        this.foundingLocation = foundingLocation;
    }

    public Affiliation withFoundingLocation(ContainsPlace foundingLocation) {
        this.foundingLocation = foundingLocation;
        return this;
    }

    /**
     * The location of for example where the event is happening, an organization is located, or where an action takes place.
     * 
     * @return
     *     The location
     */
    @JsonProperty("location")
    public java.lang.Object getLocation() {
        return location;
    }

    /**
     * The location of for example where the event is happening, an organization is located, or where an action takes place.
     * 
     * @param location
     *     The location
     */
    @JsonProperty("location")
    public void setLocation(java.lang.Object location) {
        this.location = location;
    }

    public Affiliation withLocation(java.lang.Object location) {
        this.location = location;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public Affiliation withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @return
     *     The events
     */
    @JsonProperty("events")
    public Event getEvents() {
        return events;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @param events
     *     The events
     */
    @JsonProperty("events")
    public void setEvents(Event events) {
        this.events = events;
    }

    public Affiliation withEvents(Event events) {
        this.events = events;
        return this;
    }

    /**
     * Demand
     * <p>
     * A demand entity represents the public, not necessarily binding, not necessarily exclusive, announcement by an organization or person to seek a certain type of goods or services. For describing demand using this type, the very same properties used for Offer apply.
     * 
     * @return
     *     The seeks
     */
    @JsonProperty("seeks")
    public Seeks getSeeks() {
        return seeks;
    }

    /**
     * Demand
     * <p>
     * A demand entity represents the public, not necessarily binding, not necessarily exclusive, announcement by an organization or person to seek a certain type of goods or services. For describing demand using this type, the very same properties used for Offer apply.
     * 
     * @param seeks
     *     The seeks
     */
    @JsonProperty("seeks")
    public void setSeeks(Seeks seeks) {
        this.seeks = seeks;
    }

    public Affiliation withSeeks(Seeks seeks) {
        this.seeks = seeks;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public Affiliation withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @return
     *     The numberOfEmployees
     */
    @JsonProperty("numberOfEmployees")
    public Weight getNumberOfEmployees() {
        return numberOfEmployees;
    }

    /**
     * QuantitativeValue
     * <p>
     *  A point value or interval for product characteristics and other purposes.
     * 
     * @param numberOfEmployees
     *     The numberOfEmployees
     */
    @JsonProperty("numberOfEmployees")
    public void setNumberOfEmployees(Weight numberOfEmployees) {
        this.numberOfEmployees = numberOfEmployees;
    }

    public Affiliation withNumberOfEmployees(Weight numberOfEmployees) {
        this.numberOfEmployees = numberOfEmployees;
        return this;
    }

    /**
     * The brand(s) associated with a product or service, or the brand(s) maintained by an organization or business person.
     * 
     * @return
     *     The brand
     */
    @JsonProperty("brand")
    public java.lang.Object getBrand() {
        return brand;
    }

    /**
     * The brand(s) associated with a product or service, or the brand(s) maintained by an organization or business person.
     * 
     * @param brand
     *     The brand
     */
    @JsonProperty("brand")
    public void setBrand(java.lang.Object brand) {
        this.brand = brand;
    }

    public Affiliation withBrand(java.lang.Object brand) {
        this.brand = brand;
        return this;
    }

    /**
     * The official name of the organization, e.g. the registered company name.
     * 
     * @return
     *     The legalName
     */
    @JsonProperty("legalName")
    public String getLegalName() {
        return legalName;
    }

    /**
     * The official name of the organization, e.g. the registered company name.
     * 
     * @param legalName
     *     The legalName
     */
    @JsonProperty("legalName")
    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public Affiliation withLegalName(String legalName) {
        this.legalName = legalName;
        return this;
    }

    /**
     * An award won by or for this item.
     * 
     * @return
     *     The award
     */
    @JsonProperty("award")
    public String getAward() {
        return award;
    }

    /**
     * An award won by or for this item.
     * 
     * @param award
     *     The award
     */
    @JsonProperty("award")
    public void setAward(String award) {
        this.award = award;
    }

    public Affiliation withAward(String award) {
        this.award = award;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The employee
     */
    @JsonProperty("employee")
    public AccountablePerson getEmployee() {
        return employee;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param employee
     *     The employee
     */
    @JsonProperty("employee")
    public void setEmployee(AccountablePerson employee) {
        this.employee = employee;
    }

    public Affiliation withEmployee(AccountablePerson employee) {
        this.employee = employee;
        return this;
    }

    /**
     * The date that this organization was dissolved.
     * 
     * @return
     *     The dissolutionDate
     */
    @JsonProperty("dissolutionDate")
    public Date getDissolutionDate() {
        return dissolutionDate;
    }

    /**
     * The date that this organization was dissolved.
     * 
     * @param dissolutionDate
     *     The dissolutionDate
     */
    @JsonProperty("dissolutionDate")
    public void setDissolutionDate(Date dissolutionDate) {
        this.dissolutionDate = dissolutionDate;
    }

    public Affiliation withDissolutionDate(Date dissolutionDate) {
        this.dissolutionDate = dissolutionDate;
        return this;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @return
     *     The department
     */
    @JsonProperty("department")
    public Affiliation getDepartment() {
        return department;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @param department
     *     The department
     */
    @JsonProperty("department")
    public void setDepartment(Affiliation department) {
        this.department = department;
    }

    public Affiliation withDepartment(Affiliation department) {
        this.department = department;
        return this;
    }

    /**
     * Awards won by or for this item.
     * 
     * @return
     *     The awards
     */
    @JsonProperty("awards")
    public String getAwards() {
        return awards;
    }

    /**
     * Awards won by or for this item.
     * 
     * @param awards
     *     The awards
     */
    @JsonProperty("awards")
    public void setAwards(String awards) {
        this.awards = awards;
    }

    public Affiliation withAwards(String awards) {
        this.awards = awards;
        return this;
    }

    /**
     * A member of this organization.
     * 
     * @return
     *     The members
     */
    @JsonProperty("members")
    public java.lang.Object getMembers() {
        return members;
    }

    /**
     * A member of this organization.
     * 
     * @param members
     *     The members
     */
    @JsonProperty("members")
    public void setMembers(java.lang.Object members) {
        this.members = members;
    }

    public Affiliation withMembers(java.lang.Object members) {
        this.members = members;
        return this;
    }

    /**
     * Physical address of the item.
     * 
     * @return
     *     The address
     */
    @JsonProperty("address")
    public java.lang.Object getAddress() {
        return address;
    }

    /**
     * Physical address of the item.
     * 
     * @param address
     *     The address
     */
    @JsonProperty("address")
    public void setAddress(java.lang.Object address) {
        this.address = address;
    }

    public Affiliation withAddress(java.lang.Object address) {
        this.address = address;
        return this;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @return
     *     The subOrganization
     */
    @JsonProperty("subOrganization")
    public Affiliation getSubOrganization() {
        return subOrganization;
    }

    /**
     * Organization
     * <p>
     * An organization such as a school, NGO, corporation, club, etc.
     * 
     * @param subOrganization
     *     The subOrganization
     */
    @JsonProperty("subOrganization")
    public void setSubOrganization(Affiliation subOrganization) {
        this.subOrganization = subOrganization;
    }

    public Affiliation withSubOrganization(Affiliation subOrganization) {
        this.subOrganization = subOrganization;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public Affiliation withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     * @return
     *     The makesOffer
     */
    @JsonProperty("makesOffer")
    public Offer getMakesOffer() {
        return makesOffer;
    }

    /**
     * Offer
     * <p>
     * An offer to transfer some rights to an item or to provide a service&#x2014;for example, an offer to sell tickets to an event, to rent the DVD of a movie, to stream a TV show over the internet, to repair a motorcycle, or to loan a book.
     *       <br/><br/>
     *       For <a href="http://www.gs1.org/barcodes/technical/idkeys/gtin">GTIN</a>-related fields, see
     *       <a href="http://www.gs1.org/barcodes/support/check_digit_calculator">Check Digit calculator</a>
     *       and <a href="http://www.gs1us.org/resources/standards/gtin-validation-guide">validation guide</a>
     *       from <a href="http://www.gs1.org/">GS1</a>.
     * 
     * @param makesOffer
     *     The makesOffer
     */
    @JsonProperty("makesOffer")
    public void setMakesOffer(Offer makesOffer) {
        this.makesOffer = makesOffer;
    }

    public Affiliation withMakesOffer(Offer makesOffer) {
        this.makesOffer = makesOffer;
        return this;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @return
     *     The hasPOS
     */
    @JsonProperty("hasPOS")
    public ContainsPlace getHasPOS() {
        return hasPOS;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @param hasPOS
     *     The hasPOS
     */
    @JsonProperty("hasPOS")
    public void setHasPOS(ContainsPlace hasPOS) {
        this.hasPOS = hasPOS;
    }

    public Affiliation withHasPOS(ContainsPlace hasPOS) {
        this.hasPOS = hasPOS;
        return this;
    }

    /**
     * The geographic area where the service is provided.
     * 
     * @return
     *     The serviceArea
     */
    @JsonProperty("serviceArea")
    public java.lang.Object getServiceArea() {
        return serviceArea;
    }

    /**
     * The geographic area where the service is provided.
     * 
     * @param serviceArea
     *     The serviceArea
     */
    @JsonProperty("serviceArea")
    public void setServiceArea(java.lang.Object serviceArea) {
        this.serviceArea = serviceArea;
    }

    public Affiliation withServiceArea(java.lang.Object serviceArea) {
        this.serviceArea = serviceArea;
        return this;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public Affiliation withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Affiliation withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * The North American Industry Classification System (NAICS) code for a particular organization or business person.
     * 
     * @return
     *     The naics
     */
    @JsonProperty("naics")
    public String getNaics() {
        return naics;
    }

    /**
     * The North American Industry Classification System (NAICS) code for a particular organization or business person.
     * 
     * @param naics
     *     The naics
     */
    @JsonProperty("naics")
    public void setNaics(String naics) {
        this.naics = naics;
    }

    public Affiliation withNaics(String naics) {
        this.naics = naics;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public Affiliation withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * An Organization (or ProgramMembership) to which this Person or Organization belongs.
     * 
     * @return
     *     The memberOf
     */
    @JsonProperty("memberOf")
    public java.lang.Object getMemberOf() {
        return memberOf;
    }

    /**
     * An Organization (or ProgramMembership) to which this Person or Organization belongs.
     * 
     * @param memberOf
     *     The memberOf
     */
    @JsonProperty("memberOf")
    public void setMemberOf(java.lang.Object memberOf) {
        this.memberOf = memberOf;
    }

    public Affiliation withMemberOf(java.lang.Object memberOf) {
        this.memberOf = memberOf;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The employees
     */
    @JsonProperty("employees")
    public AccountablePerson getEmployees() {
        return employees;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param employees
     *     The employees
     */
    @JsonProperty("employees")
    public void setEmployees(AccountablePerson employees) {
        this.employees = employees;
    }

    public Affiliation withEmployees(AccountablePerson employees) {
        this.employees = employees;
        return this;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @return
     *     The alumni
     */
    @JsonProperty("alumni")
    public AccountablePerson getAlumni() {
        return alumni;
    }

    /**
     * Person
     * <p>
     * A person (alive, dead, undead, or fictional).
     * 
     * @param alumni
     *     The alumni
     */
    @JsonProperty("alumni")
    public void setAlumni(AccountablePerson alumni) {
        this.alumni = alumni;
    }

    public Affiliation withAlumni(AccountablePerson alumni) {
        this.alumni = alumni;
        return this;
    }

    /**
     * Email address.
     * 
     * @return
     *     The email
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     * Email address.
     * 
     * @param email
     *     The email
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    public Affiliation withEmail(String email) {
        this.email = email;
        return this;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @return
     *     The reviews
     */
    @JsonProperty("reviews")
    public Review getReviews() {
        return reviews;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @param reviews
     *     The reviews
     */
    @JsonProperty("reviews")
    public void setReviews(Review reviews) {
        this.reviews = reviews;
    }

    public Affiliation withReviews(Review reviews) {
        this.reviews = reviews;
        return this;
    }

    /**
     * The geographic area where a service or offered item is provided.
     * 
     * @return
     *     The areaServed
     */
    @JsonProperty("areaServed")
    public java.lang.Object getAreaServed() {
        return areaServed;
    }

    /**
     * The geographic area where a service or offered item is provided.
     * 
     * @param areaServed
     *     The areaServed
     */
    @JsonProperty("areaServed")
    public void setAreaServed(java.lang.Object areaServed) {
        this.areaServed = areaServed;
    }

    public Affiliation withAreaServed(java.lang.Object areaServed) {
        this.areaServed = areaServed;
        return this;
    }

    /**
     * Products owned by the organization or person.
     * 
     * @return
     *     The owns
     */
    @JsonProperty("owns")
    public java.lang.Object getOwns() {
        return owns;
    }

    /**
     * Products owned by the organization or person.
     * 
     * @param owns
     *     The owns
     */
    @JsonProperty("owns")
    public void setOwns(java.lang.Object owns) {
        this.owns = owns;
    }

    public Affiliation withOwns(java.lang.Object owns) {
        this.owns = owns;
        return this;
    }

    /**
     * The Value-added Tax ID of the organization or person.
     * 
     * @return
     *     The vatID
     */
    @JsonProperty("vatID")
    public String getVatID() {
        return vatID;
    }

    /**
     * The Value-added Tax ID of the organization or person.
     * 
     * @param vatID
     *     The vatID
     */
    @JsonProperty("vatID")
    public void setVatID(String vatID) {
        this.vatID = vatID;
    }

    public Affiliation withVatID(String vatID) {
        this.vatID = vatID;
        return this;
    }

    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     * @return
     *     The contactPoints
     */
    @JsonProperty("contactPoints")
    public ContactPoint getContactPoints() {
        return contactPoints;
    }

    /**
     * ContactPoint
     * <p>
     * A contact point&#x2014;for example, a Customer Complaints department.
     * 
     * @param contactPoints
     *     The contactPoints
     */
    @JsonProperty("contactPoints")
    public void setContactPoints(ContactPoint contactPoints) {
        this.contactPoints = contactPoints;
    }

    public Affiliation withContactPoints(ContactPoint contactPoints) {
        this.contactPoints = contactPoints;
        return this;
    }

    /**
     * The <a href="http://www.gs1.org/gln">Global Location Number</a> (GLN, sometimes also referred to as International Location Number or ILN) of the respective organization, person, or place. The GLN is a 13-digit number used to identify parties and physical locations.
     * 
     * @return
     *     The globalLocationNumber
     */
    @JsonProperty("globalLocationNumber")
    public String getGlobalLocationNumber() {
        return globalLocationNumber;
    }

    /**
     * The <a href="http://www.gs1.org/gln">Global Location Number</a> (GLN, sometimes also referred to as International Location Number or ILN) of the respective organization, person, or place. The GLN is a 13-digit number used to identify parties and physical locations.
     * 
     * @param globalLocationNumber
     *     The globalLocationNumber
     */
    @JsonProperty("globalLocationNumber")
    public void setGlobalLocationNumber(String globalLocationNumber) {
        this.globalLocationNumber = globalLocationNumber;
    }

    public Affiliation withGlobalLocationNumber(String globalLocationNumber) {
        this.globalLocationNumber = globalLocationNumber;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(founder).append(parentOrganization).append(sameAs).append(image).append(foundingDate).append(telephone).append(faxNumber).append(duns).append(additionalType).append(hasOfferCatalog).append(logo).append(contactPoint).append(isicV4).append(founders).append(review).append(aggregateRating).append(taxID).append(event).append(member).append(foundingLocation).append(location).append(mainEntityOfPage).append(events).append(seeks).append(description).append(numberOfEmployees).append(brand).append(legalName).append(award).append(employee).append(dissolutionDate).append(department).append(awards).append(members).append(address).append(subOrganization).append(alternateName).append(makesOffer).append(hasPOS).append(serviceArea).append(potentialAction).append(name).append(naics).append(url).append(memberOf).append(employees).append(alumni).append(email).append(reviews).append(areaServed).append(owns).append(vatID).append(contactPoints).append(globalLocationNumber).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Affiliation) == false) {
            return false;
        }
        Affiliation rhs = ((Affiliation) other);
        return new EqualsBuilder().append(founder, rhs.founder).append(parentOrganization, rhs.parentOrganization).append(sameAs, rhs.sameAs).append(image, rhs.image).append(foundingDate, rhs.foundingDate).append(telephone, rhs.telephone).append(faxNumber, rhs.faxNumber).append(duns, rhs.duns).append(additionalType, rhs.additionalType).append(hasOfferCatalog, rhs.hasOfferCatalog).append(logo, rhs.logo).append(contactPoint, rhs.contactPoint).append(isicV4, rhs.isicV4).append(founders, rhs.founders).append(review, rhs.review).append(aggregateRating, rhs.aggregateRating).append(taxID, rhs.taxID).append(event, rhs.event).append(member, rhs.member).append(foundingLocation, rhs.foundingLocation).append(location, rhs.location).append(mainEntityOfPage, rhs.mainEntityOfPage).append(events, rhs.events).append(seeks, rhs.seeks).append(description, rhs.description).append(numberOfEmployees, rhs.numberOfEmployees).append(brand, rhs.brand).append(legalName, rhs.legalName).append(award, rhs.award).append(employee, rhs.employee).append(dissolutionDate, rhs.dissolutionDate).append(department, rhs.department).append(awards, rhs.awards).append(members, rhs.members).append(address, rhs.address).append(subOrganization, rhs.subOrganization).append(alternateName, rhs.alternateName).append(makesOffer, rhs.makesOffer).append(hasPOS, rhs.hasPOS).append(serviceArea, rhs.serviceArea).append(potentialAction, rhs.potentialAction).append(name, rhs.name).append(naics, rhs.naics).append(url, rhs.url).append(memberOf, rhs.memberOf).append(employees, rhs.employees).append(alumni, rhs.alumni).append(email, rhs.email).append(reviews, rhs.reviews).append(areaServed, rhs.areaServed).append(owns, rhs.owns).append(vatID, rhs.vatID).append(contactPoints, rhs.contactPoints).append(globalLocationNumber, rhs.globalLocationNumber).isEquals();
    }

}
