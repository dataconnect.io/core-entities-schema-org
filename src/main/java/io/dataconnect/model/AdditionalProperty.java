
package io.dataconnect.model;

import java.net.URI;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * PropertyValue
 * <p>
 * A property-value pair, e.g. representing a feature of a product or place. Use the 'name' property for the name of the property. If there is an additional human-readable version of the value, put that into the 'description' property.
 *         <br/><br/>
 *         Always use specific schema.org properties when a) they exist and b) you can populate them. Using PropertyValue as a substitute will typically not trigger the same effect as using the original, specific property.
 *     
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "potentialAction",
    "valueReference",
    "description",
    "sameAs",
    "image",
    "maxValue",
    "value",
    "minValue",
    "additionalType",
    "url",
    "unitText",
    "mainEntityOfPage",
    "unitCode",
    "alternateName",
    "propertyID",
    "name"
})
public class AdditionalProperty {

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * A pointer to a secondary value that provides additional information on the original value, e.g. a reference temperature.
     * 
     */
    @JsonProperty("valueReference")
    @JsonPropertyDescription("")
    private java.lang.Object valueReference;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * The upper value of some characteristic or property.
     * 
     */
    @JsonProperty("maxValue")
    @JsonPropertyDescription("")
    private Double maxValue;
    /**
     * The value of the quantitative value or property value node. For QuantitativeValue, the recommended type for values is 'Number'. For PropertyValue, it can be 'Text;', 'Number', 'Boolean', or 'StructuredValue'.
     * 
     */
    @JsonProperty("value")
    @JsonPropertyDescription("")
    private java.lang.Object value;
    /**
     * The lower value of some characteristic or property.
     * 
     */
    @JsonProperty("minValue")
    @JsonPropertyDescription("")
    private Double minValue;
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * A string or text indicating the unit of measurement. Useful if you cannot provide a standard unit code for
     * <a href='unitCode'>unitCode</a>.
     * 
     */
    @JsonProperty("unitText")
    @JsonPropertyDescription("")
    private String unitText;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * The unit of measurement given using the UN/CEFACT Common Code (3 characters) or a URL. Other codes than the UN/CEFACT Common Code may be used with a prefix followed by a colon.
     * 
     */
    @JsonProperty("unitCode")
    @JsonPropertyDescription("")
    private java.lang.Object unitCode;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * A commonly used identifier for the characteristic represented by the property, e.g. a manufacturer or a standard code for a property. propertyID can be
     * (1) a prefixed string, mainly meant to be used with standards for product properties; (2) a site-specific, non-prefixed string (e.g. the primary key of the property or the vendor-specific id of the property), or (3)
     * a URL indicating the type of the property, either pointing to an external vocabulary, or a Web resource that describes the property (e.g. a glossary entry).
     * Standards bodies should promote a standard prefix for the identifiers of properties from their standards.
     * 
     */
    @JsonProperty("propertyID")
    @JsonPropertyDescription("")
    private java.lang.Object propertyID;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public AdditionalProperty withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * A pointer to a secondary value that provides additional information on the original value, e.g. a reference temperature.
     * 
     * @return
     *     The valueReference
     */
    @JsonProperty("valueReference")
    public java.lang.Object getValueReference() {
        return valueReference;
    }

    /**
     * A pointer to a secondary value that provides additional information on the original value, e.g. a reference temperature.
     * 
     * @param valueReference
     *     The valueReference
     */
    @JsonProperty("valueReference")
    public void setValueReference(java.lang.Object valueReference) {
        this.valueReference = valueReference;
    }

    public AdditionalProperty withValueReference(java.lang.Object valueReference) {
        this.valueReference = valueReference;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public AdditionalProperty withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public AdditionalProperty withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public AdditionalProperty withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * The upper value of some characteristic or property.
     * 
     * @return
     *     The maxValue
     */
    @JsonProperty("maxValue")
    public Double getMaxValue() {
        return maxValue;
    }

    /**
     * The upper value of some characteristic or property.
     * 
     * @param maxValue
     *     The maxValue
     */
    @JsonProperty("maxValue")
    public void setMaxValue(Double maxValue) {
        this.maxValue = maxValue;
    }

    public AdditionalProperty withMaxValue(Double maxValue) {
        this.maxValue = maxValue;
        return this;
    }

    /**
     * The value of the quantitative value or property value node. For QuantitativeValue, the recommended type for values is 'Number'. For PropertyValue, it can be 'Text;', 'Number', 'Boolean', or 'StructuredValue'.
     * 
     * @return
     *     The value
     */
    @JsonProperty("value")
    public java.lang.Object getValue() {
        return value;
    }

    /**
     * The value of the quantitative value or property value node. For QuantitativeValue, the recommended type for values is 'Number'. For PropertyValue, it can be 'Text;', 'Number', 'Boolean', or 'StructuredValue'.
     * 
     * @param value
     *     The value
     */
    @JsonProperty("value")
    public void setValue(java.lang.Object value) {
        this.value = value;
    }

    public AdditionalProperty withValue(java.lang.Object value) {
        this.value = value;
        return this;
    }

    /**
     * The lower value of some characteristic or property.
     * 
     * @return
     *     The minValue
     */
    @JsonProperty("minValue")
    public Double getMinValue() {
        return minValue;
    }

    /**
     * The lower value of some characteristic or property.
     * 
     * @param minValue
     *     The minValue
     */
    @JsonProperty("minValue")
    public void setMinValue(Double minValue) {
        this.minValue = minValue;
    }

    public AdditionalProperty withMinValue(Double minValue) {
        this.minValue = minValue;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public AdditionalProperty withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public AdditionalProperty withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * A string or text indicating the unit of measurement. Useful if you cannot provide a standard unit code for
     * <a href='unitCode'>unitCode</a>.
     * 
     * @return
     *     The unitText
     */
    @JsonProperty("unitText")
    public String getUnitText() {
        return unitText;
    }

    /**
     * A string or text indicating the unit of measurement. Useful if you cannot provide a standard unit code for
     * <a href='unitCode'>unitCode</a>.
     * 
     * @param unitText
     *     The unitText
     */
    @JsonProperty("unitText")
    public void setUnitText(String unitText) {
        this.unitText = unitText;
    }

    public AdditionalProperty withUnitText(String unitText) {
        this.unitText = unitText;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public AdditionalProperty withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * The unit of measurement given using the UN/CEFACT Common Code (3 characters) or a URL. Other codes than the UN/CEFACT Common Code may be used with a prefix followed by a colon.
     * 
     * @return
     *     The unitCode
     */
    @JsonProperty("unitCode")
    public java.lang.Object getUnitCode() {
        return unitCode;
    }

    /**
     * The unit of measurement given using the UN/CEFACT Common Code (3 characters) or a URL. Other codes than the UN/CEFACT Common Code may be used with a prefix followed by a colon.
     * 
     * @param unitCode
     *     The unitCode
     */
    @JsonProperty("unitCode")
    public void setUnitCode(java.lang.Object unitCode) {
        this.unitCode = unitCode;
    }

    public AdditionalProperty withUnitCode(java.lang.Object unitCode) {
        this.unitCode = unitCode;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public AdditionalProperty withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * A commonly used identifier for the characteristic represented by the property, e.g. a manufacturer or a standard code for a property. propertyID can be
     * (1) a prefixed string, mainly meant to be used with standards for product properties; (2) a site-specific, non-prefixed string (e.g. the primary key of the property or the vendor-specific id of the property), or (3)
     * a URL indicating the type of the property, either pointing to an external vocabulary, or a Web resource that describes the property (e.g. a glossary entry).
     * Standards bodies should promote a standard prefix for the identifiers of properties from their standards.
     * 
     * @return
     *     The propertyID
     */
    @JsonProperty("propertyID")
    public java.lang.Object getPropertyID() {
        return propertyID;
    }

    /**
     * A commonly used identifier for the characteristic represented by the property, e.g. a manufacturer or a standard code for a property. propertyID can be
     * (1) a prefixed string, mainly meant to be used with standards for product properties; (2) a site-specific, non-prefixed string (e.g. the primary key of the property or the vendor-specific id of the property), or (3)
     * a URL indicating the type of the property, either pointing to an external vocabulary, or a Web resource that describes the property (e.g. a glossary entry).
     * Standards bodies should promote a standard prefix for the identifiers of properties from their standards.
     * 
     * @param propertyID
     *     The propertyID
     */
    @JsonProperty("propertyID")
    public void setPropertyID(java.lang.Object propertyID) {
        this.propertyID = propertyID;
    }

    public AdditionalProperty withPropertyID(java.lang.Object propertyID) {
        this.propertyID = propertyID;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public AdditionalProperty withName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(potentialAction).append(valueReference).append(description).append(sameAs).append(image).append(maxValue).append(value).append(minValue).append(additionalType).append(url).append(unitText).append(mainEntityOfPage).append(unitCode).append(alternateName).append(propertyID).append(name).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AdditionalProperty) == false) {
            return false;
        }
        AdditionalProperty rhs = ((AdditionalProperty) other);
        return new EqualsBuilder().append(potentialAction, rhs.potentialAction).append(valueReference, rhs.valueReference).append(description, rhs.description).append(sameAs, rhs.sameAs).append(image, rhs.image).append(maxValue, rhs.maxValue).append(value, rhs.value).append(minValue, rhs.minValue).append(additionalType, rhs.additionalType).append(url, rhs.url).append(unitText, rhs.unitText).append(mainEntityOfPage, rhs.mainEntityOfPage).append(unitCode, rhs.unitCode).append(alternateName, rhs.alternateName).append(propertyID, rhs.propertyID).append(name, rhs.name).isEquals();
    }

}
