
package io.dataconnect.model;

import java.net.URI;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * AdministrativeArea
 * <p>
 * A geographical region, typically under the jurisdiction of a particular government.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "openingHoursSpecification",
    "sameAs",
    "photo",
    "image",
    "telephone",
    "faxNumber",
    "aggregateRating",
    "containsPlace",
    "additionalType",
    "logo",
    "event",
    "isicV4",
    "geo",
    "review",
    "maps",
    "additionalProperty",
    "mainEntityOfPage",
    "events",
    "map",
    "description",
    "containedIn",
    "containedInPlace",
    "photos",
    "address",
    "alternateName",
    "hasMap",
    "branchCode",
    "potentialAction",
    "name",
    "url",
    "reviews",
    "globalLocationNumber"
})
public class GeographicArea {

    /**
     * OpeningHoursSpecification
     * <p>
     * A structured value providing information about the opening hours of a place or a certain service inside a place.
     * 
     */
    @JsonProperty("openingHoursSpecification")
    @JsonPropertyDescription("")
    private HoursAvailable openingHoursSpecification;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * A photograph of this place.
     * 
     */
    @JsonProperty("photo")
    @JsonPropertyDescription("")
    private java.lang.Object photo;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * The telephone number.
     * 
     */
    @JsonProperty("telephone")
    @JsonPropertyDescription("")
    private String telephone;
    /**
     * The fax number.
     * 
     */
    @JsonProperty("faxNumber")
    @JsonPropertyDescription("")
    private String faxNumber;
    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     */
    @JsonProperty("aggregateRating")
    @JsonPropertyDescription("")
    private AggregateRating aggregateRating;
    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     */
    @JsonProperty("containsPlace")
    @JsonPropertyDescription("")
    private ContainsPlace containsPlace;
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * An associated logo.
     * 
     */
    @JsonProperty("logo")
    @JsonPropertyDescription("")
    private java.lang.Object logo;
    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     */
    @JsonProperty("event")
    @JsonPropertyDescription("")
    private Event event;
    /**
     * The International Standard of Industrial Classification of All Economic Activities (ISIC), Revision 4 code for a particular organization, business person, or place.
     * 
     */
    @JsonProperty("isicV4")
    @JsonPropertyDescription("")
    private String isicV4;
    /**
     * The geo coordinates of the place.
     * 
     */
    @JsonProperty("geo")
    @JsonPropertyDescription("")
    private java.lang.Object geo;
    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     */
    @JsonProperty("review")
    @JsonPropertyDescription("")
    private Review review;
    /**
     * A URL to a map of the place.
     * 
     */
    @JsonProperty("maps")
    @JsonPropertyDescription("")
    private URI maps;
    /**
     * PropertyValue
     * <p>
     * A property-value pair, e.g. representing a feature of a product or place. Use the 'name' property for the name of the property. If there is an additional human-readable version of the value, put that into the 'description' property.
     *         <br/><br/>
     *         Always use specific schema.org properties when a) they exist and b) you can populate them. Using PropertyValue as a substitute will typically not trigger the same effect as using the original, specific property.
     *     
     * 
     */
    @JsonProperty("additionalProperty")
    @JsonPropertyDescription("")
    private AdditionalProperty additionalProperty;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     */
    @JsonProperty("events")
    @JsonPropertyDescription("")
    private Event events;
    /**
     * A URL to a map of the place.
     * 
     */
    @JsonProperty("map")
    @JsonPropertyDescription("")
    private URI map;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;
    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     */
    @JsonProperty("containedIn")
    @JsonPropertyDescription("")
    private ContainsPlace containedIn;
    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     */
    @JsonProperty("containedInPlace")
    @JsonPropertyDescription("")
    private ContainsPlace containedInPlace;
    /**
     * Photographs of this place.
     * 
     */
    @JsonProperty("photos")
    @JsonPropertyDescription("")
    private java.lang.Object photos;
    /**
     * Physical address of the item.
     * 
     */
    @JsonProperty("address")
    @JsonPropertyDescription("")
    private java.lang.Object address;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * A URL to a map of the place.
     * 
     */
    @JsonProperty("hasMap")
    @JsonPropertyDescription("")
    private java.lang.Object hasMap;
    /**
     * A short textual code (also called "store code") that uniquely identifies a place of business. The code is typically assigned by the parentOrganization and used in structured URLs.
     * <br /><br /> For example, in the URL http://www.starbucks.co.uk/store-locator/etc/detail/3047 the code "3047" is a branchCode for a particular branch.
     *       
     * 
     */
    @JsonProperty("branchCode")
    @JsonPropertyDescription("")
    private String branchCode;
    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     */
    @JsonProperty("reviews")
    @JsonPropertyDescription("")
    private Review reviews;
    /**
     * The <a href="http://www.gs1.org/gln">Global Location Number</a> (GLN, sometimes also referred to as International Location Number or ILN) of the respective organization, person, or place. The GLN is a 13-digit number used to identify parties and physical locations.
     * 
     */
    @JsonProperty("globalLocationNumber")
    @JsonPropertyDescription("")
    private String globalLocationNumber;

    /**
     * OpeningHoursSpecification
     * <p>
     * A structured value providing information about the opening hours of a place or a certain service inside a place.
     * 
     * @return
     *     The openingHoursSpecification
     */
    @JsonProperty("openingHoursSpecification")
    public HoursAvailable getOpeningHoursSpecification() {
        return openingHoursSpecification;
    }

    /**
     * OpeningHoursSpecification
     * <p>
     * A structured value providing information about the opening hours of a place or a certain service inside a place.
     * 
     * @param openingHoursSpecification
     *     The openingHoursSpecification
     */
    @JsonProperty("openingHoursSpecification")
    public void setOpeningHoursSpecification(HoursAvailable openingHoursSpecification) {
        this.openingHoursSpecification = openingHoursSpecification;
    }

    public GeographicArea withOpeningHoursSpecification(HoursAvailable openingHoursSpecification) {
        this.openingHoursSpecification = openingHoursSpecification;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public GeographicArea withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * A photograph of this place.
     * 
     * @return
     *     The photo
     */
    @JsonProperty("photo")
    public java.lang.Object getPhoto() {
        return photo;
    }

    /**
     * A photograph of this place.
     * 
     * @param photo
     *     The photo
     */
    @JsonProperty("photo")
    public void setPhoto(java.lang.Object photo) {
        this.photo = photo;
    }

    public GeographicArea withPhoto(java.lang.Object photo) {
        this.photo = photo;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public GeographicArea withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * The telephone number.
     * 
     * @return
     *     The telephone
     */
    @JsonProperty("telephone")
    public String getTelephone() {
        return telephone;
    }

    /**
     * The telephone number.
     * 
     * @param telephone
     *     The telephone
     */
    @JsonProperty("telephone")
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public GeographicArea withTelephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    /**
     * The fax number.
     * 
     * @return
     *     The faxNumber
     */
    @JsonProperty("faxNumber")
    public String getFaxNumber() {
        return faxNumber;
    }

    /**
     * The fax number.
     * 
     * @param faxNumber
     *     The faxNumber
     */
    @JsonProperty("faxNumber")
    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public GeographicArea withFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
        return this;
    }

    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     * @return
     *     The aggregateRating
     */
    @JsonProperty("aggregateRating")
    public AggregateRating getAggregateRating() {
        return aggregateRating;
    }

    /**
     * AggregateRating
     * <p>
     * The average rating based on multiple ratings or reviews.
     * 
     * @param aggregateRating
     *     The aggregateRating
     */
    @JsonProperty("aggregateRating")
    public void setAggregateRating(AggregateRating aggregateRating) {
        this.aggregateRating = aggregateRating;
    }

    public GeographicArea withAggregateRating(AggregateRating aggregateRating) {
        this.aggregateRating = aggregateRating;
        return this;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @return
     *     The containsPlace
     */
    @JsonProperty("containsPlace")
    public ContainsPlace getContainsPlace() {
        return containsPlace;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @param containsPlace
     *     The containsPlace
     */
    @JsonProperty("containsPlace")
    public void setContainsPlace(ContainsPlace containsPlace) {
        this.containsPlace = containsPlace;
    }

    public GeographicArea withContainsPlace(ContainsPlace containsPlace) {
        this.containsPlace = containsPlace;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public GeographicArea withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * An associated logo.
     * 
     * @return
     *     The logo
     */
    @JsonProperty("logo")
    public java.lang.Object getLogo() {
        return logo;
    }

    /**
     * An associated logo.
     * 
     * @param logo
     *     The logo
     */
    @JsonProperty("logo")
    public void setLogo(java.lang.Object logo) {
        this.logo = logo;
    }

    public GeographicArea withLogo(java.lang.Object logo) {
        this.logo = logo;
        return this;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @return
     *     The event
     */
    @JsonProperty("event")
    public Event getEvent() {
        return event;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @param event
     *     The event
     */
    @JsonProperty("event")
    public void setEvent(Event event) {
        this.event = event;
    }

    public GeographicArea withEvent(Event event) {
        this.event = event;
        return this;
    }

    /**
     * The International Standard of Industrial Classification of All Economic Activities (ISIC), Revision 4 code for a particular organization, business person, or place.
     * 
     * @return
     *     The isicV4
     */
    @JsonProperty("isicV4")
    public String getIsicV4() {
        return isicV4;
    }

    /**
     * The International Standard of Industrial Classification of All Economic Activities (ISIC), Revision 4 code for a particular organization, business person, or place.
     * 
     * @param isicV4
     *     The isicV4
     */
    @JsonProperty("isicV4")
    public void setIsicV4(String isicV4) {
        this.isicV4 = isicV4;
    }

    public GeographicArea withIsicV4(String isicV4) {
        this.isicV4 = isicV4;
        return this;
    }

    /**
     * The geo coordinates of the place.
     * 
     * @return
     *     The geo
     */
    @JsonProperty("geo")
    public java.lang.Object getGeo() {
        return geo;
    }

    /**
     * The geo coordinates of the place.
     * 
     * @param geo
     *     The geo
     */
    @JsonProperty("geo")
    public void setGeo(java.lang.Object geo) {
        this.geo = geo;
    }

    public GeographicArea withGeo(java.lang.Object geo) {
        this.geo = geo;
        return this;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @return
     *     The review
     */
    @JsonProperty("review")
    public Review getReview() {
        return review;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @param review
     *     The review
     */
    @JsonProperty("review")
    public void setReview(Review review) {
        this.review = review;
    }

    public GeographicArea withReview(Review review) {
        this.review = review;
        return this;
    }

    /**
     * A URL to a map of the place.
     * 
     * @return
     *     The maps
     */
    @JsonProperty("maps")
    public URI getMaps() {
        return maps;
    }

    /**
     * A URL to a map of the place.
     * 
     * @param maps
     *     The maps
     */
    @JsonProperty("maps")
    public void setMaps(URI maps) {
        this.maps = maps;
    }

    public GeographicArea withMaps(URI maps) {
        this.maps = maps;
        return this;
    }

    /**
     * PropertyValue
     * <p>
     * A property-value pair, e.g. representing a feature of a product or place. Use the 'name' property for the name of the property. If there is an additional human-readable version of the value, put that into the 'description' property.
     *         <br/><br/>
     *         Always use specific schema.org properties when a) they exist and b) you can populate them. Using PropertyValue as a substitute will typically not trigger the same effect as using the original, specific property.
     *     
     * 
     * @return
     *     The additionalProperty
     */
    @JsonProperty("additionalProperty")
    public AdditionalProperty getAdditionalProperty() {
        return additionalProperty;
    }

    /**
     * PropertyValue
     * <p>
     * A property-value pair, e.g. representing a feature of a product or place. Use the 'name' property for the name of the property. If there is an additional human-readable version of the value, put that into the 'description' property.
     *         <br/><br/>
     *         Always use specific schema.org properties when a) they exist and b) you can populate them. Using PropertyValue as a substitute will typically not trigger the same effect as using the original, specific property.
     *     
     * 
     * @param additionalProperty
     *     The additionalProperty
     */
    @JsonProperty("additionalProperty")
    public void setAdditionalProperty(AdditionalProperty additionalProperty) {
        this.additionalProperty = additionalProperty;
    }

    public GeographicArea withAdditionalProperty(AdditionalProperty additionalProperty) {
        this.additionalProperty = additionalProperty;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public GeographicArea withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @return
     *     The events
     */
    @JsonProperty("events")
    public Event getEvents() {
        return events;
    }

    /**
     * Event
     * <p>
     * An event happening at a certain time and location, such as a concert, lecture, or festival. Ticketing information may be added via the 'offers' property. Repeated events may be structured as separate Event objects.
     * 
     * @param events
     *     The events
     */
    @JsonProperty("events")
    public void setEvents(Event events) {
        this.events = events;
    }

    public GeographicArea withEvents(Event events) {
        this.events = events;
        return this;
    }

    /**
     * A URL to a map of the place.
     * 
     * @return
     *     The map
     */
    @JsonProperty("map")
    public URI getMap() {
        return map;
    }

    /**
     * A URL to a map of the place.
     * 
     * @param map
     *     The map
     */
    @JsonProperty("map")
    public void setMap(URI map) {
        this.map = map;
    }

    public GeographicArea withMap(URI map) {
        this.map = map;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public GeographicArea withDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @return
     *     The containedIn
     */
    @JsonProperty("containedIn")
    public ContainsPlace getContainedIn() {
        return containedIn;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @param containedIn
     *     The containedIn
     */
    @JsonProperty("containedIn")
    public void setContainedIn(ContainsPlace containedIn) {
        this.containedIn = containedIn;
    }

    public GeographicArea withContainedIn(ContainsPlace containedIn) {
        this.containedIn = containedIn;
        return this;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @return
     *     The containedInPlace
     */
    @JsonProperty("containedInPlace")
    public ContainsPlace getContainedInPlace() {
        return containedInPlace;
    }

    /**
     * Place
     * <p>
     * Entities that have a somewhat fixed, physical extension.
     * 
     * @param containedInPlace
     *     The containedInPlace
     */
    @JsonProperty("containedInPlace")
    public void setContainedInPlace(ContainsPlace containedInPlace) {
        this.containedInPlace = containedInPlace;
    }

    public GeographicArea withContainedInPlace(ContainsPlace containedInPlace) {
        this.containedInPlace = containedInPlace;
        return this;
    }

    /**
     * Photographs of this place.
     * 
     * @return
     *     The photos
     */
    @JsonProperty("photos")
    public java.lang.Object getPhotos() {
        return photos;
    }

    /**
     * Photographs of this place.
     * 
     * @param photos
     *     The photos
     */
    @JsonProperty("photos")
    public void setPhotos(java.lang.Object photos) {
        this.photos = photos;
    }

    public GeographicArea withPhotos(java.lang.Object photos) {
        this.photos = photos;
        return this;
    }

    /**
     * Physical address of the item.
     * 
     * @return
     *     The address
     */
    @JsonProperty("address")
    public java.lang.Object getAddress() {
        return address;
    }

    /**
     * Physical address of the item.
     * 
     * @param address
     *     The address
     */
    @JsonProperty("address")
    public void setAddress(java.lang.Object address) {
        this.address = address;
    }

    public GeographicArea withAddress(java.lang.Object address) {
        this.address = address;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public GeographicArea withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * A URL to a map of the place.
     * 
     * @return
     *     The hasMap
     */
    @JsonProperty("hasMap")
    public java.lang.Object getHasMap() {
        return hasMap;
    }

    /**
     * A URL to a map of the place.
     * 
     * @param hasMap
     *     The hasMap
     */
    @JsonProperty("hasMap")
    public void setHasMap(java.lang.Object hasMap) {
        this.hasMap = hasMap;
    }

    public GeographicArea withHasMap(java.lang.Object hasMap) {
        this.hasMap = hasMap;
        return this;
    }

    /**
     * A short textual code (also called "store code") that uniquely identifies a place of business. The code is typically assigned by the parentOrganization and used in structured URLs.
     * <br /><br /> For example, in the URL http://www.starbucks.co.uk/store-locator/etc/detail/3047 the code "3047" is a branchCode for a particular branch.
     *       
     * 
     * @return
     *     The branchCode
     */
    @JsonProperty("branchCode")
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * A short textual code (also called "store code") that uniquely identifies a place of business. The code is typically assigned by the parentOrganization and used in structured URLs.
     * <br /><br /> For example, in the URL http://www.starbucks.co.uk/store-locator/etc/detail/3047 the code "3047" is a branchCode for a particular branch.
     *       
     * 
     * @param branchCode
     *     The branchCode
     */
    @JsonProperty("branchCode")
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public GeographicArea withBranchCode(String branchCode) {
        this.branchCode = branchCode;
        return this;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public GeographicArea withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public GeographicArea withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public GeographicArea withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @return
     *     The reviews
     */
    @JsonProperty("reviews")
    public Review getReviews() {
        return reviews;
    }

    /**
     * Review
     * <p>
     * A review of an item - for example, of a restaurant, movie, or store.
     * 
     * @param reviews
     *     The reviews
     */
    @JsonProperty("reviews")
    public void setReviews(Review reviews) {
        this.reviews = reviews;
    }

    public GeographicArea withReviews(Review reviews) {
        this.reviews = reviews;
        return this;
    }

    /**
     * The <a href="http://www.gs1.org/gln">Global Location Number</a> (GLN, sometimes also referred to as International Location Number or ILN) of the respective organization, person, or place. The GLN is a 13-digit number used to identify parties and physical locations.
     * 
     * @return
     *     The globalLocationNumber
     */
    @JsonProperty("globalLocationNumber")
    public String getGlobalLocationNumber() {
        return globalLocationNumber;
    }

    /**
     * The <a href="http://www.gs1.org/gln">Global Location Number</a> (GLN, sometimes also referred to as International Location Number or ILN) of the respective organization, person, or place. The GLN is a 13-digit number used to identify parties and physical locations.
     * 
     * @param globalLocationNumber
     *     The globalLocationNumber
     */
    @JsonProperty("globalLocationNumber")
    public void setGlobalLocationNumber(String globalLocationNumber) {
        this.globalLocationNumber = globalLocationNumber;
    }

    public GeographicArea withGlobalLocationNumber(String globalLocationNumber) {
        this.globalLocationNumber = globalLocationNumber;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(openingHoursSpecification).append(sameAs).append(photo).append(image).append(telephone).append(faxNumber).append(aggregateRating).append(containsPlace).append(additionalType).append(logo).append(event).append(isicV4).append(geo).append(review).append(maps).append(additionalProperty).append(mainEntityOfPage).append(events).append(map).append(description).append(containedIn).append(containedInPlace).append(photos).append(address).append(alternateName).append(hasMap).append(branchCode).append(potentialAction).append(name).append(url).append(reviews).append(globalLocationNumber).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GeographicArea) == false) {
            return false;
        }
        GeographicArea rhs = ((GeographicArea) other);
        return new EqualsBuilder().append(openingHoursSpecification, rhs.openingHoursSpecification).append(sameAs, rhs.sameAs).append(photo, rhs.photo).append(image, rhs.image).append(telephone, rhs.telephone).append(faxNumber, rhs.faxNumber).append(aggregateRating, rhs.aggregateRating).append(containsPlace, rhs.containsPlace).append(additionalType, rhs.additionalType).append(logo, rhs.logo).append(event, rhs.event).append(isicV4, rhs.isicV4).append(geo, rhs.geo).append(review, rhs.review).append(maps, rhs.maps).append(additionalProperty, rhs.additionalProperty).append(mainEntityOfPage, rhs.mainEntityOfPage).append(events, rhs.events).append(map, rhs.map).append(description, rhs.description).append(containedIn, rhs.containedIn).append(containedInPlace, rhs.containedInPlace).append(photos, rhs.photos).append(address, rhs.address).append(alternateName, rhs.alternateName).append(hasMap, rhs.hasMap).append(branchCode, rhs.branchCode).append(potentialAction, rhs.potentialAction).append(name, rhs.name).append(url, rhs.url).append(reviews, rhs.reviews).append(globalLocationNumber, rhs.globalLocationNumber).isEquals();
    }

}
