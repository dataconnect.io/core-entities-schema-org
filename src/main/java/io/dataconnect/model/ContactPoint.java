
package io.dataconnect.model;

import java.net.URI;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * ContactPoint
 * <p>
 * A contact point&#x2014;for example, a Customer Complaints department.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "serviceArea",
    "potentialAction",
    "name",
    "sameAs",
    "productSupported",
    "image",
    "contactOption",
    "telephone",
    "faxNumber",
    "url",
    "hoursAvailable",
    "areaServed",
    "contactType",
    "mainEntityOfPage",
    "additionalType",
    "alternateName",
    "email",
    "availableLanguage",
    "description"
})
public class ContactPoint {

    /**
     * The geographic area where the service is provided.
     * 
     */
    @JsonProperty("serviceArea")
    @JsonPropertyDescription("")
    private java.lang.Object serviceArea;
    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     */
    @JsonProperty("potentialAction")
    @JsonPropertyDescription("")
    private PotentialAction potentialAction;
    /**
     * The name of the item.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("")
    private String name;
    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     */
    @JsonProperty("sameAs")
    @JsonPropertyDescription("")
    private URI sameAs;
    /**
     * The product or service this support contact point is related to (such as product support for a particular product line). This can be a specific product or product line (e.g. "iPhone") or a general category of products or services (e.g. "smartphones").
     * 
     */
    @JsonProperty("productSupported")
    @JsonPropertyDescription("")
    private java.lang.Object productSupported;
    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     */
    @JsonProperty("image")
    @JsonPropertyDescription("")
    private java.lang.Object image;
    /**
     * ContactPointOption
     * <p>
     * Enumerated options related to a ContactPoint.
     * 
     */
    @JsonProperty("contactOption")
    @JsonPropertyDescription("")
    private ContactOption contactOption;
    /**
     * The telephone number.
     * 
     */
    @JsonProperty("telephone")
    @JsonPropertyDescription("")
    private String telephone;
    /**
     * The fax number.
     * 
     */
    @JsonProperty("faxNumber")
    @JsonPropertyDescription("")
    private String faxNumber;
    /**
     * URL of the item.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("")
    private URI url;
    /**
     * OpeningHoursSpecification
     * <p>
     * A structured value providing information about the opening hours of a place or a certain service inside a place.
     * 
     */
    @JsonProperty("hoursAvailable")
    @JsonPropertyDescription("")
    private HoursAvailable hoursAvailable;
    /**
     * The geographic area where a service or offered item is provided.
     * 
     */
    @JsonProperty("areaServed")
    @JsonPropertyDescription("")
    private java.lang.Object areaServed;
    /**
     * A person or organization can have different contact points, for different purposes. For example, a sales contact point, a PR contact point and so on. This property is used to specify the kind of contact point.
     * 
     */
    @JsonProperty("contactType")
    @JsonPropertyDescription("")
    private String contactType;
    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     */
    @JsonProperty("mainEntityOfPage")
    @JsonPropertyDescription("")
    private java.lang.Object mainEntityOfPage;
    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     */
    @JsonProperty("additionalType")
    @JsonPropertyDescription("")
    private URI additionalType;
    /**
     * An alias for the item.
     * 
     */
    @JsonProperty("alternateName")
    @JsonPropertyDescription("")
    private String alternateName;
    /**
     * Email address.
     * 
     */
    @JsonProperty("email")
    @JsonPropertyDescription("")
    private String email;
    /**
     * Language
     * <p>
     * Natural languages such as Spanish, Tamil, Hindi, English, etc. and programming languages such as Scheme and Lisp.
     * 
     */
    @JsonProperty("availableLanguage")
    @JsonPropertyDescription("")
    private AvailableLanguage availableLanguage;
    /**
     * A short description of the item.
     * 
     */
    @JsonProperty("description")
    @JsonPropertyDescription("")
    private String description;

    /**
     * The geographic area where the service is provided.
     * 
     * @return
     *     The serviceArea
     */
    @JsonProperty("serviceArea")
    public java.lang.Object getServiceArea() {
        return serviceArea;
    }

    /**
     * The geographic area where the service is provided.
     * 
     * @param serviceArea
     *     The serviceArea
     */
    @JsonProperty("serviceArea")
    public void setServiceArea(java.lang.Object serviceArea) {
        this.serviceArea = serviceArea;
    }

    public ContactPoint withServiceArea(java.lang.Object serviceArea) {
        this.serviceArea = serviceArea;
        return this;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @return
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public PotentialAction getPotentialAction() {
        return potentialAction;
    }

    /**
     * Action
     * <p>
     * An action performed by a direct agent and indirect participants upon a direct object. Optionally happens at a location with the help of an inanimate instrument. The execution of the action may produce a result. Specific action sub-type documentation specifies the exact expectation of each argument/role.
     *       <br/><br/>See also <a href="http://blog.schema.org/2014/04/announcing-schemaorg-actions.html">blog post</a>
     *       and <a href="http://schema.org/docs/actions.html">Actions overview document</a>.
     * 
     * @param potentialAction
     *     The potentialAction
     */
    @JsonProperty("potentialAction")
    public void setPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
    }

    public ContactPoint withPotentialAction(PotentialAction potentialAction) {
        this.potentialAction = potentialAction;
        return this;
    }

    /**
     * The name of the item.
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * The name of the item.
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public ContactPoint withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @return
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public URI getSameAs() {
        return sameAs;
    }

    /**
     * URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
     * 
     * @param sameAs
     *     The sameAs
     */
    @JsonProperty("sameAs")
    public void setSameAs(URI sameAs) {
        this.sameAs = sameAs;
    }

    public ContactPoint withSameAs(URI sameAs) {
        this.sameAs = sameAs;
        return this;
    }

    /**
     * The product or service this support contact point is related to (such as product support for a particular product line). This can be a specific product or product line (e.g. "iPhone") or a general category of products or services (e.g. "smartphones").
     * 
     * @return
     *     The productSupported
     */
    @JsonProperty("productSupported")
    public java.lang.Object getProductSupported() {
        return productSupported;
    }

    /**
     * The product or service this support contact point is related to (such as product support for a particular product line). This can be a specific product or product line (e.g. "iPhone") or a general category of products or services (e.g. "smartphones").
     * 
     * @param productSupported
     *     The productSupported
     */
    @JsonProperty("productSupported")
    public void setProductSupported(java.lang.Object productSupported) {
        this.productSupported = productSupported;
    }

    public ContactPoint withProductSupported(java.lang.Object productSupported) {
        this.productSupported = productSupported;
        return this;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @return
     *     The image
     */
    @JsonProperty("image")
    public java.lang.Object getImage() {
        return image;
    }

    /**
     * An image of the item. This can be a <a href="http://schema.org/URL">URL</a> or a fully described <a href="http://schema.org/ImageObject">ImageObject</a>.
     * 
     * @param image
     *     The image
     */
    @JsonProperty("image")
    public void setImage(java.lang.Object image) {
        this.image = image;
    }

    public ContactPoint withImage(java.lang.Object image) {
        this.image = image;
        return this;
    }

    /**
     * ContactPointOption
     * <p>
     * Enumerated options related to a ContactPoint.
     * 
     * @return
     *     The contactOption
     */
    @JsonProperty("contactOption")
    public ContactOption getContactOption() {
        return contactOption;
    }

    /**
     * ContactPointOption
     * <p>
     * Enumerated options related to a ContactPoint.
     * 
     * @param contactOption
     *     The contactOption
     */
    @JsonProperty("contactOption")
    public void setContactOption(ContactOption contactOption) {
        this.contactOption = contactOption;
    }

    public ContactPoint withContactOption(ContactOption contactOption) {
        this.contactOption = contactOption;
        return this;
    }

    /**
     * The telephone number.
     * 
     * @return
     *     The telephone
     */
    @JsonProperty("telephone")
    public String getTelephone() {
        return telephone;
    }

    /**
     * The telephone number.
     * 
     * @param telephone
     *     The telephone
     */
    @JsonProperty("telephone")
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public ContactPoint withTelephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    /**
     * The fax number.
     * 
     * @return
     *     The faxNumber
     */
    @JsonProperty("faxNumber")
    public String getFaxNumber() {
        return faxNumber;
    }

    /**
     * The fax number.
     * 
     * @param faxNumber
     *     The faxNumber
     */
    @JsonProperty("faxNumber")
    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public ContactPoint withFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
        return this;
    }

    /**
     * URL of the item.
     * 
     * @return
     *     The url
     */
    @JsonProperty("url")
    public URI getUrl() {
        return url;
    }

    /**
     * URL of the item.
     * 
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(URI url) {
        this.url = url;
    }

    public ContactPoint withUrl(URI url) {
        this.url = url;
        return this;
    }

    /**
     * OpeningHoursSpecification
     * <p>
     * A structured value providing information about the opening hours of a place or a certain service inside a place.
     * 
     * @return
     *     The hoursAvailable
     */
    @JsonProperty("hoursAvailable")
    public HoursAvailable getHoursAvailable() {
        return hoursAvailable;
    }

    /**
     * OpeningHoursSpecification
     * <p>
     * A structured value providing information about the opening hours of a place or a certain service inside a place.
     * 
     * @param hoursAvailable
     *     The hoursAvailable
     */
    @JsonProperty("hoursAvailable")
    public void setHoursAvailable(HoursAvailable hoursAvailable) {
        this.hoursAvailable = hoursAvailable;
    }

    public ContactPoint withHoursAvailable(HoursAvailable hoursAvailable) {
        this.hoursAvailable = hoursAvailable;
        return this;
    }

    /**
     * The geographic area where a service or offered item is provided.
     * 
     * @return
     *     The areaServed
     */
    @JsonProperty("areaServed")
    public java.lang.Object getAreaServed() {
        return areaServed;
    }

    /**
     * The geographic area where a service or offered item is provided.
     * 
     * @param areaServed
     *     The areaServed
     */
    @JsonProperty("areaServed")
    public void setAreaServed(java.lang.Object areaServed) {
        this.areaServed = areaServed;
    }

    public ContactPoint withAreaServed(java.lang.Object areaServed) {
        this.areaServed = areaServed;
        return this;
    }

    /**
     * A person or organization can have different contact points, for different purposes. For example, a sales contact point, a PR contact point and so on. This property is used to specify the kind of contact point.
     * 
     * @return
     *     The contactType
     */
    @JsonProperty("contactType")
    public String getContactType() {
        return contactType;
    }

    /**
     * A person or organization can have different contact points, for different purposes. For example, a sales contact point, a PR contact point and so on. This property is used to specify the kind of contact point.
     * 
     * @param contactType
     *     The contactType
     */
    @JsonProperty("contactType")
    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public ContactPoint withContactType(String contactType) {
        this.contactType = contactType;
        return this;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @return
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public java.lang.Object getMainEntityOfPage() {
        return mainEntityOfPage;
    }

    /**
     * Indicates a page (or other CreativeWork) for which this thing is the main entity being described.
     *       <br /><br />
     *       See <a href="/docs/datamodel.html#mainEntityBackground">background notes</a> for details.
     *       
     * 
     * @param mainEntityOfPage
     *     The mainEntityOfPage
     */
    @JsonProperty("mainEntityOfPage")
    public void setMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
    }

    public ContactPoint withMainEntityOfPage(java.lang.Object mainEntityOfPage) {
        this.mainEntityOfPage = mainEntityOfPage;
        return this;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @return
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public URI getAdditionalType() {
        return additionalType;
    }

    /**
     * An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
     * 
     * @param additionalType
     *     The additionalType
     */
    @JsonProperty("additionalType")
    public void setAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
    }

    public ContactPoint withAdditionalType(URI additionalType) {
        this.additionalType = additionalType;
        return this;
    }

    /**
     * An alias for the item.
     * 
     * @return
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * An alias for the item.
     * 
     * @param alternateName
     *     The alternateName
     */
    @JsonProperty("alternateName")
    public void setAlternateName(String alternateName) {
        this.alternateName = alternateName;
    }

    public ContactPoint withAlternateName(String alternateName) {
        this.alternateName = alternateName;
        return this;
    }

    /**
     * Email address.
     * 
     * @return
     *     The email
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     * Email address.
     * 
     * @param email
     *     The email
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    public ContactPoint withEmail(String email) {
        this.email = email;
        return this;
    }

    /**
     * Language
     * <p>
     * Natural languages such as Spanish, Tamil, Hindi, English, etc. and programming languages such as Scheme and Lisp.
     * 
     * @return
     *     The availableLanguage
     */
    @JsonProperty("availableLanguage")
    public AvailableLanguage getAvailableLanguage() {
        return availableLanguage;
    }

    /**
     * Language
     * <p>
     * Natural languages such as Spanish, Tamil, Hindi, English, etc. and programming languages such as Scheme and Lisp.
     * 
     * @param availableLanguage
     *     The availableLanguage
     */
    @JsonProperty("availableLanguage")
    public void setAvailableLanguage(AvailableLanguage availableLanguage) {
        this.availableLanguage = availableLanguage;
    }

    public ContactPoint withAvailableLanguage(AvailableLanguage availableLanguage) {
        this.availableLanguage = availableLanguage;
        return this;
    }

    /**
     * A short description of the item.
     * 
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * A short description of the item.
     * 
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public ContactPoint withDescription(String description) {
        this.description = description;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(serviceArea).append(potentialAction).append(name).append(sameAs).append(productSupported).append(image).append(contactOption).append(telephone).append(faxNumber).append(url).append(hoursAvailable).append(areaServed).append(contactType).append(mainEntityOfPage).append(additionalType).append(alternateName).append(email).append(availableLanguage).append(description).toHashCode();
    }

    @Override
    public boolean equals(java.lang.Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ContactPoint) == false) {
            return false;
        }
        ContactPoint rhs = ((ContactPoint) other);
        return new EqualsBuilder().append(serviceArea, rhs.serviceArea).append(potentialAction, rhs.potentialAction).append(name, rhs.name).append(sameAs, rhs.sameAs).append(productSupported, rhs.productSupported).append(image, rhs.image).append(contactOption, rhs.contactOption).append(telephone, rhs.telephone).append(faxNumber, rhs.faxNumber).append(url, rhs.url).append(hoursAvailable, rhs.hoursAvailable).append(areaServed, rhs.areaServed).append(contactType, rhs.contactType).append(mainEntityOfPage, rhs.mainEntityOfPage).append(additionalType, rhs.additionalType).append(alternateName, rhs.alternateName).append(email, rhs.email).append(availableLanguage, rhs.availableLanguage).append(description, rhs.description).isEquals();
    }

}
